<?php


//-------------> Text domain (code localisation) registration <--------------
add_action('after_setup_theme', 'language_support_setup');
function language_support_setup(){
	load_theme_textdomain('ono', get_template_directory() . '/lang');
}
// theme_name-he_IL.mo
// theme_name-he_IL.po



//-------------> Menu registration <------------
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menu ( 'primary', 'Header Navigation' );
	register_nav_menu ( 'slider-menu', 'Slider Navigation' );
}


//-------------> Add Excerpt Support to Posts <------------
// Keep in mind that after that you need to activate it inside of page �Screen Options�
add_post_type_support( 'page', 'excerpt' );


//-------------> Thumbnail Support <------------
add_theme_support( 'post-thumbnails' );

//add_theme_support( 'post-thumbnails', array( 'page' ) );


//-------------> jQuery registration <------------
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   //$jquery_url = "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js";
   $jquery_url = get_template_directory_uri() . "/js/jquery.1.11.0.min.js";
   wp_register_script('jquery', $jquery_url, false, null);
   wp_enqueue_script('jquery');
}


//-------------> Custom Javascript <------------
function theme_custom_javascript() {
	?>
	<div id="fb-root"></div>
	
	<script  type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui-1.10.4.custom.min.js"></script>
	<script  type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
	<script  type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
	
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "http://connect.facebook.net/he_IL/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script type="text/javascript">
	 
	</script>
	<?php
}

add_action('wp_footer', 'theme_custom_javascript');


//-------------> Add shortcode support in text widget <------------
add_filter( 'widget_text', 'do_shortcode' );

//--------------> custom admin style <---------------------------
add_action( 'login_head', my_theme_add_editor_styles);

function my_theme_add_editor_styles() {
    add_editor_style( 'css/custom-editor-style.css' );
}

//--------------> Change Search URL rewriting <-------------------
function change_search_url_rewrite() {
    if ( isset( $_GET['s'] ) ) {
        wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
        exit();
    }   
}
add_action( 'template_redirect', 'change_search_url_rewrite' );


//-------------> Dynamic sidebars registration <------------------
function ono_sedebar_registration() {
	register_sidebar(array(
		'name' => __('Default Sidebar'),
		'id' => 'default-sidebar',
		'description' => '',
		'before_widget' => '<div id="widget-container">',
		'after_widget' => '</div>'
	));
	register_sidebar(array(
		'name' => __('Footer Menu 1'),
		'id' => 'footer-menu-1',
		'description' => __('The first footer menu'),
		'before_widget' => '<div id="widget-container">',
		'after_widget' => '</div>'
	));
	register_sidebar(array(
		'name' => __('Footer Menu 2'),
		'id' => 'footer-menu-2',
		'description' => __('The second footer menu'),
		'before_widget' => '<div id="widget-container">',
		'after_widget' => '</div>'
	));
	register_sidebar(array(
		'name' => __('Footer Menu 3'),
		'id' => 'footer-menu-3',
		'description' => __('The second footer menu'),
		'before_widget' => '<div id="widget-container">',
		'after_widget' => '</div>'
	));
	register_sidebar(array(
		'name' => __('Footer Menu 4'),
		'id' => 'footer-menu-4',
		'description' => __('The second footer menu'),
		'before_widget' => '<div id="widget-container">',
		'after_widget' => '</div>'
	));
	register_sidebar(array(
		'name' => __('Footer Menu 5'),
		'id' => 'footer-menu-5',
		'description' => __('The second footer menu'),
		'before_widget' => '<div id="widget-container">',
		'after_widget' => '</div>'
	));
}

add_action( 'widgets_init', 'ono_sedebar_registration' );





//-------------------> Widgets <------------------

include_once('incl/widget-facebook.php');
include_once('incl/widget-contact-info.php');
include_once('incl/widget-events.php');
include_once('incl/widget-news.php');
include_once('incl/widget-stuff-thumbnail.php');
include_once('incl/widget-faculty-stuff-link.php');
include_once('incl/widget-faculty-stuff-list.php');
include_once('incl/widget-learning-program.php');

include_once('incl/ono-theme-settings-page.php');




?>
