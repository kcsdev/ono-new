<?php get_header(); ?>




<!------------------------* SLIDER *--------------------------->

<div class="thumbnail-container">
	
	<?php 
		echo '<img src="' . get_template_directory_uri() . '/img/news_and_events.jpg" />';
	?>
	
	<div id="main-title-container">
		<h1 id="main-title">
			<?php if(ICL_LANGUAGE_CODE!=en){ echo 'ארועים וחדשות';} else{ _e('Events', 'ono');} ?>
		</h1>
	</div>
</div>

<!------------------------* CONTENT *--------------------------->

<div class="container">
	
	<div class="row">
		<div id="main-content" class="col-2-3 col-right">
			<!---------------- Main Column ----------------------------------->

			
			<div class="content">
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
				
				<?php if ( ! post_password_required() ): ?>
				
					<h2 class="subtitle">
						<a href="<?php echo get_post_permalink();  ?>">
							<?php the_title(); ?>
						</a>
					</h2>	
					
					<?php the_excerpt(); ?>
					
				<?php endif; ?>
				
				<?php
					endwhile;
					endif;
				?>
				
				<div class="pagination">
					<?php
						global $wp_query;

						$big = 999999999; // need an unlikely integer

						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total' => $wp_query->max_num_pages
						) );
						?>
				</div>
							 <div class="print">
<?php echo do_shortcode( '[print-me]' ) ?>
</div>
			</div>
		</div>
		<div class="col-1-3 col-left">
			<!---------------- Sidebar ---------------------------------------->
			<div id="sidebar">
				
				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
	
</div>




<?php get_footer(); ?>


