
<?php get_header(); ?>


<div class="thumbnail-container">
	
	<img src="<?php echo get_template_directory_uri(); ?>/img/slider-teachers2.jpg" />
	
	<div id="main-title-container">
		<h1 id="main-title">
			<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term->name; ?>
		</h1>
	</div>
</div>


<div class="container">

<?php include_once('breadcrumbs.php'); ?>
	
	<div class="row">
		<div class="col-1-3 col-right">
			<div id="page-side-menu">
				<div id="smartphone-submenu-btn">
					<img src="<?php echo get_template_directory_uri(); ?>/img/three-bars.png" />
				</div>
				
				<div id="page-side-menu-wrap">
					<?php include_once('incl/menu-lecturers.php'); ?>
				</div>
				
			</div>
		</div>
		<div id="staff-single-content" class="col-2-3 col-left">
			<!-- Main Column -->
			
			<div id="lectures-slider" class="flexslider">
				<ul class="slides">
					<?php
					$term =	$wp_query->queried_object;
					$lecturers = array();

					query_posts( array( 'post_type' => 'lecturer', 'showposts' => -1, 'tax_query' => array(
												array( 'taxonomy' => $term -> taxonomy, 'field' => 'slug', 'terms' => $term -> slug )
									)));

					// query_posts( array( 'post_type' => 'lecturer', 'tax_query' => array(
					// 							array( 'taxonomy' => 'lecturer-subcat', 'field' => 'slug', 'terms' => $subcat -> slug ),
					// 							array( 'taxonomy' => 'faculty', 'field' => 'slug', 'terms' => $faculty -> slug ),
					// 				)));

					if(have_posts()) : while(have_posts()) : the_post();
					   // $terms2 = get_the_terms( get_the_ID(), 'faculty' ); 
						  //   foreach($terms2 as $term2) {
						  //     echo $term2->name;
						  //   }
						$thumb = '';
						$img = types_render_field("photo", array("raw"=>"true"));
						if ( $img ) $thumb = get_site_url() . "/wp-content/uploads/staff-photos/" . $img;
						// if ( has_post_thumbnail() ) $thumb = get_the_post_thumbnail('medium');
						if ( has_post_thumbnail() ) {
						 $thumb = get_the_post_thumbnail(get_the_ID(),array(140,140));}
						else { $thumb = '<img width="140" height="140" src="http://www.ono-new.c14.co.il/wp-content/uploads/2015/01/lecturers.png" alt="lecturers">';
					}
						
						if ( $thumb )
							$lecturers[] = array(
												'ID' => get_the_ID(),
												'thumbnail' => $thumb,
												'title' => get_the_title(),
												'position' => types_render_field("position", array("raw"=>"true")),
												'permalink' => get_the_permalink(),
												'content' => wp_trim_words( strip_tags(get_the_content()), 50 )
												);
					?>
					
						<?php if ( $thumb ): ?>
							<li>
								<div class="lecturer-wrap">
									<a href="#" class="slider-link" onClick="show_lecturer(<?php echo get_the_ID(); ?>);">
										<?php echo $thumb; ?>
										<div class="lecturer-name">
											<?php the_title(); ?>
										</div>
									</a>
								</div>
							</li>
						<?php endif; ?>
					
					<?php
					endwhile;
					endif;
					wp_reset_query();
					?>
				</ul>
			</div>
			
			<?php foreach( $lecturers as $lec ): ?>
				<div class="lecturer-excerpt row" id="lecturer-<?php echo $lec['ID'] ?>">
					<div class="col-1-3 col-right">
						<div class="thumbnail">
							<?php echo $lec['thumbnail']; ?>
						</div>
					</div>
					<div class="col-2-3 col-left">
						<h2><?php echo $lec['title']; ?></h2>
						<div class="subtitle"><?php echo $lec['position']; ?></div>
						<?php echo $lec['content']; ?>
						<div class="row">
							<a href="<?php echo $lec['permalink']; ?>" class="show-more icon-gt"><? _e("To profile page","ono"); ?></a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			
		</div>
	</div>
	
</div>










<?php get_footer(); ?>






