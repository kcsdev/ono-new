



<div class="footer row">
	<div id="footer-contact">
		<div class="container row">
			<div class="title"><? _e("So what are you wating for?","ono"); ?></div>
			

			<?php 
				if (ICL_LANGUAGE_CODE == 'en') { 
					echo do_shortcode('[contact-form-7 id="33719" title="Contact Form - Footer - ENGLISH"]');
				}
				else { 
					echo do_shortcode('[contact-form-7 id="10431" title="Contact Form - Footer"]'); 
				}
			?>
            


		</div>
	</div>

	<div id="footer-widgets">
		<div class="container row">
			<div class="col-1-6">
				<div class="widgettitle"><? _e("Categories","ono"); ?></div>
				<?php dynamic_sidebar( 'footer-menu-1' ); ?>
			</div>
			<div class="col-1-6">
				<div class="widgettitle">&nbsp;</div>
				<?php dynamic_sidebar( 'footer-menu-2' ); ?>
			</div>
			<div class="col-1-6">
				<div class="widgettitle">&nbsp;</div>
				<?php dynamic_sidebar( 'footer-menu-3' ); ?>
			</div>
			<div class="col-1-6">
				<div class="widgettitle">&nbsp;</div>
				<?php dynamic_sidebar( 'footer-menu-4' ); ?>
			</div>
			<div class="col-1-6">
				<div class="widgettitle">&nbsp;</div>
				<?php dynamic_sidebar( 'footer-menu-5' ); ?>
			</div>
			<div class="col-1-6">
				<div id="social-links-container">
					<div class="widgettitle"><? _e("Join us","ono"); ?></div>
					<ul id="social-links-list">
						<li><a href="<?php echo get_option('ono_facebook'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-footer-facebook.png" /></a></li>
						<li><a href="<?php echo get_option('ono_twitter'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-footer-twitter.png" /></a></li>
						<li><a href="<?php echo get_option('ono_youtube'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-footer-youtube.png" /></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="copyright row">
	<div class="container">
		<div class="col-right">
			&copy;
			<? _e("Copyright - All Rights Reserved - Ono Academic College","ono"); ?>
		</div>
        <div class="col-left">
			<a href="#">
				<img id="kcs-logo" src="<?php echo get_template_directory_uri(); ?>/img/kcs-logo.png" />
			</a>
		</div>
	</div>
</div>

<?php wp_footer(); ?>

</body>
</html>