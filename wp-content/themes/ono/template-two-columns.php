<?php
/*
Template Name: Two columns Page
*/
?>


<?php get_header(); ?>






<div class="thumbnail-container">
	<?php include_once('incl/lecturer-top-thumbnail.php'); ?>
	<?php 
		if ( has_post_thumbnail() ) the_post_thumbnail('full');
			else echo '<img src="' . get_template_directory_uri() . '/img/general.jpg" />';
	?>
	
	<div id="main-title-container">
		<?php /*<h1 id="main-title">
			<?php the_title(); ?>
		</h1> */ ?>
	</div>
</div>



<div class="container">
	<?php include_once('breadcrumbs.php'); ?>
	
	
	<div class="row">
		<div id="main-content" class="col-2-3 col-right">
			
			<h1 class="subtitle">
				<?php the_title(); ?>
			</h1>

			<div class="excerpt">
				<?php the_excerpt(); ?>
			</div>
			
			<div class="info">
				<span class="author">
					<?php echo get_the_author(); ?>
				</span>
				<span class="date">
					<?php the_date(); ?>
				</span>
			</div>
			
			<div class="content">
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
				
				<?php the_content(); ?>
					<div class="print">
						<?php echo do_shortcode( '[print-me]' ) ?>
					</div>
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
		<div class="col-1-3 col-left">
			
			<div id="sidebar">

				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
	
</div>




<?php get_footer(); ?>


