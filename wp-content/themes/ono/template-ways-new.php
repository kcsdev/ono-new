<?php
/*
Template Name: New Ways Page
*/
?>


<?php get_header(); ?>




<!------------------------* SLIDER *--------------------------->

<div class="thumbnail-container">
	
	<?php 
		if ( has_post_thumbnail() ) the_post_thumbnail('full');
			else echo '<img src="' . get_template_directory_uri() . '/img/contact-01.jpg" />';
	?>
	
	<div id="main-title-container">
		<h1 id="main-title">
			<?php the_title(); ?>
		</h1>
	</div>
</div>

<!------------------------* CONTENT *--------------------------->

<div class="container">
	<?php include_once('breadcrumbs.php'); ?>
	
	<div class="row">
		<div id="main-content" class="col-2-3 col-right">
			<!---------------- Main Column ----------------------------------->
			
			<!---------------- Kiriat Ono ----------------------------------->
			<h2 class="subtitle">
				<?php _e('Campus Kiriat Ono', 'ono'); ?>
			</h2>

			<div class="row">
				<div id="tabs-ono" class="tabs">
					<ul>
						<li><a href="#tabs-1"><?php echo (trim(get_field('map-link-ono-title'))!='' ? get_field('map-link-ono-title') : _e('Address', 'ono'));?></a></li>
						<li><a href="#tabs-2"><?php echo (trim(get_field('transport-ono-title'))!='' ? get_field('transport-ono-title') : _e('Public transport', 'ono'));?></a></li>
						<li><a href="#tabs-3"><?php echo (trim(get_field('parking-ono-title'))!='' ? get_field('parking-ono-title') : _e('Parking', 'ono'));?></a></li>
						<li><a href="#tabs-4"><?php echo (trim(get_field('category-ono-title'))!='' ? get_field('category-ono-title') : _e('Additional categories', 'ono'));?></a></li>
					</ul>
					<div id="tabs-1">
						<iframe src="<?php echo get_field('map-link-ono'); ?>" width="100%" height="400" frameborder="0" style="border:0"></iframe>
					</div>
					<div id="tabs-2">
						<div class="transport-table-wrap">
							<?php echo get_field('transport-ono'); ?>
						</div>
					</div>
					<div id="tabs-3">
						<?php echo get_field('parking-ono'); ?>
					</div>
					<div id="tabs-4">
						<?php echo get_field('category-ono'); ?>
					</div>
				</div>
			</div>
			
			
			
			<!---------------- Or Yehuda ----------------------------------->
			<h2 class="subtitle">
				<?php _e('Campus Or Yehuda', 'ono'); ?>
			</h2>
		
			<div class="row">
				<div id="tabs-or-yehuda" class="tabs">
					<ul>
						
						<li><a href="#tabs-1"><?php echo (trim(get_field('map-link-ehuda-title'))!='' ? get_field('map-link-ehuda-title') : _e('Address', 'ono'));?></a></li>
						<li><a href="#tabs-2"><?php echo (trim(get_field('transport-ehuda-title'))!='' ? get_field('transport-ehuda-title') : _e('Public transport', 'ono'));?></a></li>
						<li><a href="#tabs-3"><?php echo (trim(get_field('parking-ehuda-title'))!='' ? get_field('parking-ehuda-title') : _e('Parking', 'ono'));?></a></li>
						<li><a href="#tabs-4"><?php echo (trim(get_field('category-ehuda-title'))!='' ? get_field('category-ehuda-title') : _e('Additional categories', 'ono'));?></a></li>
					</ul>
					<div id="tabs-1">
						<iframe src="<?php echo get_field('map-link-ehuda'); ?>" width="100%" height="400" frameborder="0" style="border:0"></iframe>
					</div>
					<div id="tabs-2">
						<div class="transport-table-wrap">
							<?php echo get_field('transport-ehuda'); ?>
						</div>
					</div>
					<div id="tabs-3">
						<?php echo get_field('parking-ehuda'); ?>
					</div>
					<div id="tabs-4">
						<?php echo get_field('category-ehuda'); ?>
					</div>
				</div>
			</div>
			
			<!---------------- Jerusalem ----------------------------------->
			<h2 class="subtitle">
                <?php if(ICL_LANGUAGE_CODE==en){ _e('Campus Jerusalem', 'ono'); }else{echo 'קמפוס ירושלים כללי'; }  ?> 
			</h2>
		
			<div class="row">
				<div id="tabs-Jerusalem" class="tabs">
					<ul>
						
						<li><a href="#tabs-1"><?php echo (trim(get_field('map-link-Jerusalem-title'))!='' ? get_field('map-link-Jerusalem-title') : _e('Address', 'ono'));?></a></li>
						<li><a href="#tabs-2"><?php echo (trim(get_field('transport-Jerusalem-title'))!='' ? get_field('transport-Jerusalem-title') : _e('Public transport', 'ono'));?></a></li>
						<li><a href="#tabs-3"><?php echo (trim(get_field('parking-Jerusalem-title'))!='' ? get_field('parking-Jerusalem-title') : _e('Parking', 'ono'));?></a></li>
						<li><a href="#tabs-4"><?php echo (trim(get_field('category-Jerusalem-title'))!='' ? get_field('category-Jerusalem-title') : _e('Additional categories', 'ono'));?></a></li>
					</ul>
					<div id="tabs-1">
						<iframe src="<?php echo get_field('map-link-Jerusalem'); ?>" width="100%" height="400" frameborder="0" style="border:0"></iframe>
					</div>
					<div id="tabs-2">
						<div class="transport-table-wrap">
							<?php echo get_field('transport-Jerusalem'); ?>
						</div>
					</div>
					<div id="tabs-3">
						<?php echo get_field('parking-Jerusalem'); ?>
					</div>
					<div id="tabs-4">
						<?php echo get_field('category-Jerusalem'); ?>
					</div>
				</div>
			</div>

		</div>
		
                <div class="col-1-3 col-left">

			<!---------------- Sidebar ---------------------------------------->
			<div id="sidebar">
				
				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
	
</div>

<style>
.tabs > div {
    min-height: 0;
    background: #F5F5F9;
}
.transport-table-wrap {
    background: #F5F5F9;
    padding: 0;
}
</style>


<?php get_footer(); ?>


