<?php
/**
 * Learning Program Widget Class
 */
class inart_learning_program_widget extends WP_Widget {
 
 
    /** constructor -- name this the same as the class above */
    function __construct() {
        parent::__construct(
			// Base ID of your widget
			'inart_learning_program_widget', 

			// Widget name will appear in UI
			__('ONO Learning Program Widget'), 

			// Widget description
			array( 'description' => '' ) 
			);
    }
 
    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {	
        extract( $args );
		$program_file_id 	= get_post_meta( get_the_ID(), 'program-file', true);
		$image 	= $instance['image'];
		$title 	= $instance['title'];
        ?>
              <?php echo $before_widget; ?>
			  
			  <?php if($program_file_id): ?>
					<div class="inart-learning-program-widget">
						<div class="thumbnail">
							<img src="<?php echo get_template_directory_uri(); ?>/img/booklet.png" />
						</div>
						<div class="button">
							<?php echo wp_get_attachment_link($program_file_id, '', '', '', $title); ?>
						</div>
					</div>
			<?php endif; ?>
			
            <?php echo $after_widget; ?>
        <?php
    }
 
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {		
		$instance = $old_instance;
		$instance['image'] = strip_tags($new_instance['image']);
		$instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
 
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {
		$image	= esc_attr($instance['image']);
		$title = esc_attr($instance['title']);
        ?>
		<p>
			<label for="<?php echo $this->get_field_id('image'); ?>"><?php _e('Image'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('image'); ?>" name="<?php echo $this->get_field_name('image'); ?>" type="text" value="<?php echo $image; ?>" />
        </p>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <?php 
    }
 
 
} // end class inart_learning_program_widget
add_action('widgets_init', create_function('', 'return register_widget("inart_learning_program_widget");'));
?>