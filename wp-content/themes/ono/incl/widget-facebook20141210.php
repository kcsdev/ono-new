<?php
/**
 * Facebook Widget Class
 */
class inart_facebook_widget extends WP_Widget {
 
 
    /** constructor -- name this the same as the class above */
    function __construct() {
        parent::__construct(
			// Base ID of your widget
			'inart_facebook_widget', 

			// Widget name will appear in UI
			__('ONO Facebook Widget'), 

			// Widget description
			array( 'description' => '' ) 
			);
    }
 
    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {	
        extract( $args );
		$facebook_url 	= $instance['facebook_url'];
        ?>
              <?php echo $before_widget; ?>
                <div class="inart-facebook-widget">
					<div class="fb-like-box" data-href="<?php echo $facebook_url ?>" data-width="235" data-height="290" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
				</div>
              <?php echo $after_widget; ?>
        <?php
    }
 
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {		
		$instance = $old_instance;
		$instance['facebook_url'] = strip_tags($new_instance['facebook_url']);
        return $instance;
    }
 
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {
		$facebook_url	= esc_attr($instance['facebook_url']);
        ?>
		<p>
			<label for="<?php echo $this->get_field_id('facebook_url'); ?>"><?php _e('Facebook'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('facebook_url'); ?>" name="<?php echo $this->get_field_name('facebook_url'); ?>" type="text" value="<?php echo $facebook_url; ?>" />
        </p>
        <?php 
    }
 
 
} // end class inart_facebook_widget
add_action('widgets_init', create_function('', 'return register_widget("inart_facebook_widget");'));
?>