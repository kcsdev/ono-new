<div class="share-block">
	<?php
		$link = get_permalink( get_the_ID() );
		$twit = get_the_title() . ' ' . $link;
	?>
	<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/share-facebook.jpg" class="icon-share" /></a>
	<a href="https://twitter.com/home?status=<?php echo $twit ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/share-twitter.jpg" class="icon-share" /></a>
	<a href="https://plus.google.com/share?url=<?php echo $link ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/share-google-plus.jpg" class="icon-share" /></a>
	<a href="<?php echo $link; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/share-link.jpg" class="icon-share" /></a>
</div>