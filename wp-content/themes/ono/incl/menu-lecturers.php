<ul class="menu">
	<?php 
		$lecturer_faculty_list = get_terms( 'faculty', 'hide_empty=0' );
		$lecturer_subcat_list = get_terms( 'lecturer-subcat', 'hide_empty=0' );
        $new_arr = array();
foreach( $lecturer_faculty_list as $faculty ){
    if ($faculty->term_taxonomy_id != 1350) {
        $new_arr[]=$faculty;
    };
}
foreach( $lecturer_faculty_list as $faculty ){
    if ($faculty->term_taxonomy_id == 1350) {
        $new_arr[]=$faculty;
    };
}
      $lecturer_faculty_list =   $new_arr;
    
	?>
		
    <?php foreach( $lecturer_faculty_list as $faculty ):  /*echo $faculty->term_taxonomy_id;*/?>
        
		<li class="page_item_has_children">
			<a href="<?php echo get_term_link($faculty); ?>"><?php echo $faculty -> name ?></a>
			<ul class="children">
				<?php foreach( $lecturer_subcat_list as $subcat ): ?>
							<?php
                                
								if ($subcat->slug == 'senior-lecturer'){ continue; }?>
							<?php
								$lecturers = array();
								query_posts( array( 'post_type' => 'lecturer', 'tax_query' => array(
															array( 'taxonomy' => 'lecturer-subcat', 'field' => 'slug', 'terms' => $subcat -> slug ),
															array( 'taxonomy' => 'faculty', 'field' => 'slug', 'terms' => $faculty -> slug ),
												)));
								if(have_posts()) : while(have_posts()) : the_post();

									$lecturers[] = (object) array(
															'title' => get_the_title(),
															'permalink' => get_permalink()
														);

								endwhile;
								endif;
								wp_reset_query();
							?>
					<?php if( count( $lecturers ) ): ?>
						<li class="page_item_has_children">
							<a href="<?php echo get_term_link($subcat); ?>"><?php echo $subcat -> name ?></a>
								<ul class="children">
									<?php foreach( $lecturers as $lec ): ?>
                                      
                                    
										<li><a href="<?php echo $lec -> permalink ?>"><?php echo $lec -> title ?></a></li>
									<?php endforeach; ?>
								</ul>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
				
			</ul>
		</li>
	<?php endforeach; ?>
		
</ul>