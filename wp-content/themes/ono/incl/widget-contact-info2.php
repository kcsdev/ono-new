<?php
/**
 * Contact Info Widget Class
 */
class inart_contact_info_widget extends WP_Widget {
 
 
    /** constructor -- name this the same as the class above */
    function __construct() {
        parent::__construct(
			// Base ID of your widget
			'inart_contact_info_widget', 

			// Widget name will appear in UI
			__('ONO Contact Info Widget'), 

			// Widget description
			array( 'description' => '' ) 
			);
    }
 
    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {	
        extract( $args );
		$title 	= $instance['title'];
		$address 	= $instance['address'];
		$phones 	= $instance['phones'];
		$facebook_url = $instance['facebook_url'];
        ?>
              <?php echo $before_widget; ?>
				<div id="sidebar-contact-list-widget">
					<h4 class="widgettitle">
						<?php echo $title; ?>
					</h4>
					<div class="subtitle icon icon-map-pin">כתובת</div>
					<div class="content">
						<?php echo $address ?>
					</div>
					<div class="subtitle icon icon-phone">טלפונים</div>
					<div class="content">
						<?php echo nl2br($phones); ?>
					</div>
					<div class="content icon icon-facebook">
						<a href="<?php echo $facebook_url; ?>" target="_blank">
							האקדמית אונו בפייסבוק
						</a>
					</div>
				</div>
              <?php echo $after_widget; ?>
        <?php
    }
 
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {		
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['address'] = strip_tags($new_instance['address']);
		$instance['phones'] = strip_tags($new_instance['phones']);
		$instance['facebook_url'] = strip_tags($new_instance['facebook_url']);
        return $instance;
    }
 
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {
		$title	= esc_attr($instance['title']);
		$address = esc_attr($instance['address']);
		$phones = esc_attr($instance['phones']);
		$facebook_url	= esc_attr($instance['facebook_url']);
        ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" type="text" value="<?php echo $address; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('phones'); ?>"><?php _e('Phones'); ?>:</label> 
			<textarea class="widefat" id="<?php echo $this->get_field_id('phones'); ?>" name="<?php echo $this->get_field_name('phones'); ?>"><?php echo $phones; ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('facebook_url'); ?>"><?php _e('Facebook'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('facebook_url'); ?>" name="<?php echo $this->get_field_name('facebook_url'); ?>" type="text" value="<?php echo $facebook_url; ?>" />
        </p>
        <?php 
    }
 
 
} // end class inart_contact_info_widget
add_action('widgets_init', create_function('', 'return register_widget("inart_contact_info_widget");'));
?>