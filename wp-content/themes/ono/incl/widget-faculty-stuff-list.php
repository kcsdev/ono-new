<?php
/**
 * Faculty Stuff List Widget Class
 */
class inart_faculty_stuff_list_widget extends WP_Widget {
 
 
    /** constructor -- name this the same as the class above */
    function __construct() {
        parent::__construct(
			// Base ID of your widget
			'inart_faculty_stuff_list_widget', 

			// Widget name will appear in UI
			__('ONO Faculty Stuff LIST Widget'), 

			// Widget description
			array( 'description' => '' ) 
			);
    }
 
    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {	
        extract( $args );
		$faculty_term_id 	= get_post_meta( get_the_ID(), 'lecturer-faculty', true);
		$faculty_term = get_term_by('id', $faculty_term_id, 'faculty');
		$faculty_term_slug = $faculty_term -> slug;
		$title 	= $instance['title'];
		$posts_num = intval($instance['posts_num']) > 0 ? intval($instance['posts_num']) : -1;
        ?>
		<?php if ( $faculty_term_id ): ?>
              <?php echo $before_widget; ?>
                <div class="inart-faculty-stuff-list-widget">

					<div class="title"><?php echo $title; ?></div>
					<?php
						$lecturers = array();
						query_posts( array( 'post_type' => 'lecturer', 'showposts' => -1, 'tax_query' => array(
													array( 'taxonomy' => 'faculty', 'field' => 'slug', 'terms' => $faculty_term_slug )
										)));
						if(have_posts()) : while(have_posts()) : the_post();
							
							$lecturer_id = get_the_ID();
							$img = get_post_meta($lecturer_id, 'wpcf-photo', true);
							if ( $img ) $thumb = get_site_url() . "/wp-content/uploads/staff-photos/" . $img;
							if ( has_post_thumbnail( $lecturer_id ) ) $thumb = get_the_post_thumbnail('thumbnail', $lecturer_id);

							if ( $thumb && 
										( count($lecturers) < $posts_num || $posts_num == -1 ) ) {
								$lecturers[] = array('title' => get_the_title(),
															'permalink' => get_permalink(),
															'thumbnail' => $thumb,
															'position' => get_post_meta($lecturer_id, 'wpcf-position', true));
							}
							
						endwhile;
						endif;
						wp_reset_query();
					?>
				
					<?php foreach($lecturers as $lecturer): ?>
						<div class="row lecturer">
							<div class="col-right col-1-3">
								<div class="thumbnail">
									<a href="<?php echo $lecturer['permalink']; ?>">
										<?php echo "<img src='" . $lecturer['thumbnail'] . "' />"; ?>
									</a>
								</div>
							</div>
							<div class="col-left col-2-3">
								<div class="name">
									<a href="<?php echo $lecturer['permalink']; ?>">
										<?php echo $lecturer['title']; ?>
									</a>
								</div>
								<div class="position">
										<?php echo $lecturer['position']; ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				

					<div class="row more-link">
						<a href="<?php echo get_term_link( $faculty_term_slug, 'faculty' ); ?>" class="show-more icon-plus">לרשימה מלאה</a>
					</div>
				</div>
              <?php echo $after_widget; ?>
			  
			 <?php endif; ?>
        <?php
    }
 
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {		
		$instance = $old_instance;
		$instance['faculty_slug'] = strip_tags($new_instance['faculty_slug']);
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['posts_num'] = strip_tags($new_instance['posts_num']);
        return $instance;
    }
 
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {
		$faculty_slug	= esc_attr($instance['faculty_slug']);
		$title = esc_attr($instance['title']);
		$posts_num = esc_attr($instance['posts_num']);
        ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
		<p>
			<label for="<?php echo $this->get_field_id('posts_num'); ?>"><?php _e('Posts number'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('posts_num'); ?>" name="<?php echo $this->get_field_name('posts_num'); ?>" type="text" value="<?php echo $posts_num; ?>" />
        </p>
        <?php 
    }
 
 
} // end class inart_faculty_stuff_list_widget
add_action('widgets_init', create_function('', 'return register_widget("inart_faculty_stuff_list_widget");'));
?>