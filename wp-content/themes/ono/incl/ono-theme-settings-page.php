<?php

add_action('admin_menu', 'inart_ono_create_settings_page');

function inart_ono_create_settings_page() {
	//create a submenu under Settings
	add_options_page( 
		__( 'Ono Website Settings Page', 'ono' ),
		__( 'Ono Website Settings', 'ono' ),
		'manage_options',
		__FILE__,
		'inart_ono_display_settings'
		);
}

//-----------------> Register Custom Settings <---------------------------
if ( is_admin() )
	add_action( 'admin_init', 'register_inart_ono_settings' );

function register_inart_ono_settings() {
	register_setting( 'inart-ono-settings-group', 'ono_facebook' );
	register_setting( 'inart-ono-settings-group', 'ono_google_plus' );
	register_setting( 'inart-ono-settings-group', 'ono_twitter' );
	register_setting( 'inart-ono-settings-group', 'ono_youtube' );
	
	register_setting( 'inart-ono-settings-group', 'ono_registers_link' );
	register_setting( 'inart-ono-settings-group', 'ono_students_link' );
	register_setting( 'inart-ono-settings-group', 'ono_graduate_link' );
	register_setting( 'inart-ono-settings-group', 'ono_graduate_link_en' );
	register_setting( 'inart-ono-settings-group', 'ono_staff_link' );
	
	register_setting( 'inart-ono-settings-group', 'ono_main_phone_num' );
	
	register_setting( 'inart-ono-settings-group', 'ono_chat_link' );
	register_setting( 'inart-ono-settings-group', 'ono_chat_link_en' );
	register_setting( 'inart-ono-settings-group', 'ono_phone_list' );
	register_setting( 'inart-ono-settings-group', 'ono_phone_list_en' );
	register_setting( 'inart-ono-settings-group', 'ono_directions' );
	register_setting( 'inart-ono-settings-group', 'ono_directions_en' );


	//add_settings_section();
}

function inart_ono_display_settings() {
	global $title;
	
	?>
	<br />
	<h2><?php echo $title;?></h2>
	<br />
	
	<form method="POST" action="options.php">
		<?php settings_fields( 'inart-ono-settings-group' ); ?>
		<?php do_settings_sections( 'inart-ono-settings-group' ); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><label for="ono_facebook"><?php _e( 'Facebook link', 'ono' ); ?></label></th>
				<td><input size="50" name="ono_facebook" value="<?php echo get_option('ono_facebook'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_google_plus"><?php _e( 'Google Plus link', 'ono' ); ?></label></th>
				<td><input size="50" name="ono_google_plus" value="<?php echo get_option('ono_google_plus'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_twitter"><?php _e( 'Twitter link', 'ono' ); ?></label></th>
				<td><input size="50" name="ono_twitter" value="<?php echo get_option('ono_twitter'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_youtube"><?php _e( 'Youtube link', 'ono' ); ?></label></th>
				<td><input size="50" name="ono_youtube" value="<?php echo get_option('ono_youtube'); ?>" /></td>
			</tr>			
		</table>
		
		<hr />
		
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><label for="ono_main_phone_num"><? _e("Main phone number","ono"); ?></label></th>
				<td><input size="50" name="ono_main_phone_num" value="<?php echo get_option('ono_main_phone_num'); ?>" /></td>
			</tr>
		</table>
		
		<hr />
		
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><label for="ono_registers_link"><? _e("Registers enter link","ono"); ?></label></th>
				<td><input size="50" name="ono_registers_link" value="<?php echo get_option('ono_registers_link'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_students_link"><? _e("Students enter link","ono"); ?></label></th>
				<td><input size="50" name="ono_students_link" value="<?php echo get_option('ono_students_link'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_graduate_link"><? _e("Graduates","ono"); ?></label></th>
				<td><input size="50" name="ono_graduate_link" value="<?php echo get_option('ono_graduate_link'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_graduate_link_en"><? _e("Graduates - English","ono"); ?></label></th>
				<td><input size="50" name="ono_graduate_link_en" value="<?php echo get_option('ono_graduate_link_en'); ?>" /></td>
			</tr>

			<tr valign="top">
				<th scope="row"><label for="ono_staff_link"><? _e("Link entrance of Staff","ono"); ?></label></th>
				<td><input size="50" name="ono_staff_link" value="<?php echo get_option('ono_staff_link'); ?>" /></td>
			</tr>
		</table>
		
		<hr />
		
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><label for="ono_chat_link"><? _e("Chat with representative","ono"); ?></label></th>
				<td><input size="50" name="ono_chat_link" value="<?php echo get_option('ono_chat_link'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_chat_link_en"><? _e("Chat with representative - English","ono"); ?></label></th>
				<td><input size="50" name="ono_chat_link_en" value="<?php echo get_option('ono_chat_link_en'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_phone_list"><? _e("Phones list","ono"); ?></label></th>
				<td><input size="50" name="ono_phone_list" value="<?php echo get_option('ono_phone_list'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_phone_list_en"><? _e("Phones list - English","ono"); ?></label></th>
				<td><input size="50" name="ono_phone_list_en" value="<?php echo get_option('ono_phone_list_en'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_directions"><? _e("Directions","ono"); ?></label></th>
				<td><input size="50" name="ono_directions" value="<?php echo get_option('ono_directions'); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="ono_directions_en"><? _e("Directions - English","ono"); ?></label></th>
				<td><input size="50" name="ono_directions_en" value="<?php echo get_option('ono_directions_en'); ?>" /></td>
			</tr>
		</table>
		
		<table class="form-table">
			<tr valign="top">
				<?php submit_button(); ?>
			</td>
		</table>
	</form>
	

	
	<?php
}








