<?php
/**
 * News Widget Class
 */
class inart_news_widget extends WP_Widget {
 
 
    /** constructor -- name this the same as the class above */
    function __construct() {
        parent::__construct(
			// Base ID of your widget
			'inart_news_widget', 

			// Widget name will appear in UI
			__('ONO News Widget'), 

			// Widget description
			array( 'description' => '' ) 
			);
    }
 
    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {	
        extract( $args );
		$title	= $instance['title'];
		$posts_num = intval($instance['posts_num']) > 0 ? intval($instance['posts_num']) : -1 ;
        ?>
              <?php echo $before_widget; ?>
				<div id="news-list-widget">
					<h4 class="widgettitle">
						<?php echo $title; ?>
					</h4>
					<div class="events-wrap">
							<?php
								query_posts(array(
													'showposts' => $posts_num,
													'post_type' => 'news',
													'post_status' => 'publish'
														));
								if ( have_posts() ): while ( have_posts() ): the_post();
							?>
						<div class="row event">
							<div class="col-right col-1-3">
								<div class="thumbnail">
									<a href="<?php echo get_permalink(); ?>">
										<?php the_post_thumbnail('thumbnail'); ?>
									</a>
								</div>
							</div>
							<div class="col-left col-2-3">
								<div class="title">
									<a href="<?php echo get_permalink(); ?>">
										<?php the_title(); ?>
									</a>
								</div>
								<div class="date">
									<?php 
										echo types_render_field("date", array("format"=>"M d"));
										$time_start = types_render_field("time-start", array("format"=>"M d"));
										if ( $time_start ) echo ' | ' . $time_start;
									?>
								</div>
							</div>
						</div>
							<?php
								endwhile;
								endif;
								wp_reset_query();
							?>
					</div>
					<div class="row more-link">
						<a href="<?php echo get_post_type_archive_link('news'); ?>" class="show-more icon-gt">עוד כתבות</a>
					</div>
				</div>
              <?php echo $after_widget; ?>
        <?php
    }
 
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {		
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['posts_num'] = strip_tags($new_instance['posts_num']);
        return $instance;
    }
 
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {
		$title	= esc_attr($instance['title']);
		$posts_num	= esc_attr($instance['posts_num']);
        ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
		<p>
			<label for="<?php echo $this->get_field_id('posts_num'); ?>"><?php _e('Number of posts'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('posts_num'); ?>" name="<?php echo $this->get_field_name('posts_num'); ?>" type="text" value="<?php echo $posts_num; ?>" />
        </p>
        <?php 
    }
 
 
} // end class inart_news_widget
add_action('widgets_init', create_function('', 'return register_widget("inart_news_widget");'));
?>