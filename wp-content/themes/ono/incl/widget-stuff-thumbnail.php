<?php
/**
 * Stuff Thumbnail Widget Class
 */
class inart_stuff_thumbnail_widget extends WP_Widget {
 
 
    /** constructor -- name this the same as the class above */
    function __construct() {
        parent::__construct(
			// Base ID of your widget
			'inart_stuff_thumbnail_widget', 

			// Widget name will appear in UI
			__('ONO Stuff Thumbnail Widget'), 

			// Widget description
			array( 'description' => '' ) 
			);
    }
 
    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {	
        extract( $args );
		$stuff_id 	= $instance['stuff_id'];
		$title 	= $instance['title'];
        ?>
              <?php echo $before_widget; ?>
                <div class="inart-stuff-thumb-widget">
					<div class="title"><?php echo $title; ?></div>
					<?php
						$thumb = '';
						$img = get_post_meta($stuff_id, 'wpcf-photo', true);
						if ( $img ) $thumb = get_site_url() . "/wp-content/uploads/staff-photos/" . $img;
						if ( has_post_thumbnail($stuff_id) ) $thumb = get_the_post_thumbnail($stuff_id, 'small');
						$post = get_post( $stuff_id );
					?>
					<a href="<?php echo get_permalink($stuff_id); ?>">
						<div class="thumbnail">
							<?php //echo "<img src='" . $thumb . "' />"; ?>
							<?php echo $thumb; ?>
						</div>
						<div class="name"><?php echo $post -> post_title; ?></div>
					</a>
					<div class="position"><?php echo get_post_meta($stuff_id, 'wpcf-position', true); ?></div>
				</div>
              <?php echo $after_widget; ?>
        <?php
    }
 
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {		
		$instance = $old_instance;
		$instance['stuff_id'] = strip_tags($new_instance['stuff_id']);
		$instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
 
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {
		$stuff_id	= esc_attr($instance['stuff_id']);
		$title = esc_attr($instance['title']);
        ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
		<p>
			<select id="<?php echo $this->get_field_id('stuff_id'); ?>" name="<?php echo $this->get_field_name('stuff_id'); ?>">
			<?php
				query_posts( array( 'post_type' => 'lecturer', 'showposts' => -1 ) );
				if(have_posts()) : while(have_posts()) : the_post();
					$post_id = get_the_ID();
			?>
				<option value="<?php echo $post_id; ?>" <?php if($post_id == $stuff_id) echo "selected"; ?>><?php the_title(); ?></option>
			<?php
				endwhile;
				endif;
				wp_reset_query();
			?>
			</select>
		</p>
        <?php 
    }
 
 
} // end class inart_stuff_thumbnail_widget
add_action('widgets_init', create_function('', 'return register_widget("inart_stuff_thumbnail_widget");'));
?>