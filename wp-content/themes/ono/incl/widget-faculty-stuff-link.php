<?php
/**
 * Faculty Stuff Link Widget Class
 */
class inart_faculty_stuff_link_widget extends WP_Widget {
 
 
    /** constructor -- name this the same as the class above */
    function __construct() {
        parent::__construct(
			// Base ID of your widget
			'inart_faculty_stuff_link_widget', 

			// Widget name will appear in UI
			__('ONO Faculty Stuff Link Widget'), 

			// Widget description
			array( 'description' => '' ) 
			);
    }
 
    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {	
        extract( $args );
		$faculty_slug 	= $instance['faculty_slug'];
		$title 	= $instance['title'];
        ?>
              <?php echo $before_widget; ?>
                <div class="inart-faculty-stuff-link-widget">
					<div class="title">
						<a href="<?php echo esc_attr(get_term_link($faculty_slug, 'faculty')); ?>"><?php echo $title; ?></a>
					</div>
				</div>
              <?php echo $after_widget; ?>
        <?php
    }
 
    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {		
		$instance = $old_instance;
		$instance['faculty_slug'] = strip_tags($new_instance['faculty_slug']);
		$instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
 
    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {
		$faculty_slug	= esc_attr($instance['faculty_slug']);
		$title = esc_attr($instance['title']);
        ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label> 
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
		<p>
			<select id="<?php echo $this->get_field_id('faculty_slug'); ?>" name="<?php echo $this->get_field_name('faculty_slug'); ?>">
			<?php
				$faculties = get_terms( 'faculty', array(
										'orderby'    => 'count',
										'hide_empty' => 1
									) );
				if  ( !empty( $faculties ) && !is_wp_error( $faculties ) ):
					foreach ($faculties  as $faculty ):
			?>
				<option value="<?php echo $faculty -> slug; ?>" <?php if($faculty_slug == $faculty -> slug) echo "selected"; ?>><?php echo $faculty -> name; ?></option>
			<?php
				endforeach;
				endif;
			?>
			<!--
			<?php print_r($faculties); ?>
			-->
			</select>
		</p>
        <?php 
    }
 
 
} // end class inart_faculty_stuff_link_widget
add_action('widgets_init', create_function('', 'return register_widget("inart_faculty_stuff_link_widget");'));
?>