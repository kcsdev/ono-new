<?php
/*
Template Name: Three columns Page
*/
?>


<?php get_header(); ?>





<div class="thumbnail-container">
	<?php include_once('incl/lecturer-top-thumbnail.php'); ?>
	<?php 
if ( has_post_thumbnail() ) { the_post_thumbnail('full'); ?>
<?php }
			else echo '<img src="' . get_template_directory_uri() . '/img/slider-toar-rishon.jpg" alt="'.get_the_title().'"/>';
	?>
	<div id="main-title-container">
		<h1 id="main-title">
			<?php the_title(); ?>
		</h1>
</div> 
	
</div>


<div class="container">

<?php include_once('breadcrumbs.php'); ?>
	
	<div class="row">
		<div class="col-1-5 col-menu">
			<div id="page-side-menu">
				<div id="smartphone-submenu-btn">
					<img src="<?php echo get_template_directory_uri(); ?>/img/three-bars.png" />
					<p style="display:inline-block;"><?php _e('Menu of', 'ono'); ?> <?php the_title(); ?></p>
				</div>				

				<div id="page-side-menu-wrap">
					<ul class="menu">
						<?php 
							$children = wp_list_pages("title_li=&child_of=" . get_the_ID() . "&echo=0");
							if ($children){
								
							} else {
								$parent = wp_get_post_parent_id( get_the_ID() );
								$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");
							}
							echo $children;
						?>
					</ul>
				</div>
				
			</div>
		</div>
		<div id="main-content" class="col-3-5">
			
			<h1 class="event-subtitle">
				<?php the_title(); ?>
			</h1>
			
			<div class="content">
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
				
				<?php the_content(); ?>
					<div class="print">
						<?php echo do_shortcode( '[print-me]' ) ?>
					</div>
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
		<div class="col-1-5 col-sidebar">
			
			<div id="sidebar">
				
				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
	
</div>




<?php get_footer(); ?>