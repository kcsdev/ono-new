<?php get_header(); ?>




<!------------------------* SLIDER *--------------------------->

<div class="thumbnail-container">
	
	<?php 
		echo '<img src="' . get_template_directory_uri() . '/img/news_and_events.jpg" />';
	?>
	
	<div id="main-title-container">
		<h1 id="main-title">
			<?php _e('Events', 'ono'); ?>
		</h1>
	</div>
</div>

<!------------------------* CONTENT *--------------------------->

<div class="container">
	<?php include_once('breadcrumbs.php'); ?>
	
	<div class="row">
		<div id="main-content" class="col-2-3 col-right">
			<!---------------- Main Column ----------------------------------->
			<h2 class="subtitle">
				<?php the_title(); ?>
			</h2>
			
			<div class="info">
				<span class="date">
					<?php
						echo types_render_field("date", array('format' => 'l, d/n/Y') );

						$start_time = types_render_field("time-start", array('raw' => 'true') );
						$end_time = types_render_field("time-end", array('raw' => 'true') );
						if ($start_time) {
							echo ', ';
							echo $start_time . ' - ' . $end_time;
						}
					?>
				</span>
			</div>
			
			<div class="content">
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
				
				<?php include_once('incl/share-links.php'); ?>
				
				<?php
					if ( has_post_thumbnail() ) the_post_thumbnail('medium', array('class' => 'thumbnail') );
				?>
				
				<?php the_content(); ?>
				
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
		<div class="col-1-3 col-left">
			<!---------------- Sidebar ---------------------------------------->
			<div id="sidebar">
				
				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
	
</div>


<?php get_footer(); ?>


