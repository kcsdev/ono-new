<!DOCTYPE HTML>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html>
<head>
	<title>
	<?php wp_title( '|', 'true', 'right' );
		bloginfo( 'name' );
	?>
	</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="<?php bloginfo( 'description' ); ?>" />
	<meta name="keywords" content="" />
	<!--<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery-ui-1.10.4.custom.css" />-->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php wp_head(); ?>
</head>
<body <?php body_class($class); ?>>

<div id="top-green-line">
</div>

<div id="header" class="container row">
	<div class="header-right">
		<a href="/"><img id="header-logo" src="<?php echo get_template_directory_uri(); ?>/img/logo-header.jpg" /></a>
	</div>
	
	<div class="header-center">
		<div id="language-container" class="row">
			<a href="<? echo home_url(); ?>">עברית</a>
			<a href="<? echo get_site_url() . "/en"; ?>">אנגלית</a>
			<div class="text-size-icons">
				<a id="zoom-in" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-text-size-big.jpg" /></a>
				<a id="zoom-out" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-text-size-small.jpg" /></a>
			</div>
		</div>
		<div id="main-menu-container">
			<div id="smartphone-menu-btn">
				<img src="<?php echo get_template_directory_uri(); ?>/img/three-bars.png" />
			</div>
			<div class="row">
			
				<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'container_class' => 'top_menu',
						'container_id' => 'top_menu_id',
						'items_wrap' => '<ul class="%2$s">%3$s</ul>'
						)
					);
				?>
			
			</div>
		</div>
		<div id="search-container" class="row">
			<form>
				<input type="text" name="s" placeholder="<?php _e('Site search', 'ono'); ?>">
				<div id="search-button"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-search.png" /></div>
			</form>
		</div>
	</div>
	
	<div class="header-left">
		<div class="row private_menu_wrap">
			<div class="private_menu_title"><?php _e('Private area', 'ono'); ?></div>
			<div class="private_menu">
				<ul class="menu">
					<li><a href="<?php echo get_option('ono_students_link'); ?>" target="_blank"><?php _e('Students', 'ono'); ?></a></li>
					<li><a href="<?php echo get_option('ono_graduate_link'); ?>" target="_blank"><?php _e('Graduates', 'ono'); ?></a></li>
					<li><a href="<?php echo get_option('ono_staff_link'); ?>" target="_blank"><?php _e('Teachers', 'ono'); ?></a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div id="header-phone-number">	
				<?php echo get_option('ono_main_phone_num'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-right col-1-2">
				<ul class="top-small-menu"> 
					<li class="icon icon-man"><a href="<?php echo get_option('ono_chat_link'); ?>"><?php _e('Chat with representative', 'ono'); ?></a></li>
					<li class="icon icon-map-pin"><a href="<?php echo get_option('ono_directions'); ?>"><?php _e('Directions', 'ono'); ?></a></li>
				</ul>
			</div>
			<div class="col-left col-1-2">
				<ul class="top-small-menu">
					<li class="icon icon-phone"><a href="<?php echo get_option('ono_phone_list'); ?>"><?php _e('Phone list', 'ono'); ?></a></li>
					<li class="icon icon-facebook"><a href="<?php echo get_option('ono_facebook'); ?>"><?php _e('Facebook too', 'ono'); ?></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>