<?php
/*
Template Name: Contact Page
*/
?>


<?php get_header(); ?>





<div id="top-map" class="slider-container">
	
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3381.7264790151103!2d34.861711500000006!3d32.04959420000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151d4a7f6812baf1%3A0x1fc710cc0247dd50!2sTsahal+St+104!5e0!3m2!1sen!2s!4v1399810572370" width="100%" height="280" frameborder="0" style="border:0"></iframe>

	<div id="main-title-container">
		<h1 id="main-title">
			<?php the_title(); ?>
		</h1>
	</div>
</div>




<div class="container">
	<?php include_once('breadcrumbs.php'); ?>
	
	<div class="row">
		<div id="main-content" class="col-2-3 col-right">
			
			<h2 class="subtitle">
				<?php the_title(); ?>
			</h2>

			<div class="excerpt">
				<?php the_excerpt(); ?>
			</div>
			
			<div class="info">
				<span class="author">
					<?php echo get_the_author(); ?>
				</span>
				<span class="date">
					<?php the_date(); ?>
				</span>
			</div>
			
			<div class="content">
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
				
				<?php the_content(); ?>
					<div class="print">
						<?php echo do_shortcode( '[print-me]' ) ?>
					</div>
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
		<div class="col-1-3 col-left">
			
			<div id="sidebar">
				
				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
	
</div>




<?php get_footer(); ?>