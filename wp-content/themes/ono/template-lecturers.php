<?php
/*
Template Name: Lecturers Page
*/

// http://ono.lakoah.co.il/%D7%90%D7%95%D7%93%D7%95%D7%AA-2/%D7%A1%D7%92%D7%9C-%D7%9E%D7%A8%D7%A6%D7%99%D7%9D/
?>
<?php get_header(); ?>


<div class="thumbnail-container">
	
	<img src="<?php echo get_template_directory_uri(); ?>/img/slider-teachers2.jpg" />
	
	<div id="main-title-container">
		<h1 id="main-title">
			<?php the_title(); ?>
		</h1>
	</div>
</div>


<div class="container">

<?php include_once('breadcrumbs.php'); ?>
	
	<div class="row">
		<div class="col-1-3 col-right">
			<div id="page-side-menu">
				<div id="smartphone-submenu-btn">
					<img src="<?php echo get_template_directory_uri(); ?>/img/three-bars.png" />
				</div>
				
				<div id="page-side-menu-wrap">
					<?php //include_once('incl/menu-lecturers.php'); ?>
					<ul class="menu">
						<?php 
							$children = wp_list_pages("title_li=&child_of=" . get_the_ID() . "&echo=0");
							if ($children){
								
							} else {
								//$parent = wp_get_post_parent_id( get_the_ID() );
								//$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");
							}
							echo $children;
						?>
					</ul>
				</div>
				
			</div>
		</div>
		<div id="staff-single-content" class="col-2-3 col-left">
			<!-- Main Column -->
			
			<div id="lectures-slider" class="flexslider">
				<ul class="slides">
					<?php
					$term =	$wp_query->queried_object;
					
					$lecturers = array();
					// $children = wp_list_pages("title_li=&child_of=" . get_the_ID() . "&echo=0");
					// print_r($children);
					$args = array( 
					        'child_of' => get_the_ID(), 
					        'hierarchical' => 0,
					        'sort_column' => 'menu_order', 
					        'sort_order' => 'asc'
					);
					$mypages = get_pages( $args );
					//query_posts( array( 'post_type' => 'lecturer', 'showposts' => -1, 'tax_query' => array(
												//array( 'taxonomy' => $term -> taxonomy, 'field' => 'slug', 'terms' => $term -> slug )
									// )));
					// if(have_posts()) : while(have_posts()) : the_post();
					foreach( $mypages as $children ): 
						// var_dump($children);
					// echo $children->ID;
					// echo $children->post_title;
						$thumb = '';
						$img = types_render_field("photo", array("raw"=>"true"));
						if ( $img ) $thumb = get_site_url() . "/wp-content/uploads/staff-photos/" . $img;
						if ( has_post_thumbnail() ) {
						 $thumb = get_the_post_thumbnail($children->ID, array(140,140));}
						else { $thumb = '<img width="140" height="140" src="http://www.ono-new.c14.co.il/wp-content/uploads/2015/01/lecturers.png" alt="lecturers">';
					}

						
						if ( $thumb )
							$lecturers[] = array(
												'ID' => $children->ID,
												'thumbnail' => $thumb,
												'title' => $children->post_title,
												'position' => types_render_field("position", array("raw"=>"true")),
												'permalink' => $children->guid,
												'content' => wp_trim_words( strip_tags($children->post_content), 50 )
												);
					?>
					
						<?php if ( $thumb ): ?>
							<li>
								<div class="lecturer-wrap">
									<a href="#" class="slider-link" onClick="show_lecturer(<?php echo $children->ID; ?>);">
										<?php echo $thumb; ?>
										<div class="lecturer-name">
											<?php echo $children->post_title; ?>
										</div>
									</a>
								</div>
							</li>
						<?php endif; ?>
					
					<?php
					// endwhile;
						// endif;
					endforeach;
					wp_reset_query();
					?>
				</ul>
			</div>
			
			<?php foreach( $lecturers as $lec ): ?>
				<div class="lecturer-excerpt row" id="lecturer-<?php echo $lec['ID'] ?>">
					<div class="col-1-3 col-right">
						<div class="thumbnail">
							<?php echo $lec['thumbnail']; ?>
						</div>
					</div>
					<div class="col-2-3 col-left">
						<h2><?php echo $lec['title']; ?></h2>
						<div class="subtitle"><?php echo $lec['position']; ?></div>
						<?php echo $lec['content']; ?>
						<div class="row">
							<a href="<?php echo $lec['permalink']; ?>" class="show-more icon-gt"><? _e("To profile page","ono"); ?></a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			
		</div>
	</div>
	
</div>










<?php get_footer(); ?>






