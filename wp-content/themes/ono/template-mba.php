<?php
/*
Template Name: M.B.A Page
*/
?>


<?php get_header(); ?>





<div class="thumbnail-container">
	<?php include_once('incl/lecturer-top-thumbnail.php'); ?>
	<?php 
		if ( has_post_thumbnail() ) the_post_thumbnail('full');
			else echo '<img src="' . get_template_directory_uri() . '/img/slider-toar-rishon.jpg" />';
	?>
	
	<div id="main-title-container">
		<h1 id="main-title">
			<?php the_title(); ?>
		</h1>
	</div>
</div>


<div class="container">

<?php include_once('breadcrumbs.php'); ?>
	
	<div class="row">
		<div class="col-1-5 col-menu">
			<div id="page-side-menu">
				<div id="smartphone-submenu-btn">
					<img src="<?php echo get_template_directory_uri(); ?>/img/three-bars.png" />
				</div>				

	<!-- 			<div id="page-side-menu-wrap">
					<ul class="menu">
						<?php 
							$children = wp_list_pages("title_li=&child_of=" . get_the_ID() . "&echo=0");
							echo $children;
						?>
					</ul>
				</div> -->
				
			</div>
		</div>
		<div id="main-content" class="col-4-5">
			
			<h2 class="event-subtitle">
				<?php the_title(); ?>
			</h2>
			
			<div class="content">
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
				
				<?php the_content(); ?>
				
				<?php
					endwhile;
					endif;
				?>
			</div>
			<div class="clear"></div>
			<div class="speciallize">
			<div class="clear"></div>
			<!-- <p dir="RTL"><strong style="color: #afc11c; font-size: 24px; text-align: justify;border-bottom: 1px dashed #e1e1e1;">התמחויות</strong></p>			 -->
			<div class="clear"></div>	
			<ul class="speciallize_list">
						<?php 
							// $children = wp_list_pages("title_li=&depth=1&child_of=". get_field('parent_page_specialities'));
$args = array( 
        'child_of' => get_field('parent_page_specialities'), 
        'parent' => get_field('parent_page_specialities'),
        'hierarchical' => 0,
        'sort_column' => 'menu_order', 
        'sort_order' => 'asc'
);
$mypages = get_pages( $args );?>
<?php foreach( $mypages as $page): ?>
        				<li>
        				<div class="pic"><a href="<?php echo $page->guid == 'http://ono-new.c14.co.il/' ? $page->guid.'?page_id='.$page->ID : $page->guid;?>"><?php echo get_the_post_thumbnail($page->ID,array(82, 82) );?></a></div>
        				<div class="text">
        				<a style="text-decoration:none" href="<?php echo $page->guid == 'http://ono-new.c14.co.il/' ? $page->guid.'?page_id='.$page->ID : $page->guid;?>"><h2><?php echo $page->post_title;?></h2></a>
        				<!-- <span><?php echo $page->post_excerpt; ?></span> -->
        				<p><a class="button" href="<?php echo $page->guid == 'http://ono-new.c14.co.il/' ? $page->guid.'?page_id='.$page->ID : $page->guid;?>">לתכנית ההתמחות</a></p>
        				</div>
        				</li>
			            <?php endforeach; ?>

			</ul>
			</div>
			 <div class="print">
<?php echo do_shortcode( '[print-me]' ) ?>
</div>
		</div>
		<div class="col-1-5 col-sidebar">
<?php	if( get_field('days_hours_study_title') ):    ?>

<div id="sidebar_study">
<h2 class="sidebar_h2" style="margin-top:0px;">ימים ושעות הלימוד</h2>
<div class="block1 minhal_block">
						   <div class="header">
							   <div class="right"><?php echo get_field('days_hours_study_title');?></div>
							   <div class="left"></div>
						   </div>
						   <div class="details" style="display: none;">
						    <div class="header_text" style="height:auto">
						    	<?php echo get_field('days_hours_study');?>
						    </div>
    					</div>
			    </div>
	    </div>
	
		<?php endif;?>

		<script>
jQuery(".block1.minhal_block > div.header").click(function(){
	jQuery(this).parent().find(".details").toggle();
});
</script>	
<div id="sidebar_terms">
			<h2 class="sidebar_h2">תנאי קבלה לתואר</h2>
<?php $admission = get_field('admission');
$terms = get_posts(array(
  'post_type' => 'terms',
  'numberposts' => -1,
  'tax_query' => array(
    array(
      'taxonomy' => 'terms_categories',
      'field' => 'id',
      'terms' => $admission[0], // Where term_id of Term 1 is "1".
      'include_children' => false
    )
  )
));
?>
<div class="conditions">
	<ul>
	<?php foreach( $terms as $term): ?>
        				<li><a href="<?php echo $term->guid;?>"><?php echo $term->post_title;?></a></li>
			            <?php endforeach; ?>
				</ul>
	<div class="sidebar_button_lnk">
	<?php	if( get_field('admission_link') ):    ?>
		<a class="sidebar_button" href="<?php echo get_field('admission_link'); ?>">לפרטים על תנאי הקבלה</a>
		<?php endif;?>
	<?php	if( get_field('degree_program_file') ):    ?>
    <a class="sidebar_button" href="<?php the_field('degree_program_file'); ?>">הורדת תכנית התואר</a>
<?php endif;?>
	
	</div>
	<!--<a class='sidebar_button'>לפרטים על תנאי הקבלה</a>
	<a class='sidebar_button'>הורדת תכנית התואר</a>-->
	 <div class="clear_both"></div>
	
</div></div>



			<div id="sidebar_lecture">
			<h2 class="sidebar_h2">המרצים שלכם</h2>
			<div class="lecture">
				
<?php

$post_object = get_field('main_lecture');

if( $post_object ): 

	// override $post
	$post = $post_object;
	setup_postdata( $post ); 

	?>
   <div class="block"> 
    	
    	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(82, 82) );?></a>
    	<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    	<span><?php the_field('excerpt_lecture'); ?></span>
    
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
		</div>	
		<div class="clear"></div>	
<?php

$post_objects = get_field('lecture');

if( $post_objects ): ?>
    <?php foreach( $post_objects as $post_object): ?>
    	<div class="block"> 
            
            <a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_post_thumbnail($post_object->ID, array(82, 82) );?></a>
            <h3><a href="<?php echo get_permalink($post_object->ID); ?>"><?php echo get_the_title($post_object->ID); ?></a></h3>
            <span><?php the_field('excerpt_lecture', $post_object->ID); ?></span>
            </div>
            <div class="clear"></div>
    <?php endforeach; ?>
<?php else:

$args = 'post_type=lecturer&meta_key=_thumbnail_id&posts_per_page=2&orderby=rand';
// The Query
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {
	echo '<ul>';
	while ( $the_query->have_posts() ) {
		$the_query->the_post();?>
    	<div class="block"> 
            
            <a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail(array(82, 82) );?></a>
            <h3><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
            <span><?php the_field('excerpt_lecture', $get_the_id); ?></span>
            </div>
            <div class="clear"></div>
		<?php
	}
	echo '</ul>';
} else {
	// no posts found
}
/* Restore original Post Data */
wp_reset_postdata();?>

<?php endif;?>


<a href="<?php echo get_site_url(); ?>/academy/faculty-lecturers/" style="padding-top: 20px;float:left; font-weight:bold;">לרשימה המלאה</a>
			</div>
			</div>



			<div id="sidebar">

				<?php get_sidebar(); ?>

			</div>
<div class="prepration">
	<a href="<?php echo get_site_url(); ?>/consulting-and-registration/%D7%94%D7%A8%D7%A9%D7%9E%D7%94/%D7%98%D7%A4%D7%A1%D7%99%D7%9D-%D7%9C%D7%94%D7%95%D7%A8%D7%93%D7%94/">טפסי רישום והכנה</a> 
	<a href="https://yedion.ono.ac.il/Yedion/fireflyweb.aspx?appname=BSHITA&amp;prgname=Enter_1" class="space">להרשמה אונליין ל.M.B.A</a> 
</div>
		</div>
	</div>

</div>




<?php get_footer(); ?>