	<div id="lecturer-thumbnail-container">
		<?php 
			$lecturer = get_field('lecturer');
			$lecturer_id = $lecturer -> ID;
			$img = get_post_meta($lecturer_id, 'wpcf-photo', true);
			if ( $img ) $thumb = get_site_url() . "/wp-content/uploads/staff-photos/" . $img;
			if ( has_post_thumbnail($lecturer_id) ) $thumb = get_the_post_thumbnail('medium', $lecturer_id);
		?>
		<?php if ( $lecturer_id ): ?>
			<div class="thumbnail">
				<a href="<?php echo get_permalink($lecturer_id); ?>">
					<?php echo "<img src='" . $thumb . "' alt='".$lecturer->post_title."'/>"; ?>
				</a>
			</div>
			<div class="name">
				<a href="<?php echo get_permalink($lecturer_id); ?>">
					<?php echo $lecturer->post_title; ?>
				</a>
			</div>
			<div class="position">
				<?php echo get_post_meta($lecturer_id, 'wpcf-position', true); ?>
			</div>
		<?php endif; ?>
	</div>