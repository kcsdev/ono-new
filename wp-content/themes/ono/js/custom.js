
jQuery( document ).ready(function(){
	
    jQuery('#main_menu_text').click(function(){   
       jQuery('#top_menu_id .menu').toggle();
    });
    
    
	jQuery('#font_size_1').click(function(){
		jQuery('body').css('zoom', '1.0');
	});
	jQuery('#font_size_2').click(function(){
		jQuery('body').css('zoom', '1.1');
	});
	jQuery('#font_size_3').click(function(){
		jQuery('body').css('zoom', '1.2');
	});

// Fix search icon
    jQuery('#search-button').click(function(){
jQuery('#search-container form').submit();
});
    
	//jQuery('#menu-item-34561').remove();
    
	jQuery('#zoom-in').click(function(){
		event.preventDefault;
		var current_zoom = jQuery('body').css('zoom');
		if ( current_zoom < 1 ) {
			jQuery('body').css('zoom', '1.0')
		} else {
			jQuery('body').css('zoom', '1.1');
		}
	});
	
	jQuery('#zoom-out').click(function(){
		event.preventDefault;
		var current_zoom = jQuery('body').css('zoom');
		if ( current_zoom > 1 ) {
			jQuery('body').css('zoom', '1.0')
		} else {
			jQuery('body').css('zoom', '0.9');
		}
	});
	
	//-----------> Slider Home Top <-------------------
    jQuery('#home-slider').flexslider({
		controlNav: false
	});
	
	//console.log( jQuery('#home-slider') );

	//-----------> Slider Features <-------------------
	jQuery('#home-feature').flexslider({
		controlNav: false
	});

	jQuery('#home-feature-prev').click(function(){
		var $slider = jQuery('#home-feature').data('flexslider');
		$slider.flexAnimate($slider.getTarget("prev"), true);
	});
	
	jQuery('#home-feature-next').click(function(){
		var $slider = jQuery('#home-feature').data('flexslider');
		$slider.flexAnimate($slider.getTarget("next"), true);
	});
	
	
	//-----------> Slider Lectures <---------------------
	function getGridSizeLectures() {
		return (window.innerWidth < 500) ? 2 :
			(window.innerWidth < 767) ? 3 :
			(window.innerWidth < 1000) ? 4 : 5;
	}
	
	jQuery('#lectures-slider').flexslider({
		controlNav: false,
		maxItems: getGridSizeLectures(),
		minItems: getGridSizeLectures(),
		animation: "slide",
		animationLoop: true,
		itemWidth: 150,
		prevText: "",
		nextText: ""
	});	
	
	
	
	//-----------> Slider News Event <-------------------
	function getGridSize() {
		return (window.innerWidth < 1000) ? 1 : 2;
	}
	
	jQuery('#news-events').flexslider({
		controlNav: false,
		maxItems: getGridSize(),
		minItems: getGridSize(),
		animation: "slide",
		animationLoop: true,
		itemWidth: 300,
		prevText: "",
		nextText: ""
	});
	
	// check grid size on resize event
	jQuery(window).resize(function() {
		if ( jQuery('#news-events').length > 0 ) {
			var gridSize = getGridSize();
			var $flexslider = $('#news-events').data('flexslider');

			$flexslider.vars.minItems = gridSize;
			$flexslider.vars.maxItems = gridSize;
		}
		
		if ( jQuery('#lectures-slider').length > 0 ) {
			var gridSize = getGridSizeLectures();
			var $flexslider = $('#lectures-slider').data('flexslider');

			$flexslider.vars.minItems = gridSize;
			$flexslider.vars.maxItems = gridSize;
		}
	});
	
	jQuery('#news-events-prev').click(function(){
		var $flexslider = jQuery('#news-events').data('flexslider');
		$flexslider.flexAnimate($flexslider.getTarget("prev"), true);
	});
	
	jQuery('#news-events-next').click(function(){
		var $flexslider = jQuery('#news-events').data('flexslider');
		$flexslider.flexAnimate($flexslider.getTarget("next"), true);
	});

	
	jQuery( "#tabs-ono" ).tabs();
	jQuery( "#tabs-or-yehuda" ).tabs();
	
	
	//----------------> Menu <-------------------------
	
	jQuery('#smartphone-menu-btn').click(function(){
		jQuery('#top_menu_id .menu').slideToggle('fast');
	});

	jQuery('#smartphone-submenu-btn').click(function(){
		jQuery('#page-side-menu-wrap .menu').slideToggle('fast');
	});
	
	//------------------> Lecturer thumbnail <-------------
	
	placeLecturerThumbnail();
	
	jQuery(window).resize(function() {
		placeLecturerThumbnail();
	});
	
	function placeLecturerThumbnail() {
		if ( jQuery('#lecturer-thumbnail-container').length > 0 ) {
			if ( jQuery(window).width() > 1020 ) {
				var left =  ( jQuery(window).width() - 1020 ) / 2 + 55;
				jQuery('#lecturer-thumbnail-container').css('left', left);
				jQuery('#lecturer-thumbnail-container').css('display', 'block');
			} else {
				jQuery('#lecturer-thumbnail-container').css('display', 'none');
			}
		}
	}

        setTimeout(function(){
var btn = jQuery('li#menu-item-33193').clone();
btn.find('a').attr('href','https://yedion.ono.ac.il/Yedion/fireflyweb.aspx?appname=BSHITA&prgname=Enter_1');
btn.addClass('mobile-btn');
btn.attr('id','new_btn');
btn.appendTo('#top-slider-home');

        //jQuery('li#menu-item-33193').clone().addClass('mobile-btn').appendTo('#top-slider-home');
        //jQuery('li#menu-item-33193:eq(2)').addClass('mobile-btn');
        },1000);
    
});

jQuery( "a.slider-link" ).click(function( event ) {
  event.preventDefault();
});

function show_lecturer( ID ) {
	var lecturer = '#lecturer-' + ID;
	jQuery(".lecturer-excerpt").each(function(){
		jQuery( this ).fadeOut( 'fast' );
	});
	jQuery( lecturer ).delay(200).fadeToggle( 'fast' );
}








