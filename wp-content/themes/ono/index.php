
<?php get_header(); ?>

<!------------------------* SLIDER *--------------------------->

<div id="top-slider-home">

<div id="home-slider" class="flexslider">
	<ul class="slides">
		<?php
			query_posts(array(
								'showposts' => -1,
								'post_type' => 'home-slider',
								'post_status' => 'publish'
									));
			if ( have_posts() ): while ( have_posts() ): the_post();
		?>
			<li>
				<?php the_post_thumbnail('full'); ?>
			</li>
		<?php
			endwhile;
			endif;
			wp_reset_query();
		?>
	</ul>
</div>



	<!----------------- slider menu ----------------->
	<div class="container row">
	
		<?php
			wp_nav_menu( array(
				'theme_location' => 'slider-menu',
				'container_class' => 'slider_menu',
				'items_wrap' => '<ul class="%2$s">%3$s</ul>'
				)
			);
		?>
	
	</div>
</div>

<!------------------------* CONTENT *--------------------------->

<div class="container">
	<div class="row">
		<div class="col-2-3 col-right">
			<!---------------- Main Column ----------------------------------->
			<!------------------ Features ------------------------------------>
			<div class="row">
				<div id="home-feature" class="flexslider">
					<ul class="slides">
					<?php
						query_posts(array(
											'showposts' => -1,
											'post_type' => 'event',
											'post_status' => 'publish'
												));
						if ( have_posts() ): while ( have_posts() ): the_post();
					?>
						<li>
							<div class="col-1-2 col-left">
								<div class="thumbnail"><?php the_post_thumbnail(); ?></div>
							</div>
							<div class="col-1-2 col-right">
                                <h2><?php if (ICL_LANGUAGE_CODE == 'en') { echo "Events"; } else { echo "אירועים"; } ?></h2>
								<?php /*<h3 class="title"><?php the_title(); ?></h3> */ ?>
								<div class="content">
									<?php the_content(); ?>
								</div>
								<?php
									$page = get_field('page_obj');
									if ( is_object($page) ) {
										$permalink = get_permalink($page->ID);
									} else {
										$permalink = '#';
									}
									?>
								<?php /*<a href="<?php echo $permalink ?>" class="show-more icon-plus"><?php echo get_field('link_text') ?></a>*/ ?>
                               <a href="<?php echo get_post_type_archive_link('event'); ?>" class="show-more icon-plus"><?php if (ICL_LANGUAGE_CODE == 'en') { echo "More Events"; } else { echo "עוד אירועים"; } ?></a> 
							</div>
						</li>
					<?php
						endwhile;
						endif;
						wp_reset_query();
					?>
						
					</ul>
				</div>
				<div class="row relative">
					<div class="home-feature-pagination">
						<ul>
							<li id="home-feature-prev">&gt;</li>
							<li id="home-feature-next">&lt;</li>
						</ul>
					</div>
				</div>
			</div>
			<!----------------- News & Events ------------------------------------->
			<div id="home-news-events" class="row">
				<h2 class="title"><? _e("New & Events","ono"); ?></h2>
				<div class="pagination row">
					<ul>
						<li id="news-events-prev">&gt;</li>
						<li id="news-events-next">&lt;</li>
					</ul>
					<div class="news-events-more-link">
						<a href="<?php echo get_post_type_archive_link('event'); ?>" class="show-more icon-gt"><? if(ICL_LANGUAGE_CODE!=en){ echo 'עוד חדשות וארועים';} else{ _e("More Events","ono"); } ?></a>
					</div>
				</div>
				<div class="row">
				
					<div id="news-events" class="flexslider">
						<ul class="slides">
							<?php
								query_posts(array(
													'showposts' => 10,
													'post_type' => 'event',
													'post_status' => 'publish'
														));
								if ( have_posts() ): while ( have_posts() ): the_post();
							?>
							<?php if (strtotime(types_render_field("date", array('format' => 'd/m/Y'))) < time()) { ?>
								<li>
									<div class="content">
										<div class="thumbnail">
											<a href="<?php echo get_permalink(); ?>">
												<?php the_post_thumbnail('medium'); ?>
											</a>										
										</div>
										<?php if (types_render_field("date", array('format' => 'l, d/n/Y')) ) { ?>
										<div class="date-subtitle">
											<a href="<?php echo get_permalink(); ?>">
												<?php
													echo types_render_field("date", array('format' => 'l, d/n/Y') );

													$start_time = types_render_field("time-start", array('raw' => 'true') );
													$end_time = types_render_field("time-end", array('raw' => 'true') );
													if ($start_time) {
														echo ', ';
														echo $start_time . ' - ' . $end_time;
													}
												?>
											</a>
										</div>
										<?php } ?>
										<div class="subtitle">
											<a href="<?php echo get_permalink(); ?>">
												<?php the_title(); ?>
											</a>
										</div>
									</div>
								</li>
								<?php } ?>
							<?php
								endwhile;
								endif;
								wp_reset_query();
							?>

						</ul>
					</div>
					
				</div>
			</div>
		</div>
		<div class="col-1-3 col-left">
			<!---------------- Sidebar ---------------------------------------->
			<div id="sidebar">
				
				<?php get_sidebar(); ?>
				
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>

