
<?php get_header(); ?>




<!------------------------* SLIDER *--------------------------->

<div class="thumbnail-container">
	
	<?php 
		/*if ( has_post_thumbnail() ) the_post_thumbnail('full');
			else */echo '<img src="' . get_template_directory_uri() . '/img/general.jpg" />';
	?>
	
	<div id="main-title-container">
		<h1 id="main-title">
			<?php the_title(); ?>
		</h1>
	</div>
</div>

<!------------------------* CONTENT *--------------------------->

<div class="container">
	
	<div class="row">
		<div id="main-content" class="col-2-3 col-right">
			<!---------------- Main Column ----------------------------------->

			
			<div class="content">
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
				
				<?php if ( ! post_password_required() ): ?>
				
					<h2 class="subtitle">
						<a href="<?php echo get_post_permalink();  ?>">
							<?php the_title(); ?>
						</a>
					</h2>	
					
					<?php the_excerpt(); ?>
					
				<?php endif; ?>
				
				<?php
					endwhile;
					endif;
				?>
			</div>
		</div>
		<div class="col-1-3 col-left">
			<!---------------- Sidebar ---------------------------------------->
			<div id="sidebar">
				
				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
	
</div>




<?php get_footer(); ?>


