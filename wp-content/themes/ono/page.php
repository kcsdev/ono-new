<?php
$parent = wp_get_post_parent_id( get_the_ID() );
$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0");

if ( $children ) include_once('template-three-columns.php');
else include_once('template-two-columns.php');
?>