
<?php get_header(); ?>




<!------------------------* SLIDER *--------------------------->

<div class="thumbnail-container">
	
	<?php 
		echo '<img src="' . get_template_directory_uri() . '/img/news_and_events.jpg" />';
	?>
	
	<div id="main-title-container">
		<h1 id="main-title">
			<?php _e('News', 'ono'); ?>
		</h1>
	</div>
</div>

<!------------------------* CONTENT *--------------------------->

<div class="container">
	<?php include_once('breadcrumbs.php'); ?>
	
	<div class="row">
		<div id="main-content" class="col-2-3 col-right">
			<!---------------- Main Column ----------------------------------->
			<h2 class="subtitle">
				<?php the_title(); ?>
			</h2>
			
			<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
			
			<div class="excerpt">
				<?php echo types_render_field("excerpt", array('raw' => 'true') ); ?>
			</div>
			
			<div class="date">
				<?php the_date() ?>
			</div>
			
			<div class="content">
				
				<?php include_once('incl/share-links.php'); ?>
				
				<?php
					if ( has_post_thumbnail() ) the_post_thumbnail('medium', array('class' => 'thumbnail') );
				?>
				
				<?php the_content(); ?>
				
			</div>
			
			<?php
				endwhile;
				endif;
			?>
			
		</div>
		<div class="col-1-3 col-left">
			<!---------------- Sidebar ---------------------------------------->
			<div id="sidebar">
				
				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
	
</div>




<?php get_footer(); ?>


