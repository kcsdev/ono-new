
<?php get_header(); ?>


<div class="thumbnail-container">
	
	<img src="<?php echo get_template_directory_uri(); ?>/img/slider-teachers2.jpg" />
	
	<div id="main-title-container">
		<h1 id="main-title">
			<?php the_title(); ?>
		</h1>
	</div>
</div>


<div class="container">

<?php include_once('breadcrumbs.php'); ?>
	
	<div class="row">
		<div class="col-1-3 col-right">
			<div id="page-side-menu">
				<div id="smartphone-submenu-btn">
					<img src="<?php echo get_template_directory_uri(); ?>/img/three-bars.png" />
				</div>
				
				<div id="page-side-menu-wrap">
					<?php include_once('incl/menu-lecturers.php'); ?>
				</div>
				
			</div>
		</div>
		<div id="staff-single-content" class="col-2-3 col-left">
			<!-- Main Column -->
			<div class="col-2-6">
				<div class="thumbnail">
					<?php 
						if ( has_post_thumbnail() ) {
							the_post_thumbnail('medium');
						} else {
							$img = types_render_field("photo", array("raw"=>"true"));
							if ( $img ) echo "<img src='" . get_site_url() . "/wp-content/uploads/staff-photos/" . $img . "' />";
						}
					?>
				</div>
				<?php
					$email = types_render_field("email", array("raw"=>"true"));
					$phone = types_render_field("phone", array("raw"=>"true"));
					$position = types_render_field("position", array("raw"=>"true"));
				?>
				<?php if( $email ): ?>
					<a href="mailto:<?php echo $email ?>" class="dark icon-envelop"><?php echo $email ?></a>
				<?php endif; ?>
				<?php if( $phone ): ?>
					<div class="icon icon-phone"><?php echo $phone ?></div>
				<?php endif; ?>
			</div>
			<div class="col-4-6">				
				<h2 class="staff-subtitle">
					<?php the_title(); ?>
				</h2>

				<div class="staff-position">
					<?php echo $position; ?>
				</div>
				
				<div class="content">
					<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
					
					<?php the_content(); ?>

					<?php
						// if(get_field('resumes'))
						// {
						// 	echo '<h1 style="text-align: right;"><a href="' . get_field('resumes') . '">קורות חיים</a></h1>';
						// }
						// if(get_field('publications'))
						// {
						// 	echo '<h1 style="text-align: right;"><a href="' . get_field('publications') . '">פרסומים</a></h1>';
						// }
						// if(get_field('website_ssrn'))
						// {
						// 	echo '<h1 style="text-align: right;"><a href="' . get_field('website_ssrn') . '">אתר אישי בSSRN</a></h1>';
						// }
					?>
					
					<?php
						endwhile;
						endif;
					?>
				</div>
				
				<div class="clear"></div>
				
				<?php
					$education = get_field('education');
					$publications = get_field('publications');
					$website_ssrn = get_field('website_ssrn');
					$category = get_field('category');
				?>
				
				<!-- <div class="row"> -->
				<div class="content">
					<!-- <div id="accordion"> -->
						<?php if ( $education ): ?>
						<h1 style="text-align: right;"><a href="<?php echo $education; ?>"><?php _e('Education', 'ono'); ?></a></h1>
							<!-- <h3><?php _e('Education', 'ono'); ?></h3>
							<div>
								<?php echo $education; ?>
							</div> -->
						<?php endif; ?>
						
						<?php if ( $publications ): ?>
						<h1 style="text-align: right;"><a href="<?php echo $publications; ?>"><?php _e('Publications', 'ono'); ?></a></h1>
							<!-- <h3><?php _e('Publications', 'ono'); ?></h3>
							<div>
								<?php echo $publications; ?>
							</div> -->
						<?php endif; ?>

						<?php if ( $website_ssrn ): ?>
						<h1 style="text-align: right;"><a href="<?php echo $website_ssrn; ?>"><?php _e('Website in SSRN', 'ono'); ?></a></h1>
							<!-- <h3><?php _e('Website in SSRN', 'ono'); ?></h3>
							<div>
								<?php echo $website_ssrn; ?>
							</div> -->
						<?php endif; ?>
						
						<?php if ( $category ): ?>
							<h3><?php _e('Additional categories', 'ono'); ?></h3>
							<div>
								<?php echo $category ?>
							</div>
						<?php endif; ?>
					</div>
				<!-- </div> -->
				
			</div>
					 <div class="print">
<?php echo do_shortcode( '[print-me]' ) ?>
</div>
		</div>
	</div>
	
	<script>
		jQuery(function() {
			jQuery( "#accordion" ).accordion({
				heightStyle: "content"
			});
		});
	</script>
	
</div>










<?php get_footer(); ?>






