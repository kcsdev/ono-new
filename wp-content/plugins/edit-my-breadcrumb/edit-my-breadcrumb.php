<?php
/*
Plugin Name: Edit My Breadcrumb
Plugin URI: http://webpro.ninja/edit-my-breadcrumb/
Description: Easily add breadcrumbs to your site. 
Version: 1.2
Author: Jonathan Volks
Author URI: http://webpro.ninja
License: GPL2

Copyright 2014  Jonathan Volks  (email : jonathanvolks@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// =======================================================================================
// Register the activation and deactivation
// =======================================================================================
register_activation_hook(__FILE__,'edit_my_breadcrumb_install'); 
register_deactivation_hook( __FILE__, 'edit_my_breadcrumb_remove' );

/*
 * edit_my_breadcrumb_install()
 * Sets up fields in the database when user activates the plugin
 *  
 * @param - NONE
 * 
 * @return - NONE
 */
function edit_my_breadcrumb_install() {
	add_option("emb_divider", '&gt;', '', 'yes');	
	add_option("emb_class", '', '', 'yes');			
	add_option("emb_customfield", '', '', 'yes');	
	add_option("emb_input", '0', '', 'yes');	
	add_option("emb_home", 'Home', '', 'yes');	
	add_option("emb_frontpage", '0', '', 'yes');
}

/*
 * edit_my_breadcrumb_remove()
 * Removes database fields when user deactivates the plugin
 *  
 * @param - NONE
 * 
 * @return - NONE
 */
function edit_my_breadcrumb_remove() {

}

// =======================================================================================
// Front End - Display breadcrumbs on the page
// =======================================================================================
if(get_option('emb_input') == 1) {
	add_filter('the_content', 'add_breadcrumb');
}

/*
 * add_breadcrumb()
 * Displays at the beggining of the content
 *  
 * @param - NONE
 * 
 * @return - No direct output
 */
function add_breadcrumb($content) {
	$emb_frontpage = get_option('emb_frontpage');
	if((is_home() || is_front_page())) {
		if($emb_frontpage == 1) {
			if(get_post_type() == 'page') {
				$custom_content = display_breadcrumbs();
				$custom_content .= $content;
				return $custom_content;
			} else {
				return $content;	
			}
		} else {
			return $content;	
		}
	} else {
		if(get_post_type() == 'page') {
			$custom_content = display_breadcrumbs();
			$custom_content .= $content;
			return $custom_content;
		} else {
			return $content;	
		}
	}
	
}


/*
 * display_breadcrumbs()
 * Displays breadcrumbs on the page
 *  
 * @param - NONE
 * 
 * @return - Output Directly On Page
 */
function display_breadcrumbs() {
	$emb_divider 		= get_option('emb_divider');
	$emb_class 			= get_option('emb_class');
	$emb_customfield 	= get_option('emb_customfield');
	$emb_home 			= get_option('emb_home');
	$emb_frontpage		= get_option('emb_frontpage');
	
	if($emb_divider == "") { $emb_divider = ">"; }
	if($emb_class == "") { $emb_class = "breadcrumbs"; }
	
	echo '<p class="'. $emb_class .'">';
	echo '<a href="'. get_site_url() .'">'. $emb_home .'</a>';
	echo ' '. $emb_divider .' ';
	$current_post = get_post(get_the_ID());
	$parent_post = $current_post->post_parent;
	if($parent_post != '' && $parent_post > 0) {
		recursive_parent_check($parent_post, $emb_divider, $emb_customfield);
	}
	
	if ($emb_customfield != "") { 
		$output_title = get_post_meta(get_the_ID(), $emb_customfield, true);
		if($output_title == "") {
			$output_title = get_the_title(get_the_ID());	
		}
	} else {
		$output_title = get_the_title(get_the_ID());	
	}
	echo '<span>'. $output_title .'</span>';
	echo '</p>';
}

/*
 * recursive_parent_check()
 * Recursively loop through pages parents and then output them
 *  
 * @param($page_id) - ID of the page
 * @param($emb_divider) - Divider between breadcrumbs
 * @param($emb_customfield) - Use posts Custom Field instead of Title
 * 
 * @return - Output Directly On Page
 */
function recursive_parent_check($page_id, $emb_divider, $emb_customfield) {
	$current_post = get_post($page_id);
	$parent_post = $current_post->post_parent;
	if($parent_post != '' && $parent_post > 0) {
		recursive_parent_check($parent_post, $emb_divider, $emb_customfield);
	}
	
	// Output
	if ($emb_customfield != "") { 
		$output_title = get_post_meta($page_id, $emb_customfield, true);
		if($output_title == "") {
			$output_title = get_the_title($page_id);	
		}
	} else {
		$output_title = get_the_title($page_id);	
	}
	echo '<a href="'. get_permalink($page_id) .'">'. $output_title .'</a>';
	echo ' '. $emb_divider .' ';
}


// =======================================================================================
// Options Page - Ability for user to set options
// =======================================================================================
if (is_admin()){
	// Add to Navigation Menu
	add_action('admin_menu', 'edit_my_breadcrumb_admin_menu');
}

/*
 * edit_my_breadcrumb_admin_menu()
 * Add to Wordpress Admin Navigation and call function for html on the options page
 *  
 * @param - NONE
 * 
 * @return - NONE
 */
function edit_my_breadcrumb_admin_menu() {
	add_options_page('Edit My Breadcrumb', 'Breadcrumbs', 'administrator', 'edit-my-breadcrumb', 'edit_my_breadcrumb_html_page');
}	

/*
 * edit_my_breadcrumb_html_page()
 * HTML for the admin area where user will set options
 *  
 * @param - NONE
 * 
 * @return - Output Directly On Page
 */
function edit_my_breadcrumb_html_page() { ?>
	<div class="wrap">
	<div id="icon-options-general" class="icon32"><br /></div>
    <h2>Edit My Breadcrumbs</h2>
    <p>Edit My Breadcrumb is a plugin for WordPress that allows for breadcrumbs to be added to your page. It is fully customizable for your website needs.  There are two options to implement this into your site; web developers can add a function into their theme or it can be automatically added before your content.</p>
    <h3>Use and Settings</h3>
    <ol>
    	<li><strong>Developers</strong> – Theme developers and programmers can use the function &lt;?php display_breadcrumbs(); ?&gt; any place in the theme they wish for the breadcrumbs to display.</li>
        <li><strong>Non-Developers</strong> – In the settings configuration, enable ‘Breadcrumb Auto Output ‘ to display the breadcrumbs directly the start of your content.</li>
    </ol>

	<form method="post" action="options.php">
		<?php wp_nonce_field('update-options'); ?>
		<table class="form-table">
			<tr valign="top">
				<th scope="row"><label for="emb_divider">Breadcrumb Divider</label></th>
				<td><input name="emb_divider" type="text" id="emb_divider" value="<?php echo get_option('emb_divider'); ?>" class="regular-text" /></td>
			</tr>
            <tr valign="top">
				<th scope="row"><label for="emb_home">Breadcrumb Home Text</label></th>
				<td><input name="emb_home" type="text" id="emb_home" value="<?php echo get_option('emb_home'); ?>" class="regular-text"  /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="emb_class">Breadcrumb Class</label></th>
				<td><input name="emb_class" type="text" id="emb_class" value="<?php echo get_option('emb_class'); ?>" class="regular-text"  /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="emb_customfield">Breadcrumb Custom Field</label></th>
				<td><input name="emb_customfield" type="text" id="emb_customfield" value="<?php echo get_option('emb_customfield'); ?>" class="regular-text"  /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="emb_input">Breadcrumb Auto Output</label></th>
				<td>
					<select name="emb_input" id="emb_input">
						<option value="0">No</option>
						<option value="1" <?php if(get_option('emb_input') == 1) { echo "SELECTED"; }?>>Yes</option>
					</select>
				</td>
			</tr>
            <tr valign="top">
            	<th scope="row"><label for="emb_frontpage">Output on Home Page</label></th>
                <td>
                	<select name="emb_frontpage" id="emb_frontpage">
	                    <option value="0">No</option>
						<option value="1" <?php if(get_option('emb_frontpage') == 1) { echo "SELECTED"; }?>>Yes</option>
                    </select>
                </td>
            </tr>
            <tr>
            	<th><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e('Save Changes') ?>" /></th>
                <td></td>
            </tr>
		</table>

		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="emb_divider,emb_home,emb_class,emb_customfield,emb_input,emb_frontpage" />

	</form>
	</div>
		
<?php }



?>