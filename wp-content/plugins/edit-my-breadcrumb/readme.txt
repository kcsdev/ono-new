=== Edit My Breadcrumb ===
Contributors: raptor3300
Donate link: 
Tags: breadcrumbs, customizable, page navigation
Requires at least: 3.0
Tested up to: 4.0
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Edit My Breadcrumb is a plugin for WordPress that allows for breadcrumbs to be added to your page.

== Description ==

Edit My Breadcrumb is a plugin for WordPress that allows for breadcrumbs to be added to your page. It is fully customizable for your website needs.  There are two options to implement this into your site; web developers can add a function into their theme or it can be automatically added before your content.

== Installation ==

1. Upload 'edit-my-plugin' folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Either activate the automatic output in the settings area or use the function `<?php display_breadcrumbs(); ?>`

== Frequently asked questions ==

= How do I automatically add breadcrumbs to my site? =

In the settings configuration, enable 'Breadcrumb Auto Output' to display the breadcrumbs directly the start of your content.

== Screenshots ==

1. /assets/edit-my-breadcrumb.png

== Changelog ==

= 1.2 =
* Added option for homepage to display the breadcrumb or not to.

= 1.1 =
* Added option for edit the first Home breadcrumb
* Updated Admin area with proper Wordpress Admin CSS
* Added description and settings instructions to admin area.

= 1.0 =
* Initial Release

== Upgrade notice ==

= 1.1 =
New feature to edit Home breadcrumb and Admin area interface changes.

= 1.0 =
Initial Release


== Arbitrary section 1 ==

