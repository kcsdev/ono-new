=== Q and A - FAQ Plugin ===
Contributors: daltonrooney 
Tags: FAQs, custom posts 
Requires at least: 3.0.1
Donate link: http://daltonrooney.com/wordpress/donate
Tested up to: 3.0.1
Stable tag: 0.1.3

Create, categorize, and reorder FAQs and insert them into a page with a shortcode.

== Description ==

Create, categorize, and reorder FAQs and insert them into a page with a shortcode. Questions are shown/hidden with a nice jQuery animation, users without javascript enabled will click through to the single question page. Based on the new Custom Post Type in WordPress 3.0.

This plugin is sponsored by <a href="http://storycorps.org">StoryCorps</a>.

The plugin also includes functionality from the <a href="http://geekyweekly.com/mypageorder">My Page Order</a> plugin by Andrew Charlton.

== Installation ==

Extract the zip file and upload the contents to the wp-content/plugins/ directory of your WordPress installation and then activate the plugin from plugins page. 

Use shortcode [qa cat="category-slug"] to insert your FAQs into a page. The "cat" attribute is optional and will include only questions assigned to that category. Leaving out the "cat" attribute will show all FAQs. You can also insert a single FAQ with the format [aq id="1234"] where 1234 is the post ID. Note: the cat & the id attributes are mutually exclusive. Don't use both in the same shortcode.

== Frequently Asked Questions ==


= Can it do this, this, or that? =

Not yet. Maybe someday. You might want to drop a link in the [forum](http://daltonrooney.com/wordpress/forum).

== Screenshots ==


1. Example of FAQs on page.

2. FAQ entry page

3. FAQ reorder page (includes code from My Page Order by Andrew Charlton)

== Changelog ==

= 0.1.3 = 

* You can now grab single FAQs by ID and insert them into a page.

= 0.1.2 =

* Changed permalinks to FAQs to a more compatible format in case javascript is disabled.

= 0.1.1 =

* Added category titles to displayed FAQs

= 0.1 =

* First version