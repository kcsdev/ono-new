=== Video Widget ===
Contributors: nikohk
Tags: video, widget, sidebar
Requires at least: 2.5
Tested up to: 2.5
Stable tag: 1.0

The Video Widget adds videos to your sidebar. Only video Id is required (no HTML code to paste).

== Description ==

The Video Widget adds videos to your sidebar. Only video Id is required (no HTML code to paste).

All accepted video sites: 
Youtube, Dailymotion, Google Video, Metacafe, LiveLeak, Revver, iFilm, MySpace, Blip.tv, CollegeHumor, VideoJug, GodTube, Veoh, Break, Movieweb, Jaycut, Myvideo, Quicktime, Windows media player

This plugin is based on [Executable PHP widget](http://wordpress.org/extend/plugins/php-code-widget/ "Link to Executable PHP widget homepage") for multiples widgets, [YouTube widget](http://nothingoutoftheordinary.com/2007/05/31/wordpress-youtube-widget "Link to YouTube widget homepage") for the idea and [Video Embedder](http://www.gate303.net/2007/12/17/video-embedder "Link to Video Embedder homepage") for the video html library.

For support and further information about the Video Widget plugin see the plugins homepage at [Video Widget homepage](http://www.nikohk.com/plugin-wordpress-video-widget/ "Link to Video Widget homepage").


== Installation ==

1. Upload `video.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Use the widget like any other widget.

== Frequently Asked Questions ==

= And the others videos =

Tell me. :)

== Screenshots ==

1. Video Widget Configuration