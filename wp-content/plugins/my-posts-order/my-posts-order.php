<?php
/*
Plugin Name: My Posts Order
Description: A plugin which allows you to sort posts in any order.
Author: Kapil Chugh
Author URI: http://kapilchugh.com/
Version: 1.0.2
*/

 	include 'includes/db-schema.php';//Custom table is added
	require_once 'includes/defines.php';
	require_once 'includes/functions.php';
	require_once 'includes/widget.php';
	require_once 'classes/Section/Section.php';


	register_activation_hook(__FILE__, 'install_sections_table');

  add_action( 'admin_head', 'add_js_in_theme' );
  function add_js_in_theme() { ?>
	<script type="text/javascript">var MPO_IMAGES_PATH = '<?php echo MPO_IMAGES_PATH; ?>';</script>
    <link rel="stylesheet" href="<?php echo MPO_CSS_PATH; ?>theme-editor.css" type="text/css" media="screen" /> <?php
  }

	add_action( 'wp_print_scripts', 'custom_theme_scripts', 100);
  function custom_theme_scripts() {
		wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'my_posts_order', MPO_JS_PATH . 'my_posts_order.js', 'jquery', '1.0', true );
    wp_enqueue_script( 'tablednd', MPO_JS_PATH . 'jquery.tablednd.js' );
  }

  add_action('admin_menu', 'add_custom_admin_page');

	// Add menu page
	function add_custom_admin_page() {
		add_options_page('My Posts Order Options', 'My Posts Order Options', 'manage_options', 'my-posts-order', 'custom_optons_posts_order');
	}

	function custom_optons_posts_order () {
		require_once('includes/select_criteria.php');
	}

	 add_action( 'wp_ajax_save_section_data', 'save_section_data' );

  function save_section_data() {
    require_once ('includes/save_section_data.php');
    exit;
  }

  add_action( 'wp_ajax_add_edit_section', 'add_edit_section' );

  function add_edit_section() {
    require_once ('includes/add_edit_criteria.php');
    exit;
  }

  add_action( 'wp_ajax_edit_section', 'edit_section' );

  function edit_section() {
    require_once ('includes/edit_section.php');
    exit;
  }

  add_action( 'wp_ajax_delete_section_data', 'delete_section_data' );

  function delete_section_data() {
    require_once ('includes/delete_section_data.php');
    exit;
  }
?>