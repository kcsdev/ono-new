<?php 
  if ( isset($rss_items) && count($rss_items) > 0) {
	 echo $before_widget;
     if ( $title ) echo $before_title . $title . $after_title;?>
    <ul> <?php
      foreach ( $rss_items as $item ) { ?>
        <li>         
          <a target="_blank"  href="<?php echo $item->get_permalink();?>"><?php echo $item->get_title(); ?></a>         
        </li> <?php
      } ?>
    </ul> <?php
	echo $after_widget;
  } else {
    $the_query = new WP_Query($args);
    if ($the_query->have_posts()) {
     echo $before_widget;
     if ( $title ) echo $before_title . $title . $after_title; ?>
       <ul>  <?php
         while ($the_query->have_posts()) : $the_query->the_post(); ?>
           <li><a href="<?php the_permalink() ?>" title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>"><?php if ( get_the_title() ) the_title(); else the_ID(); ?></a></li> <?php
         endwhile; ?>
       </ul> <?php
       echo $after_widget;
       // Reset the global $the_post as this query will have stomped on it
       wp_reset_postdata();
    } else {
      echo 'No Featured Posts Found';
    }
   }
?>