<?php
	function display_radio_buttons($selection_array = array(), $radio_button_name, $content_type = '', $disabled_radion_buttons_array = array()) {
    $count = 0;
    $disabled = '';
    $divisor = '';
    foreach ($selection_array as $sel_key => $sel_val) {
      $divisor = 3;
      if ($count % $divisor == 0) { ?>
        <kbd> <?php
      }
      if ($content_type == $sel_key) {
        $selected = 'checked="true"';
      } else {
        $selected = '';
      }
      if (in_array($sel_key, $disabled_radion_buttons_array)) {
        $disabled = 'disabled="true"';
      } else {
        $disabled = '';
      } ?>
      <i><input type="radio" <?php echo $disabled; ?> <?php echo $selected; ?> class="<?php echo $radio_button_name; ?>" name="<?php echo $radio_button_name; ?>"  id="<?php echo $sel_key; ?>" value="<?php echo $sel_key; ?>" ></i><small><?php echo $sel_val; ?></small> <?php
      $count++;
      if ($count % $divisor == 0 ) { ?>
        </kbd> <?php
      }
    }
  }

  function get_section_info($section_identifier, $info = 'theme_file') {
    global $global_section_array;
	if ( !is_array($global_section_array[$section_identifier]) ) {
		return false;
	}
    if (array_key_exists($info, $global_section_array[$section_identifier])) {
     return $global_section_array[$section_identifier][$info];
    } else {
      return false;
    }
  }

  add_action( 'wp_ajax_search_posts', 'search_posts' );

  function search_posts() {
    $_POST      = array_map( 'stripslashes_deep', $_POST );

    $search_str = $_POST['search_str'];
    $args = array('s' => $search_str);
    require_once ('show_posts.php');
    exit;
  }

  function get_section_details($section_identifier) {
    if ($section_identifier) {
			global $all_section_data;
			if (is_array($all_section_data)) {
				if (array_key_exists($section_identifier, $all_section_data)) {
					return $all_section_data[$section_identifier];
				} else {
					return false;
				}
			}
		}
	}

	function display_selection_box ($selection_box_name, $max = 5, $selected = 0) { ?>
    <select id="<?php echo $selection_box_name; ?>" name="<?php echo $selection_box_name; ?>"> <?php
      for ($i = 1; $i<= $max; $i++) {
        if ($selected == $i) {
          $sel = 'selected';
        } else {
          $sel = '';
        } ?>
        <option <?php echo $sel; ?> value="<?php echo $i; ?>"> <?php echo $i; ?></option> <?php
      } ?>
    </select> <?php
  }

  function specific_content_orderby( $orderby ) {
    global $post_ids;
    $orderby = " FIELD(ID, $post_ids)";
    return $orderby;
  }