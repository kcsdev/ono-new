<?php

/**

Plugin Name: Responsive Flip Book WordPress Plugin
Plugin URI: http://blog.mpcreation.pl
Description: This is a jQuery Flip Book plugin, no Flash Player required. Gives each user the same experience (mobile & desktop)..
Version: 1.3.2
Author: MassivePixelCreation
Author URI: http://blog.mpcreation.pl

**/

/*-----------------------------------------------------------------------------------*/
/*	Globals
/*-----------------------------------------------------------------------------------*/

global $shortname;
global $mpcrf_options;

$shortname = 'rfbwp';

/*-----------------------------------------------------------------------------------*/
/*	Constants
/*-----------------------------------------------------------------------------------*/

define('MPC_PLUGIN_ROOT', get_bloginfo('wpurl').'/wp-content/plugins/responsive-flipbook');

/*-----------------------------------------------------------------------------------*/
/*	Add CSS & JS
/*-----------------------------------------------------------------------------------*/

function rfb_enqueue_scripts() {

	// CSS
	wp_enqueue_style('rfbwp-styles', MPC_PLUGIN_ROOT.'/css/style.css');
	wp_enqueue_style('rfbwp-page-styles', MPC_PLUGIN_ROOT.'/css/page-styles.css');

	// JS
	wp_enqueue_script('custom-shortcodes', MPC_PLUGIN_ROOT.'/js/shortcodes.js', array('jquery'));
	wp_enqueue_script('swfobject', MPC_PLUGIN_ROOT.'/js/swfobject2.js', array('jquery'));
	wp_enqueue_script('jquery-easing', MPC_PLUGIN_ROOT.'/js/jquery.easing.1.3.js', array('jquery'));
	wp_enqueue_script('jquery-doubletab', MPC_PLUGIN_ROOT.'/js/jquery.doubletap.js', array('jquery'));
	wp_enqueue_script('jquery-color', MPC_PLUGIN_ROOT.'/js/jquery.color.js', array('jquery'));
}

add_action('wp_enqueue_scripts', 'rfb_enqueue_scripts');

add_action('wp_head', create_function('', 'echo \'<!--[if lt IE 9]><script>var rfbwp_ie_8 = true;</script><![endif]-->\';'));

/*--------------------------- END CSS & JS -------------------------------- */

/*-----------------------------------------------------------------------------------*/
/*	Plugin Setup
/*-----------------------------------------------------------------------------------*/

function rfb_plugin_setup() {

	/*-----------------------------------------------------------------------------------*/
	/*	Hook MPC Shortcode button & Shortcodes Source
	/*-----------------------------------------------------------------------------------*/

	require_once ('tinymce/tinymce-settings.php');
	require_once ('includes/theme-shortcodes.php');

}

add_action('after_setup_theme', 'rfb_plugin_setup');

/*-----------------------------------------------------------------------------------*/
/*	Hook Massive Panel & Get Options
/*-----------------------------------------------------------------------------------*/

if(is_admin()) {
	require_once('massive-panel/theme-settings.php');
}

function mp_get_global_options() {
	global $shortname;
	$mp_option = array();
	$mp_option = get_option($shortname.'_options');
	return $mp_option;
}

$mpcrf_options = mp_get_global_options();

/*--------------------------- END Massive Panel Hook -------------------------------- */

/*-----------------------------------------------------------------------------------*/
/*	Add Flip Book to the stage
/*-----------------------------------------------------------------------------------*/
if(!is_admin()) {
	require_once('php/settings.php');
}

function rfbwp_add_book($att, $content = null) {
		wp_enqueue_script('turn-js', MPC_PLUGIN_ROOT.'/js/turn.js', array('jquery'));
		wp_enqueue_script('flipbook-js', MPC_PLUGIN_ROOT.'/js/flipbook.js', array('jquery'));

		global $mpcrf_options;

		$id = $att['id'];

		if($id == "")
			return "Oops! You need to specify flip book id inside the shortcode.";
		else
			$book_name = $id;

		$i = 0;

		// get the book ID based on the books name
		foreach($mpcrf_options['books'] as $book) {
			if(strtolower(str_replace(" ", "_", $book['rfbwp_fb_name'])) == $id)
				break;
			$i++;
		}

		$id = $i;

		if(!isset($mpcrf_options['books'][$id]['pages']) || $mpcrf_options['books'][$id]['pages'] == '')
			return 'ERROR: There is no book with ID <strong>'.$book_name.'</strong>';

		rfbwp_setup_css($id, $mpcrf_options);

		$menuType = strtolower($mpcrf_options['books'][$id]['rfbwp_fb_nav_menu_type']);

		$arrows = ($mpcrf_options['books'][$id]['rfbwp_fb_nav_arrows'] == 1)? true : false;

		$output = '';
		$output .= '<div id="flipbook-container-'.$id.'" class="flipbook-container">';
		$output .= '<div id="flipbook-'.$id.'" class="flipbook' . (!$arrows ? ' no-arrows' : '') . '">';

		/* Insert flipbook pages */
		foreach($mpcrf_options['books'][$id]['pages'] as $page) {
			$pageType = ($page['rfbwp_fb_page_type'] == 'Double Page') ? 'double' : 'single';
			$output .= '<div class="fb-page '.$pageType.'">'; // page wrap;
			$output .= '<div class="page-content">'; // page content wrap;

			$output .= '<div class="container">';

			$custom_class = '';
			if(!empty($page['rfbwp_fb_page_custom_class']) && !empty($page['rfbwp_page_css'])) {
				$custom_class = ' ' . $page['rfbwp_fb_page_custom_class'];

				$output .= '<style>' . PHP_EOL;
				$output .= $page['rfbwp_page_css'];
				$output .= '</style>' . PHP_EOL;
			}

			$output .= '<div class="page-html' . $custom_class . '">';
			$output .= do_shortcode(stripslashes(stripslashes($page['rfbwp_page_html'])));
			$output .= '</div>';

			$output .= '<img src="'.$page['rfbwp_fb_page_bg_image'].'" class="bg-img"/>';

			if($page['rfbwp_fb_page_bg_image_zoom'] != '')
				$output .= '<img src="'.$page['rfbwp_fb_page_bg_image_zoom'].'" class="bg-img zoom-large"/>';

			$output .= '</div>';

			$output .= '</div>'; // end page content wrap
			$output .= '</div>'; // end page wrap
		}

		$output .= '</div>'; /* end flipbook */
		$output .= '<div id="fb-zoom-out-'.$id.'" class="fb-zoom-out"><span class="fb-zoom-out-icon"></span></div>';
		$output .= '<div id="fb-nav-'.$id.'" class="fb-nav mobile '.$menuType.'">';
		$output .= '<ul>';

		$numberOfButtons = 0;

		if($mpcrf_options['books'][$id]['rfbwp_fb_nav_toc'] == '1')
			$numberOfButtons++;

		if($mpcrf_options['books'][$id]['rfbwp_fb_nav_zoom'] == '1')
			$numberOfButtons++;

		if($mpcrf_options['books'][$id]['rfbwp_fb_nav_ss'] == '1')
			$numberOfButtons++;

		if($mpcrf_options['books'][$id]['rfbwp_fb_nav_sap'] == '1')
			$numberOfButtons++;

		if($mpcrf_options['books'][$id]['rfbwp_fb_nav_fs'] == '1')
			$numberOfButtons++;

		for($i = 1; $i < $numberOfButtons+1; $i++) {

			$class = '';

			if($i == 1)
				$class .= 'left';
			elseif($i == $numberOfButtons)
				$class .= 'right';
			else
				$class .= 'center';

			if($menuType == 'spread')
				$class = 'round';

			if($mpcrf_options['books'][$id]['rfbwp_fb_nav_toc'] == '1' && $mpcrf_options['books'][$id]['rfbwp_fb_nav_toc_order'] == $i)
				$output .= '<li class="toc '.$class.'">Table Of Content</li>';

			if($mpcrf_options['books'][$id]['rfbwp_fb_nav_zoom'] == '1' && $mpcrf_options['books'][$id]['rfbwp_fb_nav_zoom_order'] == $i)
				$output .= '<li class="zoom '.$class.'">Zoom</li>';

			if($mpcrf_options['books'][$id]['rfbwp_fb_nav_ss'] == '1' && $mpcrf_options['books'][$id]['rfbwp_fb_nav_ss_order'] == $i)
				$output .= '<li class="slideshow '.$class.'">Slide Show</li>';

			if($mpcrf_options['books'][$id]['rfbwp_fb_nav_sap'] == '1' && $mpcrf_options['books'][$id]['rfbwp_fb_nav_sap_order'] == $i)
				$output .= '<li class="show-all '.$class.'">Show All Pages</li>';

			if($mpcrf_options['books'][$id]['rfbwp_fb_nav_fs'] == '1' && $mpcrf_options['books'][$id]['rfbwp_fb_nav_fs_order'] == $i)
				$output .= '<li class="fullscreen '.$class.'">Fullscreen</li>';
		}

		$output .= '</ul>';
		$output .= '</div>'; /* end navigation */
		$output .= '</div>'; /* end flipbook-container */

		return $output;
}

add_shortcode('responsive-flipbook', 'rfbwp_add_book');

?>