<?php 
/**
 * Template Name: תבנית התמחויות
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header();
?>
<style>
.entry-content {
width:500px;
}

	#primary {	
		margin-top:40px;
		
	}
#container {
		
		margin: 0 0 0 -350px;
	}
	#content {
		float: right;
		
		
		margin: 0 20px 36px 0px;
	}
	.widget-area{margin-top:17px;}
	.side_menu{
		width:326px;
	}
	.sidebar_h2 {padding-bottom:10px;margin-top:10px;}
	.widget-container { margin: 0 0 8px;}
	#content .entry-title{font-size:27px;top:255px; right:20px}
	
</style>
		<div class="upper_banner">

			<?php 

$pageparentarray =  get_page( $page_id,ARRAY_A ); 
	  $pageparent = $pageparentarray[post_parent];
	  $parent_title = get_the_title($post->post_parent);
	  $page_ancestors = get_post_ancestors( $post );	

	  $parent = wp_list_pages('title_li=&child_of='.$pageparent.'&echo=0&depth=1&exclude=11846');
	  $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=1&exclude=11846');
	  $parent_url = get_permalink( $pageparent );
	  //echo $parent;
	
	//print_r($split_parent);
	//meta('upperpic');
	$meta_values = get_post_meta($post->ID, 'upperpic', true);
	$registration_link = get_post_meta($post->ID, 'registration_forms_url', true);
	$online_registration_link = get_post_meta($post->ID, 'online_registration_url', true);
	
//ob_start();
//    	//meta('upperpic');
//    	$content = ob_get_contents();
//	ob_end_clean();
	
if($meta_values){echo $meta_values;}
else{

		//foreach($page_ancestors as $post_id){
		//	
		//		if (get_post_meta($post_id, 'upperpic',true)){
					
					
					//because doesn't work from admin
					echo  '<p><img src="http://www.ono.ac.il/wp-content/uploads/2013/10/mba-upper.png" alt="" /></p>';
				//	break;
		//		}
		//}
	}
 ?> 
		</div>
		<div id = 'bread' style ="padding-top:3px;padding-bottom:5px;"> 
<?php if (class_exists('breadcrumb_navigation_xt')) {
	
	
if ($lang == 'he'){ echo 'הינך כאן : ';}
if ($lang == 'en'){echo 'You are here :'; }
	// New breadcrumb object
	$mybreadcrumb = new breadcrumb_navigation_xt;
	// Options for breadcrumb_navigation_xt
	$mybreadcrumb->opt['title_blog'] = '';
	$mybreadcrumb->opt['separator'] = ' &raquo; ';
	$mybreadcrumb->opt['singleblogpost_category_display'] = true;
	// Display the breadcrumb
	$mybreadcrumb->display();
} 


?>
<?php if ($lang != 'en' && ($post->post_parent !='1683' && !in_array('1683',$page_ancestors) && $post->ID !='1683') &&  ($post->post_parent !='24100' && !in_array('24100',$page_ancestors) && $post->ID !='24100')): ?>
<a href="https://yedion.ono.ac.il/Yedion/fireflyweb.aspx?appname=BSHITA&prgname=Enter_1" style="font-size:14px;float:left;font-weight:bold;">להרשמה אונליין – לחצו כאן</a>
<?php endif;?>

		</div>
		<div id="container">
			<div id="content" role="main">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php
						// WP_Query arguments
						$args = array (
							'post_type' => 'specialities',
						);

						// The Query
						$query = new WP_Query( $args );
						?>
						
						<?php
						// The Loop
						if($query->have_posts()) : $right = 0; ?>
						<div class="clear_both"></div>
						<ul class="speciallize_list">
						   <?php while($query->have_posts()) : $query->the_post() ?>
						   <?php $meta_values = get_post_meta($query->post->ID, 'url', true); ?>
								<li class="<?php echo ($right % 2 == 0 ? 'right' : 'left')?>">
									<div class="pic"><?php the_post_thumbnail() ?></div>
									<div class="text">
										<h2><?php echo $query->post->post_title; ?></h2>
										<?php echo limit_words(strip_tags($query->post->post_content),10); ?>
										<p><a class="button" href="<?php print_r($meta_values); ?>">לתכנית התמחות</a></p>
									</div>
								</li>
						   <?php $right++; endwhile ?>
						   <div class="clear_both"></div>
						</ul>
						<?php endif;
						// Restore original Post Data
						wp_reset_postdata();
						?>
						<div class="form_lead">
							<div class="header">
								<div class="right"><h2>לפרטים והרשמה</h2></div>
								<div class="left"><h2 style="color:#c3d81b !important;">שיחת חינם: <a href="tel:1800550555" style="color:#c3d81b !important;">1-800-550-555</a></h2></div>
								<div style="clear:both;"></div>
							</div>
							<div class="formbg">
								<p>&nbsp;</p>
								<p>&nbsp;</p>
								<form action="https://web2.ono.ac.il/leeds/SqlCheckIncludeLeeds.asp" id="leeds" method="post" name="leeds" target="_top">
									<input id="sourceSite" name="sourceSite" type="hidden" value="ono" /> 
									<input id="interest" name="interest" type="hidden" value="מנהל עסקים" /> 
									<input id="sourceID" name="sourceID" type="hidden" value="6" /> 
									<input id="sourceType" name="sourceType" type="hidden" value="2" />
									<table border="0" cellpadding="0" cellspacing="0" id="table1" width="100%">
										<tbody>
											<tr>
												<td style="width: 119px" valign="top"><strong>שם פרטי:</strong></td>
												<td valign="top"><input name="firstName" type="text" /></td>
												<td style="width: 119px" valign="top"><strong>שם משפחה:</strong></td>
												<td valign="top"><input name="lastName" type="text" /></td>
											</tr>
											<tr>
												<td style="width: 119px" valign="top"><strong>טלפון:</strong></td>
												<td valign="top"><input name="mobile" type="text" /></td>
												<td style="width: 119px" valign="top"><strong>דואר אלקטרוני:</strong></td>
												<td valign="top"><input name="eMail" type="text" /></td>
											</tr>
											<tr>
												<td colspan="2">
													<p>הקריה האקדמית אונו לא תעביר את פרטיך לגורם צד ג'</p>
												</td>
												<td align="left" colspan="2" dir="rtl">
													<input dir="ltr" name="send" style="font-family: Arial; margin-left:41px;" type="submit" value="שליחה" />
												</td>
											</tr>
										</tbody>
									</table>
								</form>
							</div>
						</div>
						<div class="page_bottom_links">
							<?php my_share_links(); ?>
						</div>
						<div class="google_plus_and_facebook">
							 <?php do_action( 'wp_plus_one_button', $url, $style, $css, $count, $redirect); ?>
							 <br />
							  <?php
								$perma=get_permalink();
								$title=get_the_title();
								$postid = strval($post->ID);
								$args = array (
											'page_id' => $postid,
											'heading' => "0",
											'size' => "16",
											'list_style' => "icon_text",
											'direction' => 'down',
											'facebook' => 'yes',
											'twitter'=>'no',
											'delicious'=>'no',
											'digg'=>'no',
											'reddit'=>'no',
											'myspace'=>'no',
											'hyves'=>'no',
											'orkut'=>'no',
											'share'=>'no',
											'page_title'=>$title,
											'page_link'=>$perma,
											'echo'=>'0',
											'facebook_share_text' => '',
											'stumble_share_text'=> 'Share with Stumblers',
											'twitter_share_text'=>'Tweet this',
											'delicious_share_text'=>'Bookmark on Delicious',
											'digg_share_text'=>'Digg this',
											'reddit_share_text'=>'Share on Reddit',
											'hyves_share_text'=>'Tip on Hyves',
											'orkut_share_text'=>'Share on Orkut',
											'myspace_share_text'=>'Share via MySpace',
								);
								social_links($args);
								?>
						</div>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
						
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

				<?php comments_template( '', true ); ?>
<?php endwhile; ?>
			</div><!-- #content -->
<?php
if (($pageparent=='812' &&  $post->ID !='594' && $post->ID !='598') || ($pageparent=='9453' && $post->ID !='9449')) {
		?>
		<div class = 'allnav' style="float:left;width:324px;">
			
			<div class = "nav_menu" style='padding-top:50px; position:relative;'>

				<div class = "menulevel1">

					<?php
					echo "<ul class = 'pagenav'>";
					wp_list_pages( 'depth=2&title_li=&exclude=11846' );
					 ?>
		 			
					</ul>
			</div>
 		</div>
		<?php
	} else {
	?>
<?php
if($post->post_parent) {
			$parent1 = get_post($post->post_parent);
		}
if ($post->post_parent =='1683' || $post->ID =='1683' || in_array('1683',$page_ancestors)): ?>		
		<div class = 'custom_sidebar' style="float:left;">
			<?php 
			
			
			ob_start();
    	get_sidebar();
    	$content = ob_get_contents();
	ob_end_clean();



if($content){
echo "<div class = 'news_slider'>";

echo $content;

echo "</div>";
 }

			 
			?>
 
	</div>
<?php endif;?>
		<div class = 'allnav' style="float:left;width:324px;">
			

<?php //start menu

//if ($children){
	
	
	
	echo '<div class = "nav_menu">';
	
	
	
	
	
	$split_parent = split('</li>',$parent);
		
		 //begin to echo menu
	 
		
	if ((count($page_ancestors) > 1 ) || ($children)){	
	//echo '<div class="side_menu_top">&nbsp;</div>';
	
	}
	
	
	//if (($content2)||($children)) {echo '<div class="side_menu_top">&nbsp;</div>';}
		
		echo '<ul class = "menulevel1">';
		
		
			//new menu
			
		if ((count($page_ancestors) > 2 ) && (!$children)){	
			
			// dont_show_menu will stop any other echoing of the menu
			$dont_show_menu = true;
		
		        $grand_parent_children = wp_list_pages('title_li=&child_of='.$pageparent.'&echo=0&depth=1&exclude=11846');	
			$parent_info = get_page($pageparent, ARRAY_A);
			$grand_parent = $parent_info[post_parent];
			//			print_r ($parent_info);
			
			$grand_nav = wp_list_pages('title_li=&child_of='.$grand_parent.'&echo=0&depth=1&exclude=11846');
			$split_grand_parent = explode('</li>',$grand_nav);
			
			//echo "<ul class = menulevel1>" .$grand_nav. "</ul>";
			//echo "<!--1111111111"; echo $grand_parent_children;echo "-->";
			//foreach($split_grand_parent as &$v){
			//	
			//	if (substr_count($grand_parent_children,trim($v))>0) {
			//		unset($v);
			//	}
			//}
			
			foreach($split_grand_parent as $uncle){
				    echo $uncle;

				    if (strrpos($uncle,'current_page_parent')){
				    	//echo ';;;;;;;;;;;;;;;;;;<br/>';
						
							echo '<ul class = "new_menulevel2">' . $grand_parent_children . "</ul>";
						
			   	    }
				    
		 	}
	
			
		}
		if ((count($page_ancestors) > 1 )&&(!$dont_show_menu)){ 
		//show page parent from third level down
		if (count($page_ancestors) > 2 ){echo "<li class = 'papa_page'><a href ='$parent_url'>$parent_title</a></li>";} //this will need to be edited
		
			foreach($split_parent as $uncle){
				echo $uncle;
				    if (strrpos($uncle,'current_page_item')){
						if ($children){ //to make sure no empty ul is created
							echo '<ul class = "menulevel2">' . $children . "</ul>";
				}
			   }
			}
		      }//end counting ancestors
		else{
			//if (count($page_ancestors) > 3 ){}
			if (!$children) { 
				//Commented Out 02.08.2012 - KCS
				//foreach($split_parent as $uncle){
				//	echo $uncle;
				//	    if (strrpos($uncle,'current_page_item')){
				//			if ($children){ //to make sure no empty ul is created
				//				echo '<ul class = "menulevel2">' . $children . "</ul>";
				//	}
				//   }
				//}
			} else {
			
				echo '<ul class = "menulevel1">' .$children . "</ul>";
			}
		}
		//if (($content2)||($children)) {//echo '<div class="side_menu_bot"></div>';}
	echo '</div> ';
}

  //menu end here?>
 
<?php if ($post->post_parent !='1683' && $post->ID !='1683' && !in_array('1683',$page_ancestors) ): ?>		
		<div class = 'custom_sidebar'>
			<?php 
				ob_start();
				get_sidebar();
				$content = ob_get_contents();
				ob_end_clean();
	
				if($content){
					echo "<div class = 'news_slider'>";
					
					
					echo "<h2 class='sidebar_h2'>ימים ושעות הלימוד</h2>";
					
					$h_posts = get_posts(array('posts_per_page'   => 10,'category'=> 1264));
					
				?>
				<div class='sidebar'>
					<?php foreach($h_posts as $h_post): ?>	
						
						<div class='block1 minhal_block'>
						   <div class='header'>
							   <div class='right'><?php echo $h_post->post_title;?></div>
							   <div class='left'><?php echo get_post_meta($h_post->ID, 'post_date', true);?></div>
						   </div>
						   <div class='details'>
							   <div class='header_text'>
								   <?php echo $h_post->post_content;?>
							   </div>
							   <div class='header_text2'>
								   <p><?php echo get_post_meta($h_post->ID, 'learn_hours', true);?></p>
							   </div>
						   </div>
						</div>
					<?php endforeach; ?>
				</div>
<h2 class='sidebar_h2' style='margin-top:50px;'>תנאי קבלה לM.B.A.</h2>
<div class='conditions'>
	<ul>
		<?php
			$links = get_links(1265,'<li>', '</li>', '', FALSE, 'id', TRUE,FALSE, -1, TRUE, TRUE); 
			//print_r($links);
		?>
	</ul>
	<a class='sidebar_button'>לפרטים על תנאי הקבלה</a>
	<a class='sidebar_button'>הורדת תכנית התואר</a>
	<div class='clear_both'></div>
</div>
<h2 class='sidebar_h2' style='margin-bottom: 10px !important;margin-top: 40px !important;'>המרצים שלכם</h2>
			<?php		
					echo $content;
					echo "</div>";
				 }
			?>
	</div>
<?php endif;?>
<div class="lecture">
<?php if(function_exists('staff_directory_custom')){ echo staff_directory_custom(8,3); } ?>
</div>
<?php if ($post->post_parent !='1683' && !in_array('1683',$page_ancestors)  && $post->ID !='1683' ): ?>
<div class="fb-like-box" data-href="https://www.facebook.com/OnoCollege" data-width="320" data-height='300' data-show-faces="true" data-border-color="#C3D81B" data-stream="false" data-header="false"></div>
<?php endif;?>
<div class="prepration">
	<a href="<?php echo $registration_link; ?>">טפסי רישום והכנה</a> 
	<a href="<?php echo $online_registration_link; ?>" class="space">להרשמה אונליין ל.M.B.A</a> 
</div>
<?php get_footer(); ?>
</div><!-- #container -->

</div>
<?php
function first_words($string, $num, $tail=' ...')
{
       /** words into an array **/
       //$words = str_word_count_utf8($string);
               
               $words = explode  ( ' '  , $string , $num + 1 );
               if (count($words) <= $num) return $string;
       /*** get the first $num words ***/
       $firstwords = array_slice( $words, 0, $num);
       /** return words in a string **/
       return implode(' ', $firstwords).$tail;
}

?>
