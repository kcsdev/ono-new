<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<style type="text/css">
	#main{padding: 40px 0 0 0;}
	#container {
		float: right;
		margin: 0 0 0 -350px;
	}
	#content {
		float: right;
		margin: 0 20px 0px 0px;
		width:550px;
	}
	.entry-content{padding: 8px 0px 0px;};
	
	.side_menu{
		width:326px;
	}	
	.entry-title{
	line-height:24px;
	}
</style>
<div class="upper_banner">

<img src = 'images/Blank2.jpg' alt = ''></img>
		</div>

		<div id="container">

			<div id="content" role="main">
			


<?php
echo '<div class = "maybe_in_banner">';
$category = get_the_category( $post->ID ) ; 
echo '</div>';
$category = ($category[0]->name);

echo "<h1 class = 'cat_name_title'>$category</h1>";
if ( have_posts() ) while ( have_posts() ) : the_post(); 

?>


				

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 class="entry-title"><?php the_title(); ?></h1>

					<div class="entry-meta">
						<?php //twentyten_posted_on(); ?>
					</div><!-- .entry-meta -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->

<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
					<div id="entry-author-info">
						<div id="author-avatar">
							<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyten_author_bio_avatar_size', 60 ) ); ?>
						</div><!-- #author-avatar -->
						<div id="author-description">
							<h2><?php printf( esc_attr__( 'About %s', 'twentyten' ), get_the_author() ); ?></h2>
							<?php  the_author_meta( 'description' ); ?>
							<div id="author-link">
								<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
									<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentyten' ), get_the_author() ); ?>
								</a>
							</div><!-- #author-link	-->
						</div><!-- #author-description -->
					</div><!-- #entry-author-info -->
<?php endif; ?>

					<div class="entry-utility">
						<?php //twentyten_posted_in(); ?>
						<?php //edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->
				</div><!-- #post-## -->

				

				<?php comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>

	</div><!-- #content -->
		<div class = 'allnav' style="float:left;width:324px;">
			<div class = "nav_menu" style='padding-top:50px; position:relative;'>

				<div class = "menulevel1">

					<?php
					echo "<ul class = 'pagenav'>";
					wp_list_pages( 'depth=2&title_li=&exclude=11846' );
					 ?>
		 			
					</ul>
			</div>
 		</div>
 	</div>
		

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
</div><!-- #container -->