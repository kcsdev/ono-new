<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	</div><!-- #main -->
<?php	$url_end = $_GET['lang'];
$lang = 'he';

$page_ancestors = get_post_ancestors( $post );	

switch($url_end){
	case 'en':
	$lang = 'en';
	break;
	case 'ar':
	$lang = 'ar';
	break;
	case 'ru':
	$lang = 'ru';
	break;
	default:
	$lang = 'he';
	break;
}


$utm_source=isset($_COOKIE['utm_source']) ? $_COOKIE['utm_source'] : @$_REQUEST['utm_source'];
$utm_medium=isset($_COOKIE['utm_medium']) ? $_COOKIE['utm_medium'] : @$_REQUEST['utm_medium'];
$utm_campaign=isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : @$_REQUEST['utm_campaign'];	

?>
	<div class="footer">
			<div class="contact">

			
			
			<?php
			
			if (substr_count(urldecode(get_permalink()),"ייעוץ-ורישום-תכנית-הציונות-הדתית")>0 || substr_count(urldecode(get_permalink()),"היחידה-ללימודי-חוץ")>0) {
			
			} else {
				?>
				<img src="<?php          if ($lang=='he'){echo 'images/contact_title.png';}
						 if ($lang=='en'){echo 'images/english/contact_title.png';}
						?>" alt="">
				
				<?

				if ($lang=='en') {echo do_shortcode('[contact-form 18 "FotterContactForm"]');}
				if ($lang=='he') {echo do_shortcode('[contact-form 5 "צור קשר פוטר"]');}
				//if ($lang=='he') {echo '<iframe src="http://ono.ac.il/ono-form/" width="979" height="80"></iframe>';}
			}
			
			
			?>

			</div>
			<div class="ft_links" style="height:200">
				<div class="link_up" style="height:400">
<ul>
	<li>
	<p class="MsoNormal" dir="RTL">
	<span lang="HE" style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/schools2/llb/"><strong>משפטים</strong></a></span>
	<br />
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/llb">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	לימודי משפטים</span></a><br>
	<a href="http://www.ono.ac.il/תחומי-לימוד/תואר-ראשון-במשפטים/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר ראשון במשפטים</span></a><br>
	<a href="http://www.ono.ac.il/schools2/llb/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר שני במשפטים</span></a><br>
	</p>
	</li>
	<li>
	<p class="MsoNormal" dir="RTL">
	<span lang="HE" style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<strong><a href="http://www.ono.ac.il/schools2/business-administration/">מנהל 
	עסקים</a></strong></span><br>
	<a href="http://www.ono.ac.il/תחומי-לימוד/הפקולטה-למנהל-עסקים/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר ראשון במנהל עסקים</span></a><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/15093/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר ראשון בשיווק</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/14882/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר ראשון בתקשורת</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/תחומי-לימוד/הפקולטה-למנהל-עסקים/תואר-שני-במנהל-עסקים/התמחויות-לתואר-שני/m-b-a-התמחות-בשיווק-ופרסום/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	לימודי פרסום</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/תחומי-לימוד/הפקולטה-למנהל-עסקים/b-a-במנהל-עסקים/התמחויות-b-a/b-a-התמחות-במימון-ושוק-ההון/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	לימודי מימון</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://ono.ac.il/תחומי-לימוד/הפקולטה-למנהל-עסקים/b-a-במנהל-עסקים/התמחויות-b-a/b-a-התמחות-חשבונאות/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	לימודי חשבונאות</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/תחומי-לימוד/הפקולטה-למקצועות-הבריאות/קלינאות-תקשורת/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	קלינאות תקשורת</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/תחומי-לימוד/הפקולטה-למקצועות-הבריאות/ריפוי-בעיסוק/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	ריפוי בעיסוק</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/תחומי-לימוד/הפקולטה-למנהל-עסקים/תואר-שני-במנהל-עסקים/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר שני במנהל עסקים</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/תחומי-לימוד/הפקולטה-למנהל-עסקים/b-a-במנהל-עסקים/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	לימודי מנהל עסקים</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/תחומי-לימוד/הפקולטה-למנהל-עסקים/תואר-שני-במנהל-עסקים/">
	<span dir="LTR" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	MBA</span></a></span><br>
	</p>
	</li>
	<li>
	<span lang="HE" style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<strong><a href="http://www.ono.ac.il/schools2/בית-ספר-למוסיקה/">בית ספר למוסיקה</a></strong></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/schools2/בית-ספר-למוסיקה/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר ראשון במוסיקה</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/schools2/בית-ספר-למוסיקה/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	לימודי מוסיקה</span></a></span><br>
	</li>
	<li>
	<span lang="HE" style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<strong><a href="http://www.ono.ac.il/hutz">היחידה ללימודי חוץ</a></strong></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/schools2/היחידה-ללימודי-חוץ/בתי-ספר/מיסים/קורס-חשבי-שכר/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	קורס חשבי שכר</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/schools2/היחידה-ללימודי-חוץ/בתי-ספר/חשבונאות-וכספים/קורס-הנהלת-חשבונות-סוג-3/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	קורס הנהלת חשבונות</span></a></span><p>
	</p>
	</li>
</ul>


					
					<div class="lin">
					<a href="/feed/"><img src="images/c_rss.png"></a>
					<a  id = 'use_terms' href="http://www.ono.ac.il/10923/">תנאי שימוש</a><!-- תנאי שימוש-->
					<a target="_blank" href="http://twitter.com/onoun
"><img src="images/c_twit.png"></a>
					<a target="_blank" href="http://www.youtube.com/user/OnoAcademicCollege"><img src="images/c_ytube.png"></a>
					<a target="_blank" href="http://www.facebook.com/OnoCollege
"><img src="images/c_face.png"></a>		
 <a id = 'kcslink' href='http://www.kcsnet.net/'>בניית אתרים</a>
 <img id="kidumitimg" src="/wp-content/themes/mytheme/images/kidumit.png">
<a id='kidumitlink' href="http://www.kidumit.org/">שיווק באינטרנט</a>

					</div>
				</div>
				<div class="link_down">
		
					<li><a href ='http://www.kcsnet.net/' > &nbsp;</a></li>
				</div>
			</div>
		</div><!-- #footer -->

</div><!-- #wrapper -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<? if ($post->post_parent !='1683' && !in_array('1683',$page_ancestors) && $post->ID !='1683' ): ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1081317-1']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();



</script>
<?php else:?>
<style>.custom_sidebar .wpcf7-response-output {
    margin-right: 160px;
    margin-top: -27px;}</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7023584-1']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   // ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- BEGIN WebLeads Chat Code -->
<script>
var prom = (("https:" == document.location.protocol) ? "https://" : "http://");
document.write ("<script type='text/javascript' src='" + prom + "wl.wizsupport.com/collect/webleadscoil/chatfloater.asp?lang=he&profile=232&url=" + escape(window.location) + "'></script"); 
document.write(">");
</script> 
<script> var prom = (("https:" == document.location.protocol) ? "https://" : "http://"); document.write("<script type='text/javascript' src='" + prom + "wl.wizsupport.com/collect/webleadscoil/collect.asp?url=" + escape(window.location) + "&refer=" + escape(document.referrer) + "&title=" + escape(document.title) + "&profile=232'></script"); 
document.write(">");
</script> 
<!-- END WebLeads Monitor. -->

<?php endif;?>

<?php
	if ($post->post_parent =='242' || in_array('242',$page_ancestors)  || $post->ID =='242' ): ?>	
<!-- BEGIN LivePerson Button Code -->
<div id="lpButDivID-1308486170595" class="chat" style="left:352px;"></div>
<script type="text/javascript" charset="UTF-8" src="https://server.iad.liveperson.net/hc/48236541/?cmd=mTagRepstate&site=48236541&buttonID=12&divID=lpButDivID-1308486170595&bt=1&c=1"></script>-->


	<!-- BEGIN LivePerson Monitor. --><script language='javascript'> var lpMTagConfig = {'lpServer' : "server.iad.liveperson.net",'lpNumber' : "13943730",'lpProtocol' : (document.location.toString().indexOf('https:')==0) ? 'https' : 'http'}; function lpAddMonitorTag(src){if(typeof(src)=='undefined'||typeof(src)=='object'){src=lpMTagConfig.lpMTagSrc?lpMTagConfig.lpMTagSrc:'/hcp/html/mTag.js';}if(src.indexOf('http')!=0){src=lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpServer+src+'?site='+lpMTagConfig.lpNumber;}else{if(src.indexOf('site=')<0){if(src.indexOf('?')<0)src=src+'?';else src=src+'&';src=src+'site='+lpMTagConfig.lpNumber;}};var s=document.createElement('script');s.setAttribute('type','text/javascript');s.setAttribute('charset','iso-8859-1');s.setAttribute('src',src);document.getElementsByTagName('head').item(0).appendChild(s);} if (window.attachEvent) window.attachEvent('onload',lpAddMonitorTag); else window.addEventListener("load",lpAddMonitorTag,false);</script><!-- END LivePerson Monitor. -->
<?php else: ?>

<div id="lpButDivID-1296375712440" class='chat'>
<!-- BEGIN WebLeads Chat button Code -->
<script>
var prom = (("https:" == document.location.protocol) ? "https://" : "http://");
document.write ("<script type='text/javascript' src='" + prom + "wl.wizsupport.com/collect/webleadscoil/isonline.asp?profile=289&lang=he'></script"); 
document.write(">");
</script>
<!-- END WebLeads Chat button Code -->
</div>

<!-- BEGIN WebLeads Chat Code -->
<script>
var prom = (("https:" == document.location.protocol) ? "https://" : "http://");
document.write ("<script type='text/javascript' src='" + prom + "wl.wizsupport.com/collect/webleadscoil/divchatfloat.asp?profile=289&lang=he&url=" + escape(window.location) + "'></script"); 
document.write(">");
</script>
<script>
var prom = (("https:" == document.location.protocol) ? "https://" : "http://");
document.write ("<script type='text/javascript' src='" + prom + "wl.wizsupport.com/collect/webleadscoil/chatfloater.asp?lang=he&profile=289&url=" + escape(window.location) + "'></script"); 
document.write(">");
</script> 
<script> var prom = (("https:" == document.location.protocol) ? "https://" : "http://"); document.write("<script type='text/javascript' src='" + prom + "wl.wizsupport.com/collect/webleadscoil/collect.asp?url=" + escape(window.location) + "&refer=" + escape(document.referrer) + "&title=" + escape(document.title) + "&profile=289'></script"); 
document.write(">");
</script>
<!-- END WebLeads Chat Code -->
<?php endif; ?>



<?php if( $post->ID !="16353" && $post->ID !="16984" && $post->ID !='11430'):?>
<!-- Google Code for Ono Site General Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "JSA4CL_xvAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=JSA4CL_xvAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif;?>



<?php if ($post->post_parent =='1172' || in_array('1172',$page_ancestors)  || $post->ID =='1172' ): ?>	
<!-- Google Code for BA Minhal Asakim Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "oKhkCLfyvAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=oKhkCLfyvAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif;?>

<?php if ($post->post_parent =='1563' || in_array('1563',$page_ancestors)  || $post->ID =='1563' ): ?>
<!-- Google Code for Heasbonut Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "47-LCKf0vAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=47-LCKf0vAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif;?>


<?php if ($post->post_parent =='13862' || in_array('13862',$page_ancestors)  || $post->ID =='13862' ): ?>
<!-- Google Code for Pirsum Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "7EtrCI_3vAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=7EtrCI_3vAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif;?>


<?php if ($post->post_parent =='1187' || in_array('1187',$page_ancestors)  || $post->ID =='1187' ): ?>

<!-- Google Code for LLB Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "8owICJ_1vAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=8owICJ_1vAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif; ?>

<?php if ($post->post_parent =='1208' || in_array('1208',$page_ancestors)  || $post->ID =='1208' ): ?>
<!-- Google Code for Briut Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "5odxCJf2vAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=5odxCJf2vAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif; ?>

<?php if ($post->post_parent =='1184' || in_array('1184',$page_ancestors)  || $post->ID =='1184' ): ?>
<!-- Google Code for MBA New Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "mdv0CJfQ6gIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=mdv0CJfQ6gIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php endif; ?>

<?php if ($post->ID=='11430'):?>

<!-- Google Code for צור קשר אתר - 2012 Conversion Page --> <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1050446152;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "JrcqCLjykAMQyJLy9AM"; var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript"
src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="http://www.googleadservices.com/pagead/conversion/1050446152/?value=0&amp;label=JrcqCLjykAMQyJLy9AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif; ?>


<?php if ($post->ID=='22054'): ?>
	<!-- Google Code for MBA Open Day Blat Aug 12 -->
	<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1038494939;
	var google_conversion_label = "AYVmCP_5zxQQ29mY7wM";
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1038494939/?value=0&amp;label=AYVmCP_5zxQQ29mY7wM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
<?php endif;?>


<?php if ($post->ID =='16680'): ?>
	<img border="0" src="http://r.turn.com/r/beacon?b2=txGSj5PySDUvP03hLoMVt9Wtl_A41Y0WQUS-JOyAhWPTXfCvTbQnWcxARm9NycW6qAGZOYJ61utKuZ297c6JwQ&cid=">
<?php endif;?>
<?php if (@$_GET['hidden-66']=="בית ספר למוסיקה"): ?>
	<img border="0" src="http://r.turn.com/r/beacon?b2=2tNQlcAnvkK1A4ucjWX0llxIKVhcJnYxv9zgBYaVNWzTXfCvTbQnWcxARm9NycW6A6-L6-QsQtDJ2AGNzaBecg&cid=">
<?php endif;?>
<?php if (is_home()):?>
	<img border="0" src="http://r.turn.com/r/beacon?b2=6cXxAeuT918CFPuwf-odXzHzozc2Ta8ttRl1JXhXdHnTXfCvTbQnWcxARm9NycW67k-t5XUH7Vt_zpGFq_5i1w&cid=">
<?php endif;?>
<?php if ($post->ID =='1245'):?>
	<img border="0" src="http://r.turn.com/r/beacon?b2=-5gWMrcUG479Df6Qp0sRPsRhR5bN7tax9VHMUtW3qqfTXfCvTbQnWcxARm9NycW6zgp4wTEBwsw_iR-Yb68EBw&cid=">
<?php endif;?>


</body>
</html>