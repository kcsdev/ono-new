<?php 
/**
 * Template Name: תבנית סגל
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */



get_header();
?>
<style>
.contact {padding-right:0px !important;}
</style>
<script type="text/javascript" src="js/jScrollPane.js"></script>
<link rel="Stylesheet" type="text/css" href="js/css/jScrollPane.css" />
	
		<div id = 'bread' style ="padding-top:18px;padding-bottom:5px;"> 
<?php if (class_exists('breadcrumb_navigation_xt')) {
	
	
echo 'הינך כאן : ';
	// New breadcrumb object
	$mybreadcrumb = new breadcrumb_navigation_xt;
	// Options for breadcrumb_navigatioobject Objectn_xt
	$mybreadcrumb->opt['title_blog'] = '';
	$mybreadcrumb->opt['separator'] = ' &raquo; ';
	$mybreadcrumb->opt['singleblogpost_category_display'] = true;
	// Display the breadcrumb
	$mybreadcrumb->display();
} 
?>
		</div>






<?php

// ------------------------------------------    print scrolling pictures      ---------------------------------------------------------------- -->


?>


    <link rel="Stylesheet" type="text/css" href="js/css/smoothDivScroll.css" />
<div id ="makeMeScrollable" style = 'direction:ltr;' >
<div class="scrollingHotSpotRight"></div>
    <div class="scrollingHotSpotLeft"></div>
	
	<div class="scrollWrapper">
		<div class="scrollableArea">
	
		
	<?php
	$myrows = $wpdb->get_results( "SELECT name, email,photo,bio,position,post_link FROM `wp_staff_directory` WHERE `show` = 'both' or `show` = 'top'" );
	$photo_counter = 0;
	$bio_array = array();
	
     $array_length = count($myrows);
       foreach($myrows as $row){
       	   if($array_length%2 == 0){//if zugi
       		if (round($array_length/2) == $photo_counter){
			echo "<img class = 'head_master grey_image' id = 'headmaster_y_jpg'  src='/wp-content/uploads/staff-photos/grey/headmaster.jpg' alt ='' width ='200' height='200' style = 'bottom:0px;'>";
		}       	
	   }else{//end zugi if
	   	if (round($array_length/2) -1 == $photo_counter){ //if not zugi -
			echo "<img class = 'head_master grey_image' id = 'headmaster_y_jpg' src='/wp-content/uploads/staff-photos/grey/headmaster.jpg' alt ='' width ='200' height='200' style = 'bottom:0px;'>";
		}
	   }
	  		 
		$photo = $row->photo;
		$name = $row->name;
		$bio = $row->bio;
		$link = $row->post_link;
		if($photo){
			if ($photo != 'headmaster.jpg' ){ //dont want headmaster to appear twice
				echo "<img class = 'grey_image' id = '". str_replace('.','_y_',$photo)."' src='/wp-content/uploads/staff-photos/grey/" .$photo ."' alt='' width='130' height='130' style = 'postion:abolute;'>";
			}
		}
		//if($photo){echo "<img class = 'colour_image' id = '$photo' src='/wp-content/uploads/staff-photos/" ."$photo ."' alt='' width='100' height='100' style = 'postion:abolute;'>";}
		$photo_counter++;

		
	
	}


?>
		</div>
		</div>
		
	</div>

<div class = 'bio_section'> 


<?php 
//print bio section
	foreach($myrows as $row){
		
		echo "<div class = 'top_bio_div' id ='bio_for_" . str_replace('.','_y_',$row->photo) ."'>";
			
				echo "<h1> $row->name </h1>";
				echo "<h3>$row->position</h3>";
				echo '<br/>';
				echo "<a href = '$row->email'>$row->email</a> ";
				echo '<br/>';

				echo $row->bio;
				echo "<a href ='/?page_id=$row->post_link'>לעמוד האישי </a>";
				
		echo "</div>";
		
		
	}
	

	?>
	
</div>	

   

<!-- ------------------------------------------   end  print scrolling pictures      ---------------------------------------------------------------- -->




<!-- ------------------------------------------   init staff directory      ---------------------------------------------------------------- -->


	<?php 
	//initialize all variables for successfull interaction with DB
	
	
	$categories_db = $wpdb->get_results( "SELECT cat_id,name FROM wp_staff_directory_categories" );
	$s_subcat = $wpdb->get_results( "SELECT subcat FROM wp_staff_directory" );//needs to be unuique
	$subcategories = array();
	$final_query = array();	

	//print_r($categories_db);
	$another_i = 0;
	//print_r($s_subcat);
	
	$subcat_array = array();
	foreach($s_subcat as $s_instance){ //destroy subcat duplicates
		$subcat_array[] = $s_instance->subcat;
	}

	$subcat_array = array_unique($subcat_array);
	
	
	//	loop_category(4,$subcat_array);	//example of the function call

		
			

	?>	
	



<!-- close this div -->



<div class = 'wrapmagic2'>


    <ul id = "magic_top">
		<li> <a id = "academy" class = 'upcat_link' onmouseOver = 'toggleCatType("academicul")'>סגל אקדמי </a></li>
	
		<li id = 'magicup3'></li> 
		<li class = 'staff_search'>
	<input type = 'text' value = 'חיפוש' id = 'staff_search_field' onFocus="value=''" /> 
	<button id = submit_staff_search> </button>
	</li>
	
		
	</ul> <!--end magic_top-->
	
</div>
   
<!--	<div class="submenu_slider"></div> -->
    
	

	<div class = 'magic2categories'>
		<div class = 'academicul'>
		
	<?php
	
	createCatLinks('סגל אקדמי');
	
	?>
		<div class = 'mymagic magicline3'>
		<p></p>
		</div>
		</div>
        
     <div class = 'minhalaul'>
	<?php
	
	createCatLinks('סגל אדמינסטרטיבי');
	
	?>
	
	
	
	<div class = 'mymagic magicline3'>
	<p></p>
	</div>
	
	
	
		</div>
		
		
    </div><!--close magoc2categories -->


<!-----------------------------------------------  end magic line ---------------------------------------------------------------------------->




<!-- ------------------------------------------   print staff academic directory      ---------------------------------------------------------------- -->
	<div class = 'staff_dir_div'> 
		<div class = 'search_div'> </div>
		<?php
		//creates the ul with the pictures and people
		$all_category = $wpdb->get_results( "SELECT name,cat_id FROM wp_staff_directory_categories");
		$iii = 0;
		foreach($all_category as $cats_name){
		
			if ($cats_name->name != 'Uncategorized'){
				$iii++;
		
				echo "<ul class = 'staff_dir' id = 'staff_dir_" .$cats_name->cat_id . "'>";
					//print_r($subcat_array);
					loop_category($cats_name->cat_id,$subcat_array);
			
				echo '</ul> <!-- staff_dir--> ';

			}
		}

		?>
<?php get_footer(); ?>
	</div>







<script  src="js/jquery.ui.core.js" type="text/javascript"></script>
<script src="js/jquery.ui.widget.js" type="text/javascript"></script>

<script type="text/javascript" src="js/jquery.smoothDivScroll-1.1.js"></script>


<script type="text/javascript">



/*
function doupload(num)
{
 	
 	

    
   // alert('done'+num);
      }});
  if (num <6000) setTimeout("doupload("+(1+num)+")", 5000);
 else alert('here'+num);
  
}


    $j(document).ready(function() {
            
        //
    
    });
*/


	$j(document).ready(function() {
	  
	 <?php //iterate_all_pages(); ///DANGER -- iterate through all pages and send a get request --> triggers ~ 400 GET's, each one goes to the edit screen of particular page and resets it ?>
	
	$j('#staff_search_field').keypress(function(event) {
		  if (event.which == '13') {
		     var staff_query = $j('#staff_search_field').attr('value');
		   // staff_query = escape(staff_query);
		    
	//alert(staff_query);
	$j('.staff_dir_div').children().css('display','none');
	$j('.search_div').css('display','block');
	$j('.search_div').load('search_query.php?phrase='+ encodeURIComponent(staff_query) +'&lang_id=20') ;

//alert('search_query.php?phrase='+ staff_query +'');
		   }
	})
  	$j('.top_bio_div').css('display','none');
	$j('#bio_for_headmaster_y_jpg').css('display','block');
		$j(function() {
		$j("div#makeMeScrollable").smoothDivScroll({ visibleHotSpots:'always',countOnlyClass:'.grey_image', autoScrollDirection: "backandforth", autoScrollStep: 1, autoScrollInterval: 15, startAtElementId: "startAtMe", visibleHotSpots: "always"});
	});
	
	$('.scrollableArea').width($('.scrollableArea').width+70)
	

	$j('#submit_staff_search').click(function(){
	var staff_query = $j('#staff_search_field').attr('value');
	staff_query = encodeURIComponent(staff_query);
	//escape()
	//alert(staff_query);
	$j('.staff_dir_div').children().css('display','none');
	$j('.search_div').css('display','block');
	$j('.search_div').load('search_query.php?phrase='+ encodeURIComponent(staff_query) +'') ;

	
	})
	
	$j('.open_info').click(show_info);
	
   
});
	function show_info(){
		$j('.img_div').css('display','none');
		$j('.new_class').removeClass('new_class');
		$j(this).parent().children().css('display','block');
		$j(this).parent().addClass('new_class');
	}
		$j('#bio_for_headmaster_y_jpg').css('display','block');
	




	$j('.cat_link').hover(function(){
	
		var rightpos = $j(this).position() ;//pressed position for magic line
		var anchor_object_width = $j(this).width();
		//alert(rightpos.left);
	
		$j('.magicline3').stop().animate({
		left: rightpos.left,
		width: anchor_object_width
	
		});
		//alert(rightpos.left);
	});
	function show_cat_div(cat_id){
	
	//display requested div && remove the ones already displayed
	//cat_id = cat_id-1; //why?
	$j('.search_div').css('display','none');
	$j('.staff_dir').css('display','none');
	$j('#staff_dir_' + cat_id + '').css('display','inline-block');
	
		
		$j('.staff_category_type_title:empty').parent().css('display','none');
	

	
	}
      
	  $j('.upcat_link').hover(function(){
	var rightpos = $j(this).parent().position() ;//pressed position for magic line
	
	//$j('#magicup3').stop().animate({left: rightpos.left-360});
	//alert(rightpos.left);
	});
	

	  function toggleCatType(type){
	  
        if (type == 'minhalaul'){
	   $j('.magicline3').stop().css('left',833);
	   $j('.magicline3').css('width', 66);
	  }
	  
	  if (type == 'academicul'){
	   $j('.magicline3').stop().css('left',789);
	   $j('.magicline3').css('width', 110);
	   }
	
	$j('.magic2categories').children().css('display','none');
	$j('.magic2categories .'+ type + '').css('display','block');
		
	}
	
$j('.grey_image').click(enlarge);
$j('.colour_image').click(enlarge);
$j('#headmaster_y_jpg').parent().append("<img class = 'colour_image' src='/wp-content/uploads/staff-photos/headmaster.jpg' alt='' width='200' height='200' style = 'position:absolute !important ; left:390px; z-index:101;'>")
var head_off = $j('#headmaster_y_jpg').position();
//alert(head_off.left);
$j('.colour_image').position(head_off);
	
	
	
function enlarge(){
	$j('.grey_image').css('bottom',-70);
	$j(this).css('bottom',0);

	$j('.grey_image').stop(true, true).animate({ opacity: 1.0 }, 400);
	$j('.grey_image').css('height','130px');
	$j('.grey_image').css('width','130px');
	$j('.colour_image').remove();

	$j(this).css('height','200px');
	$j(this).css('width','200px');
        var thisoff = $j(this).offset();
	var thisid = $j(this).attr('id');
	
	$j(this).parent().append("<img class = 'colour_image' src='/wp-content/uploads/staff-photos/" + thisid.replace('_y_','.') +"' alt='' width='200' height='200' style = 'postion:abolute;'>");
	$j('.colour_image').offset(thisoff);
	 $j(this).stop(true, true).animate({ opacity: 0.0 }, 1200);
	$j('.top_bio_div').css("display","none");	 
	 var thisbio = "#bio_for_" + thisid;
	//alert ($j(thisbio).parent().attr('id'));
	
	 var x = $j(thisbio).css('display','block');
	
	 

	}
	
	/*$j(function()
{
	$j('.scroll-pane').jScrollPane();
});*/
</script>




<?php function loop_category($category,$subcat_array){
			//print_r($subcat_array);
			foreach ($subcat_array as $subcategory){
			//echo '--------------- second loop ------------';
			global $wpdb;
				
					
					$finalquery[] = array($wpdb->get_results( "SELECT 	wp_staff_directory.name,wp_staff_directory.photo,
					wp_staff_directory.position,wp_staff_directory.bio,wp_staff_directory.post_link,wp_staff_directory.email
					FROM wp_staff_directory INNER JOIN wp_staff_directory_categories ON 
					(wp_staff_directory.category =  wp_staff_directory_categories.cat_id)
					WHERE wp_staff_directory_categories.cat_id = '$category' 
					AND wp_staff_directory.subcat = '$subcategory' AND (wp_staff_directory.show = 'bottom' OR wp_staff_directory.show = 'both')  ORDER BY wp_staff_directory.name "  ), $subcategory);
				
				/*if ($type == 'minhala'){
					$finalquery[] = array($wpdb->get_results( "SELECT 	wp_staff_directory.name,wp_staff_directory.photo,
					wp_staff_directory.position,wp_staff_directory.bio
					FROM wp_staff_directory INNER JOIN wp_staff_directory_categories ON 
					(wp_staff_directory.category =  wp_staff_directory_categories.cat_id)
					WHERE wp_staff_directory_categories.cat_id = '$category' 
					AND wp_staff_directory.subcat = 'מנהלה'" ), $subcategory);
				}*/
	}

			//print_r($finalquery);
			$ii = 0;
			foreach($finalquery as $myfinal){
			   echo "<div class = 'staff_category_type  scroll-pane $myfinal[1]'> ";
			   $objet = $myfinal[0];
			   echo "<div class = 'staff_category_type_title'>";
			   if ($objet[0]){echo "            $myfinal[1]";}
			   echo "</div>";
			  
			   foreach($myfinal[0] as $finalfinal){
				$ii++;
				
				//echo '--------------- third loop ----------------';			
				//print_r($finalquery);				
				//check for bugs from div id
						echo "<div class = 'bio_div'>";
							
							echo "<div class = 'img_div'>";
							

							if ($finalfinal->photo !="")  echo "<a href ='/?page_id=" . "$finalfinal->post_link'><img src = '/wp-content/uploads/staff-photos/$finalfinal->photo'/></a>";

							
							
							//echo "<h5>" . first_words($finalfinal->bio, 8) ."</h5>";
							echo '</div>';
							
							
							
							//echo "<div class = 'bio_div_name'>";
							echo "<a class = 'open_info'  href ='/?page_id=" . "$finalfinal->post_link'><h3> $finalfinal->name </h3></a>";
							echo "<h4> $finalfinal->position </h4>";
							echo "<div class = 'img_div email_div'>";
							echo "<a href = '$finalfinal->email'> $finalfinal->email</a>";
							echo "</div>";
							//echo "</div>";
							
							
							
							
				
					
							
						echo "</div>";
							
					}
			echo "</div>";
			}
		} 
function first_words($string, $num, $tail=' ...')
{
       /** words into an array **/
       //$words = str_word_count_utf8($string);
               
               $words = explode  ( ' '  , $string , $num + 1 );
               if (count($words) <= $num) return $string;
       /*** get the first $num words ***/
       $firstwords = array_slice( $words, 0, $num);
       /** return words in a string **/
       return implode(' ', $firstwords).$tail;
}

function iterate_all_posts(){
	$all_posts = get_posts('numberposts=-1');

	foreach($all_posts as $mypost){
		//echo $mypost->ID;
		
	}
}

function iterate_all_pages(){

	$all_pages = get_posts('numberposts=-1&post_type=page');
	foreach($all_pages as $mypost){
		
//		echo '$j.ajax({ url: "http://ono1.c14.co.il/wp-admin/post.php?post=' . $mypost->ID . '&action=edit", context: document.body, success: function(){ }});';

	}
	
}

 function createCatLinks($type){
		global $wpdb;
		$all_category = $wpdb->get_results( "SELECT name,cat_id FROM wp_staff_directory_categories WHERE cat_type = '$type'");
	//	print_r ($subcat_array);
		foreach ($all_category as $cats_name){
		
			if ($cats_name->name != 'Uncategorized'){
			echo "<div class = 'bottom_category'>";
				
				echo "<a class = 'cat_link' onclick = 'show_cat_div($cats_name->cat_id)'> $cats_name->name </a>"; //onclick = 'show_cat_div($cats_name->cat_id)'
			echo "</div> <!--bottom_category -->";
			}	
		}
	}
	
?>
