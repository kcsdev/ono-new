<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); 

$url_end = $_GET['lang'];
$lang = 'he';



switch($url_end){
	case 'en':
	$lang = 'en';
	break;
	case 'ar':
	$lang = 'ar';
	break;
	case 'ru':
	$lang = 'ru';
	break;
	default:
	$lang = 'he';
	break;
}

?>

    <div class="imghome">
    
                <div class="imghome">
			<?php 
			
//if ( function_exists( 'get_smooth_slider' ) ) {
     if ($lang == 'he'){
		//echo "<iframe width='980' height='580' scrolling='no'frameborder='0' src='http://ono.ac.il/main_banner/' style='margin-top:10px'></iframe>";
		get_smooth_slider(1);
	//echo '<br/><embed pluginspage="http://www.macromedia.com/go/getflashplayer" src="/wp-content/uploads/loader_onoBrands_980x493.swf" width="980" height="493" wmode="transparent" type="application/x-shockwave-flash" play="true" loop="true" menu="true" style="border-right: black; border-top: black; border-left: black; border-bottom: black"></embed>';
	} 
     if ($lang == 'en'){get_smooth_slider(5);
	//echo '<br/><embed pluginspage="http://www.macromedia.com/go/getflashplayer" src="/wp-content/uploads/loader_onoBrands_980x493.swf" width="980" height="493" wmode="transparent" type="application/x-shockwave-flash" play="true" loop="true" menu="true" style="border-right: black; border-top: black; border-left: black; border-bottom: black"></embed>';
	}
     
  //   }  ?>
			<!--<div class="home-contact">
			 <img src="<?php
						 //if ($lang=='he'){echo 'images/home-contact.png';}
						 //if ($lang=='en'){echo 'images/english/home-contact.png';
						 //}
						?>" alt="">
		        </div>-->
			<?php /*
			<div class="home-contact-open">	
				<div class="open-contact">
					<?php 
					if ($lang=='he'){echo do_shortcode('[contact-form 9 "טופס דף ראשי"]');}
						 if ($lang=='en'){echo do_shortcode('[contact-form 19 "main-page-form"]');}
					
					
					  ?>
				</div>
			</div>
			
			      */?>
		</div>
		<div class="content_h">
			<div class="contenthome">
				<img alt="" onclick="news_fade('news_slider')" src="<?php
						 if ($lang=='he'){echo 'images/c_news.png';}
						 if ($lang=='en'){echo 'images/english/c_news.png';}
						?>"/>
				<a class="more" href="<?php
						 if ($lang=='he'){echo '/index.php?cat=3';}
						 if ($lang=='en'){echo '/cat/englishnews?lang=en';}
						?>"><?php
						 if ($lang=='he'){echo 'עוד חדשות';}
						 if ($lang=='en'){echo 'more';}
						?></a>
				<a class="next_slide" onclick="news_fade('news_slider')"><?php
						 if ($lang=='he'){echo 'הבא';}
						 if ($lang=='en'){echo 'next';}
						?></a>
				<div id="news_slider" class="news_slider">
					<?php $temp_query = $wp_query; $nump=1; ?>
					<?php  
					

						 if ($lang=='he'){query_posts('category_name=news&showposts=4');}
						 if ($lang=='en'){query_posts('category_name=news&showposts=131');}
						?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php $do_not_duplicate[$post->ID] = $post->ID; ?>
					<div <?php echo "class=\"slider_content\""; echo "id=\"";echo $nump; echo "\" "; if($nump==1){echo "style=\"display:block;\"";}$nump+=1;?> >
					<div class="glidetext">
					<strong class="news_title" ><a onclick='' alt = "" href="<?php the_permalink();?>" <?php if (get_post_meta($post->ID, "open_new", true)):?>target='_blank'<?php endif;?> ><?php the_title();?></a></strong>
					<div>
						<?php the_glide_limit3_home(250); ?>
					</div>
					</div>
					<?php
						//echo "<a class=\"news_more\" href=\"";
						//echo the_permalink();
						//echo "\"> המשך </a>";
					?>
					</div>
					<?php endwhile; else: ?>
        				<?php endif; ?>
					
					<?php
					
					if ($lang=='en'){
						 $my_posts = get_posts("category=136&numberposts=8");
							
							foreach($my_posts as $my_post){
							
							?>
							<div <?php echo "class=\"slider_content\""; echo "id=\"";echo $nump; echo "\" "; if($nump==1){echo "style=\"display:block;\"";}$nump+=1;?> >
							
						<div class="glidetext">
						<strong class="news_title" ><a onclick='' alt = "" href="<?php echo $my_post->guid;?>" ><?php echo $my_post->post_title;?></a></strong>
						<div>
							<?php echo  strip_tags(first_words(strip_tags (  $my_post->post_content,'<p><a>' ),15,"<a href ='$my_post->guid' >  continue reading-></a>"),'<a>') ;
							//the_glide_limit3($max_char, $more_link_text = '(more...)', $stripteaser = 0, $more_file = ''); ?>
						</div>
						</div>
						</div>
						<?php } ?>
					
					<?php }
					?>
					
					
					
				</div>	
			</div>
			<div class="contenthome">
				<img onclick="news_fade('events_slider')" alt="" src="<?php
						 if ($lang=='he'){echo 'images/c_event.png';}
						 if ($lang=='en'){echo 'images/english/c_event.png';}
						?>">
				<a class="more" href="<?php
						 if ($lang=='he'){echo '/index.php?cat=4';}
						 if ($lang=='en'){echo '/cat/ono-events?lang=en';}
						?>"><?php
						 if ($lang=='he'){echo 'עוד ארועים';}
						 if ($lang=='en'){echo 'more';}
						?></a>
				<a class="next_slide" onclick="news_fade('events_slider')"><?php
						 if ($lang=='he'){echo 'הבא';}
						 if ($lang=='en'){echo 'next';}
						?></a>

					<div id="events_slider" class="events_slider" >
					<?php $temp_query = $wp_query; $nump=1; ?>
					<?php     query_posts('category_name=events&showposts=4');
					
					$querystr = "SELECT * FROM $wpdb->posts
					
					LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
					LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
					LEFT JOIN $wpdb->terms ON ($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)
					WHERE  $wpdb->terms.slug = 'events'
					AND $wpdb->posts.post_status = 'publish'
					
					AND NOT EXISTS (SELECT meta_value FROM $wpdb->postmeta WHERE $wpdb->postmeta.post_id=ID  AND $wpdb->postmeta.meta_key='expiration-date' AND $wpdb->postmeta.meta_value < ".time().")
					ORDER BY post_date DESC LIMIT 4";
					
					$events = $wpdb->get_results($querystr, OBJECT);
					
					?>
					<?php if ($events) :
					
					foreach($events as $post): ?>
					<?php $do_not_duplicate[$post->ID] = $post->ID; ?>
					<div <?php echo "class=\"slider_content\""; echo "id=\"";echo $nump; echo "\" "; if($nump==1){echo "style=\"display:block;\"";}$nump+=1;?> >
					<div class="glidetext">
					<strong class="news_title"><a onclick='' alt = "" href="<?php echo the_permalink();?>" <?php if (get_post_meta($post->ID, "open_new", true)):?>target='_blank'<?php endif;?> ><?php $event_title = $post->post_title;
					echo first_words($event_title, 16, $tail=' ...');
					?>
					
					</a></strong>
					
						<?php $chars = get_option('glideshow-text-length');?>
						<div class="time">
							<?php 
							      $post_Data =  get_post_custom_values("event_date", $post->ID);
							      $post_Time =  get_post_custom_values("event_time", $post->ID);
							list ($post_year,$post_month,$post_day) = split ('[/.-]', $post_Data[0]); ?>
  							<span class="year">
								<?php echo $post_year; ?>
							</span>
							<span class="date">
								<?php echo $post_day.'.'.$post_month; ?>
							</span>
							<span class="hour">
								<?php echo $post_Time[0] ?>
							</span>
						</div>
						<div onclick="news_fade('events_slider')">
						<?php the_glide_limit3_home(150); ?>
						</div>
					</div>
					<?php
						//echo "<a class=\"news_more\" href=\"";
						//echo the_permalink();
						//echo "\"> המשך </a>";
					?>
					</div>
					<?php endforeach; else: ?>
        				<?php endif; ?>
					
					<?php
					
					if ($lang=='en'){
						 $my_posts = get_posts("category=137&numberposts=8");
						 
							foreach($my_posts as $my_post){
							
							?>
							<div <?php echo "class=\"slider_content\""; echo "id=\"";echo $nump; echo "\" "; if($nump==1){echo "style=\"display:block;\"";}$nump+=1;?> >
							
						<div class="glidetext">
						<strong class="news_title" ><a onclick='' alt = "" href="<?php echo $my_post->guid ; ?>" ><?php echo $my_post->post_title;?></a></strong>
						<div>
							<?php 
							echo  strip_tags(first_words(strip_tags (  $my_post->post_content,'<p><a>' ),15,"<a href ='$my_post->guid' > continue reading-></a>"),'<a>') ;
							//the_glide_limit3($max_char, $more_link_text = '(more...)', $stripteaser = 0, $more_file = ''); ?>
						</div>
						</div>
						</div>
						<?php } ?>
					
					<?php }
					?>
			</div>
		</div>

                       <!--<a href="http://www.youtube.com/v/VEl1rQ9r8jE" title="Image Title" rel="shadowbox;player=swf;width=640;height=385;" alt="" >-->
			<!--<a href="http://web2.ono.ac.il/SchoolsCourses/SchoolsCoursesSelect.asp " title="Image Title"  alt="" target="_blank">-->
			<a href="https://yedion.ono.ac.il/Yedion/fireflyweb.aspx?appname=BSHITA&prgname=Enter_1" title="Image Title"  alt="" target="_new">
			<img src="images/RegistrationOnline.jpg" width="320"></a>
			<!--<div class="homevideo">
				<p><?php
						// if ($lang=='he'){echo '"אנו בקריה האקדמית אונו מציבים את הבעיות החברתיות בראש סדר העדיפויות, במטרה להוביל לשינוי מהותי בקרב האוכלוסיות המודרות. לכן אנו  פועלים למציאת פתרונות שיאפשרו נגישות ללימודים אקדמיים"';}
						// if ($lang=='en'){echo "Ono Academic College's goal is to give top priority to the social problems of the State of Israel and create major social change by opening the door of opportunity to higher education for various sectors of the population.";}
						?></p>
				<img class="play_icon" src="images/play_small.png">
			</div>-->
                       
		</div>
		
		
<script type="text/javascript">
".contenthome"

</script>

<?php //get_sidebar();?>
<?php

$post->post_title="Home";


if (function_exists('icl_object_id')){}

 get_footer();

function first_words($string, $num, $tail=' ...')
{
       /** words into an array **/
       //$words = str_word_count_utf8($string);
               
               $words = explode  ( ' '  , $string , $num + 1 );
               if (count($words) <= $num) return $string;
       /*** get the first $num words ***/
       $firstwords = array_slice( $words, 0, $num);
       /** return words in a string **/
       return implode(' ', $firstwords).$tail;
}

?>