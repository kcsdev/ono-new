<?php
/*
Copyright: © 2009 WebSharks, Inc. ( coded in the USA )
<mailto:support@websharks-inc.com> <http://www.websharks-inc.com/>

Released under the terms of the GNU General Public License.
You should have received a copy of the GNU General Public License,
along with this software. In the main directory, see: /licensing/
If not, see: <http://www.gnu.org/licenses/>.
*/
/*
Direct access denial.
*/
if (realpath (__FILE__) === realpath ($_SERVER["SCRIPT_FILENAME"]))
	exit ("Do not access this file directly.");
/*
Function for handling activation routines.
This function should match the array key for this plugin:
ws_plugin__$plugin_key_activate() is called by our themes.
*/
if (!function_exists ("ws_plugin__wp_show_ids_activate"))
	{
		function ws_plugin__wp_show_ids_activate ()
			{
				do_action ("ws_plugin__wp_show_ids_before_activation", get_defined_vars ());
				/**/
				do_action ("ws_plugin__wp_show_ids_after_activation", get_defined_vars ());
				/**/
				return;
			}
	}
/*
Function for handling de-activation cleanup routines.
This function should match the array key for this plugin:
ws_plugin__$plugin_key_deactivate() is called by our themes.
*/
if (!function_exists ("ws_plugin__wp_show_ids_deactivate"))
	{
		function ws_plugin__wp_show_ids_deactivate ()
			{
				do_action ("ws_plugin__wp_show_ids_before_deactivation", get_defined_vars ());
				/**/
				do_action ("ws_plugin__wp_show_ids_after_deactivation", get_defined_vars ());
				/**/
				return;
			}
	}
?>