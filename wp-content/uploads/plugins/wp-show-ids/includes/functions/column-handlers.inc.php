<?php
/*
Copyright: © 2009 WebSharks, Inc. ( coded in the USA )
<mailto:support@websharks-inc.com> <http://www.websharks-inc.com/>

Released under the terms of the GNU General Public License.
You should have received a copy of the GNU General Public License,
along with this software. In the main directory, see: /licensing/
If not, see: <http://www.gnu.org/licenses/>.
*/
/*
Direct access denial.
*/
if (realpath (__FILE__) === realpath ($_SERVER["SCRIPT_FILENAME"]))
	exit ("Do not access this file directly.");
/*
Add the wp_show_ids column.
*/
if (!function_exists ("_ws_plugin__wp_show_ids_return_column"))
	{
		function _ws_plugin__wp_show_ids_return_column ($cols = FALSE)
			{
				return apply_filters ("_ws_plugin__wp_show_ids_return_column", array_merge ($cols, array ("ws_plugin__wp_show_ids" => "ID")), get_defined_vars ());
			}
	}
/*
Return the wp_show_ids column value.
*/
if (!function_exists ("_ws_plugin__wp_show_ids_return_value"))
	{
		function _ws_plugin__wp_show_ids_return_value ($value = FALSE, $column_name = FALSE, $id = FALSE)
			{
				return apply_filters ("_ws_plugin__wp_show_ids_return_value", ( ($column_name === "ws_plugin__wp_show_ids") ? $id : $value), get_defined_vars ());
			}
	}
/*
Echo the wp_show_ids column value.
*/
if (!function_exists ("_ws_plugin__wp_show_ids_echo_value"))
	{
		function _ws_plugin__wp_show_ids_echo_value ($column_name = FALSE, $id = FALSE)
			{
				echo apply_filters ("_ws_plugin__wp_show_ids_echo_value", ( ($column_name === "ws_plugin__wp_show_ids") ? $id : null), get_defined_vars ());
			}
	}
/*
Echo the wp_show_ids column css for width.
*/
if (!function_exists ("_ws_plugin__wp_show_ids_echo_css"))
	{
		function _ws_plugin__wp_show_ids_echo_css ()
			{
				$css = '<style type="text/css">';
				$css .= 'th.column-ws_plugin__wp_show_ids { width: 45px; text-align:center; }';
				$css .= 'td.column-ws_plugin__wp_show_ids { width: 45px; text-align:center; }';
				$css .= '</style>';
				/**/
				echo apply_filters ("_ws_plugin__wp_show_ids_echo_css", $css, get_defined_vars ());
			}
	}
?>