<div class="wrap">
  
  <h2><?php _e('Promotion Slider Options'); ?></h2>
  <p><?php _e('The options below will change the settings for all of the Promotion Sliders running on your website.  For more granular control, please <a href="http://wordpress.org/extend/plugins/promotion-slider/faq/" target="_blank">read our documentation</a>.'); ?></p>
  
  <form method="post" action="options.php">
      
      <?php settings_fields( 'promoslider-settings-group' ); ?>
      <?php $options = get_option('promotion_slider_options'); ?>
      
      <table class="form-table">

        <tr valign="top">
          <th scope="row"><?php _e('Set Slider Time Delay'); ?></th>
          <td>
            <select name="promotion_slider_options[time_delay]">
              <?php for($i=3; $i<=15; $i++): ?>
                <option value="<?php echo $i; ?>"<?php if( $options['time_delay'] == $i ) echo 'selected="selected"'; ?>>
                  <?php echo $i; ?> <?php _e('Seconds'); ?>
                </option>
              <?php endfor; ?>
            </select>
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><?php _e('Automatic Slide Advancement'); ?></th>
          <td>
            <input type="checkbox" name="promotion_slider_options[auto_advance]" value="true" <?php if( $options['auto_advance'] ) echo 'checked="checked"'; ?> /> 
            <?php _e('Check to enable'); ?>
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><?php _e('Start Slider On'); ?></th>
          <td>
            <input type="radio" name="promotion_slider_options[start_on]" value="first" <?php if( $options['start_on'] == 'first' ) echo 'checked="checked"'; ?> />
            <?php _e('First Slide'); ?><br />
            <input type="radio" name="promotion_slider_options[start_on]" value="random" <?php if( $options['start_on'] == 'random' ) echo 'checked="checked"'; ?> />
            <?php _e('Random Slide'); ?><br />
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><?php _e('Promotion Title Display'); ?></th>
          <td>
            <input type="checkbox" name="promotion_slider_options[show_title]" value="true" <?php if( $options['show_title'] ) echo 'checked="checked"'; ?> /> 
            <?php _e('Check to enable'); ?>
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><?php _e('Display Fancy Title'); ?></th>
          <td>
            <input type="checkbox" name="promotion_slider_options[disable_fancy_title]" value="true" <?php if( $options['disable_fancy_title'] ) echo 'checked="checked"'; ?> /> 
            <?php _e('Check to disable'); ?>
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><?php _e('Promotion Excerpt Display'); ?></th>
          <td>
            <input type="checkbox" name="promotion_slider_options[show_excerpt]" value="true" <?php if( $options['show_excerpt'] ) echo 'checked="checked"'; ?> /> 
            <?php _e('Check to enable'); ?>
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><?php _e('Slider Navigation Display'); ?></th>
          <td>
            <input type="radio" name="promotion_slider_options[nav_option]" value="default" <?php if( $options['nav_option'] == 'default' ) echo 'checked="checked"'; ?> />
            <?php _e('Default Slider Navigation'); ?><br />
            <input type="radio" name="promotion_slider_options[nav_option]" value="links" <?php if( $options['nav_option'] == 'links' ) echo 'checked="checked"'; ?> />
            <?php _e('Slider Navigation Links'); ?><br />
            <input type="radio" name="promotion_slider_options[nav_option]" value="thumb" <?php if( $options['nav_option'] == 'thumb' ) echo 'checked="checked"'; ?> />
            <?php _e('Thumbnail Navigation'); ?><br />
            <input type="radio" name="promotion_slider_options[nav_option]" value="none" <?php if( $options['nav_option'] == 'none' ) echo 'checked="checked"'; ?> />
            <?php _e('No Slider Navigation'); ?><br />
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><?php _e('Display Fancy Default Nav'); ?></th>
          <td>
            <input type="checkbox" name="promotion_slider_options[disable_fancy_nav]" value="true" <?php if( $options['disable_fancy_nav'] ) echo 'checked="checked"'; ?> /> 
            <?php _e('Check to disable'); ?>
          </td>
        </tr>
        
      </table>
      
      <p class="submit">
        <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
      </p>
  
  </form>
</div>

<style type="text/css">
  #support{border:1px solid #000; padding:5px 10px 10px 10px; width:500px; margin-top:50px;}
  #support li{list-style-type:disc; margin-left:25px;}
</style>

<div id="support" class="wrap">

  <h3><?php _e('Support This Plugin'); ?></h3>
  <p><?php _e('If you have found this plugin helpful, please consider:');?>
    <ul>
      <li><?php _e('Linking to our site:');?> <a href="http://www.orderofbusiness.net/" target="_blank">http://www.orderofbusiness.net/</a></li>
      <li><?php _e('Making a donation');?></li>
    </ul>
  </p>
  
  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="ZWYWE2TQLXBJY">
    <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
  </form>
  
</div>