<?php
if( class_exists('promo_slider') && !class_exists('ps_legacy_options') ){
	
  class ps_legacy_options extends promo_slider{
	  
	  public function __construct(){
		  $this->change_legacy_post_type();
		  if( !get_option('promotion_slider_options') ){
			$this->transfer_legacy_options();
			update_option( 'promotion_slider_options', $this->options );
			$this->remove_legacy_options();
	  	  }
	  }
	  
	  private function change_legacy_post_type(){
		  global $wpdb; // Namespace the post type rather than keeping the generic name
		  $wpdb->query("UPDATE ".$wpdb->posts." SET post_type = '".$this->post_type."' WHERE post_type = 'promotion'");
	  }
	  
	  private function transfer_legacy_options(){
		  if( get_option('time_delay') ) $this->options['time_delay'] = get_option('time_delay');
		  if( get_option('auto_advance') ) $this->options['auto_advance'] = get_option('auto_advance');
		  if( get_option('show_title') ) $this->options['show_title'] = get_option('show_title');
		  if( get_option('show_excerpt') ) $this->options['show_excerpt'] = get_option('show_excerpt');
		  if( get_option('nav_option') ) $this->options['nav_option'] = get_option('nav_option');
		  if( get_option('disable_fancy_title') ) $this->options['disable_fancy_title'] = get_option('disable_fancy_title');
		  if( get_option('disable_fancy_nav') ) $this->options['disable_fancy_nav'] = get_option('disable_fancy_nav');
	  }
	  
	  private function remove_legacy_options(){
		  delete_option('time_delay');
		  delete_option('auto_advance');
		  delete_option('show_title');
		  delete_option('show_excerpt');
		  delete_option('nav_option');
		  delete_option('disable_fancy_title');
		  delete_option('disable_fancy_nav');
	  }
	  
  }
  
}
?>