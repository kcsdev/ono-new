<?php 

if ( !class_exists('wordpress_custom_taxonomy') ) {
	
  class wordpress_custom_taxonomy{
  
	  private $taxonomy, $object_type, $singular, $plural, $slug;
	  
	  function __construct( $taxonomy, $object_type, $singular = NULL, $plural = NULL, $slug = NULL ){
		  // Set class properties
		  $this->taxonomy = $taxonomy;
		  $this->object_type = $object_type;
		  $this->singular = ( $singular ) ? $singular : ucfirst($this->taxonomy) ;
		  $this->plural = ( $plural ) ? $plural : $this->singular.'s';
		  $this->slug = ( $slug ) ? $slug : strtolower($this->plural);
		  
		  // Register post type if it doesn't already exist
		  if( !taxonomy_exists($this->taxonomy) ) 
			add_action( 'init', array(&$this, 'register_taxonomy') );
	  }
	  
	  function register_taxonomy(){
		  // Create array of arguments for post type
		  $args = array('labels' => array('name' => $this->plural,
										  'singular_name' => $this->singular,
										  'search_items' => 'Search '.$this->plural,
										  'popular_items' => 'Popular '.$this->plural,
										  'all_items' => 'All '.$this->plural,
										  'parent_item' => 'Parent '.$this->singular,
										  'parent_item_colon' => 'Parent '.$this->singular.':',
										  'edit_item' => 'Edit '.$this->singular,
										  'update_item' => 'Update '.$this->singular,
										  'add_new_item' => 'Add New '.$this->singular,
										  'new_item_name' => 'New '.$this->singular.' Name',
										  'separate_items_with_commas' => 'Separate '.$this->plural.' with commas',
										  'add_or_remove_items' => 'Add or remove '.$this->plural,
										  'choose_from_most_used' => 'Choose from the most used '.$this->plural));
		  
		  // Allow editing of default arguments using the filter: wp_{taxonomy}_args
		  $args = apply_filters('wp_'.$this->taxonomy.'_tax_args', $args);
		  
		  // Register the post type
		  register_taxonomy($this->taxonomy, $this->object_type, $args);
	  }

  }
  
}
?>