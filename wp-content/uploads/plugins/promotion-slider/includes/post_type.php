<?php 

if ( !class_exists('wordpress_custom_post_type') ) {
	
  class wordpress_custom_post_type{
  
	  private $post_type, $singular, $plural, $slug;
	  
	  function __construct( $post_type, $singular = NULL, $plural = NULL, $slug = NULL ){
		  // Set class properties
		  $this->post_type = $post_type;
		  $this->singular = ( $singular ) ? $singular : ucfirst($this->post_type) ;
		  $this->plural = ( $plural ) ? $plural : $this->singular.'s';
		  $this->slug = ( $slug ) ? $slug : strtolower($this->plural);
		  
		  // Register post type if it doesn't already exist
		  if( !post_type_exists($this->post_type) ) 
			add_action( 'init', array(&$this, 'register_post_type') );
				  
		  // Add body classes for this post type
		  add_filter( 'body_class', array($this, 'body_class') );
		  
		  // Change how templates are pulled for this post type
		  add_filter( 'template_include', array($this, 'template_include') );
		  add_action( 'template_redirect', array($this, 'context_fixer') );
		  
		  // Add rewrite rules
		  add_filter('generate_rewrite_rules', array(&$this, 'add_rewrite_rules'));
	  }
	  
	  function register_post_type(){
		  // Create array of arguments for post type
		  $args = array('labels' => array('name' => $this->plural,
										  'singular_name' => $this->singular,
										  'add_new' => 'Add New '.$this->singular,
										  'add_new_item' => 'Add New '.$this->singular,
										  'edit_item' => 'Edit '.$this->singular,
										  'new_item' => 'New '.$this->singular,
										  'view_item' => 'View '.$this->singular,
										  'search_items' => 'Search '.$this->plural,
										  'not_found' => 'No '.$this->plural.' found',
										  'not_found_in_trash' => 'No '.$this->plural.' found in Trash'),
						'public' => true,
						'rewrite' => array('slug' => $this->slug));
		  
		  // Allow editing of default arguments using the filter: wp_{post_type}_args
		  $args = apply_filters('wp_'.$this->post_type.'_args', $args);
		  
		  // Register the post type
		  register_post_type($this->post_type, $args);
	  }
	  
	  function body_class( $classes ){
			// If our post type is being called, add our classes
			if ( get_query_var('post_type') === $this->post_type ) {
				$classes[] = $this->post_type;
				$classes[] = 'type-' . $this->post_type;
			}
			return $classes;
	  }
	  
	  function template_include( $template ) {
		  // If our post type is being called, customize how the templates are pulled
		  if ( get_query_var('post_type') == $this->post_type ) {
			// PAGE
			if( is_page() ){
			  $page = locate_template( array($this->post_type.'/page.php', 'page-'.$this->post_type.'.php'));
			  if ( $page ) return $page;
			}
			// SINGLE
			elseif( is_single() ){
			  $single = locate_template( array($this->post_type.'/single.php') );
			  if ( $single ) return $single;
			}
			// LOOP
			else{
			  return locate_template( array($this->post_type.'/index.php', $this->post_type.'.php'));
			}
		  }
		  return $template;
	  }
	  
	  function context_fixer() {
		  if ( get_query_var( 'post_type' ) == $this->post_type ) {
			  global $wp_query;
			  $wp_query->is_home = false;
		  }
	  }
	  
	  function add_rewrite_rules( $wp_rewrite ) {
		  $new_rules = array();
		  $new_rules[$this->slug . '/page/?([0-9]{1,})/?$'] = 'index.php?post_type=' . $this->post_type . '&paged=' . $wp_rewrite->preg_index(1);
		  $new_rules[$this->slug . '/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?post_type=' . $this->post_type . '&feed=' . $wp_rewrite->preg_index(1);
		  $new_rules[$this->slug . '/?$'] = 'index.php?post_type=' . $this->post_type;
		  $wp_rewrite->rules = array_merge($new_rules, $wp_rewrite->rules);
		  return $wp_rewrite;
	  }

  }
  
}
?>