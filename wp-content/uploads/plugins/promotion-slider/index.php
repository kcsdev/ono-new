<?php
/*
Plugin Name: Promotion Slider
Plugin URI: http://www.orderofbusiness.net/plugins/promotion-slider/
Description: This plugin creates a special post type called 'Promotions' that it uses to populate the carousel.  Just use the shortcode [promoslider] to display the slider on your site.  Be sure to check the <a href="http://wordpress.org/extend/plugins/promotion-slider/faq/" target="_blank">user guide</a> for more information on what this plugin can really do!
Version: 3.2.2
Author: Micah Wood
Author URI: http://www.orderofbusiness.net/micah-wood/
License: GPL2

Copyright 2010  Micah Wood  (email : micah.wood@orderofbusiness.net)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


// DEFINE CONSTANTS
if( !defined('PROMOSLIDER_VER') ) define( 'PROMOSLIDER_VER', '3.2.2' );

// INCLUDE FILES
include( dirname(__FILE__).'/includes/post_type.php' );
include( dirname(__FILE__).'/includes/register_tax.php' );
include( dirname(__FILE__).'/includes/functions.php' );

// BEGIN MAIN CLASS
if( !class_exists('promo_slider') ){
	
  class promo_slider{
	  
	  // Define post type and taxonomy name
	  private $post_type = 'ps_promotion',
			  $taxonomy = 'promotion-categories',
			  $options;
	  
	  function __construct(){
		  
		  // Set plugin option defaults
		  $this->options = array('version' => PROMOSLIDER_VER,
								 'time_delay' => 6,
								 'auto_advance' => true,
								 'show_title' => false,
								 'show_excerpt' => false,
								 'nav_option' => 'default',
								 'disable_fancy_title' => false,
								 'disable_fancy_nav' => false,
								 'start_on' => 'random');
		  
		  // Create the post type that we will pull from
		  add_filter('wp_'.$this->post_type.'_args', array(&$this, 'post_type_args'));
		  new wordpress_custom_post_type($this->post_type, 'Promotion', 'Promotions', 'promotion' );
		  
		  // Register the taxonomy for our post type
		  add_filter('wp_'.$this->taxonomy.'_tax_args', array(&$this, 'tax_args'));
		  new wordpress_custom_taxonomy($this->taxonomy, $this->post_type, 'Category', 'Categories', 'promotions');

		  // Perform some maintenance activities on activation
		  register_activation_hook( __FILE__, array(&$this, 'activate') );
		  
		  // Initiate key components
		  add_action( 'after_setup_theme', array(&$this, 'after_setup_theme') );
		  add_action( 'init', array(&$this, 'init') );
		  add_action( 'admin_init', array(&$this, 'admin_init') );
		  add_action( 'admin_menu', array(&$this, 'admin_menu') );
		  
		  // Load this plugin last to ensure other plugins don't overwrite theme support
		  add_action( 'activated_plugin', array(&$this, 'load_last') );

	  }
	  
	  function post_type_args( $args ){ $args['supports'] = array('title', 'editor', 'thumbnail'); return $args; }

	  function tax_args( $args ){ $args['hierarchical'] = true; return $args; }
	  
	  function activate() {
		include( dirname(__FILE__).'/includes/legacy_options.php' ); 	// Load class file
		new ps_legacy_options(); 										// Update legacy options, if needed
		$this->option_management();										// Manage plugin options
		flush_rewrite_rules();											// One time flush of rewrite rules
	  }
	  
	  function option_management(){
		  $options = get_option( 'promotion_slider_options' );
		  if( $options ){ // Update version on activation
			$options['version'] = $this->options['version'];
			update_option( 'promotion_slider_options', $options );
		  }
	  }
	  
	  function after_setup_theme(){ add_theme_support( 'post-thumbnails' ); }
	  
	  function init(){
		  // Register and load our js and css files if we are on the front end of the site
		  if( !is_admin() ) {
			  wp_enqueue_script( 'promoslider_main', plugins_url('/js/promo_slider.js', __FILE__), array('jquery'), FALSE, TRUE );
			  wp_enqueue_style( 'promoslider_main', plugins_url('/css/slide.css', __FILE__) );
		  }
		  // Localize plugin options
		  $options = get_option('promotion_slider_options');
		  $data = array('timeDelay' => $options['time_delay'], 
						'autoAdvance' => $options['auto_advance'], 
						'startOn' => $options['start_on'],
						'nav_option' => $options['nav_option'],
						'version' => $this->options['version']);
		  wp_localize_script('promoslider_main', 'promo_slider_options', $data);
		  // Create [promoslider] shortcode
		  add_shortcode( 'promoslider', array(&$this, 'show_slider') );
		  // Enable use of the shortcode in text widgets
		  add_filter('widget_text', 'do_shortcode');
	  }
	  
	  function admin_init(){ 
	  	// Register plugin options
	  	register_setting( 'promoslider-settings-group', 'promotion_slider_options', array(&$this, 'update_settings') ); 
		// Add meta box to our post type
		add_meta_box( 'promo_slider_meta_box', __('Promotion Slider Options'), array(&$this, 'meta_box_content'), $this->post_type );
		// Save meta data when saving our post type
		add_action( 'save_post', array(&$this, 'save_meta_data') );
	  }
	  
	  function admin_menu(){
		// Create options page
		add_submenu_page('edit.php?post_type='.$this->post_type, __('Promotion Slider Options'), __('Slider Options'), 'manage_options', 'options', array(&$this, 'options_page') );
	  }
	  
	  function load_last(){
		  // Get array of active plugins
		  if( !$active_plugins = get_option('active_plugins') ) return;
		  // Set this plugin as variable
		  $my_plugin = 'promotion-slider/'.basename(__FILE__);
		  // See if my plugin is in the array
		  $key = array_search( $my_plugin, $active_plugins );
		  // If my plugin was found
		  if( $key !== FALSE ){
			// Remove it from the array
			unset( $active_plugins[$key] );
			// Reset keys in the array
			$active_plugins = array_values( $active_plugins );
			// Add my plugin to the end
			array_push( $active_plugins, $my_plugin );
			// Resave the array of active plugins
			update_option( 'active_plugins', $active_plugins );
		  }
	  }
	  
	  function update_settings( $settings ){ // Validate plugin options
		  foreach($this->options as $setting => $default){
			  if( !$settings[$setting] ) $new_settings[$setting] = FALSE;
			  else $new_settings[$setting] = $settings[$setting];
		  }
		  $new_settings['version'] = $this->options['version'];
		  return $new_settings;
	  }
	  
	  function options_page(){ include( dirname(__FILE__).'/includes/options_page.php' ); }
	  
	  function meta_box_content() { include( dirname(__FILE__).'/includes/meta_box.php' ); }
	  
	  function save_meta_data( $post_id ) {
		  // If this is an auto save, our form has not been submitted, so we don't want to do anything
		  if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
		  // Is the post type correct?
		  if ( isset($_POST['post_type']) && $this->post_type != $_POST['post_type'] ) return $post_id;
		  // Verify this came from our screen and with proper authorization, because save_post can be triggered at other times
		  if ( !isset($_POST['promo_slider_noncename']) || !wp_verify_nonce( $_POST['promo_slider_noncename'], 'ps_update_promotion' )) { return $post_id; }
		  // Can user edit this post?
		  if ( !current_user_can('edit_post', $post_id) ) return $post_id;
		  // Setup array for the data we are saving	
		  $data = array('_promo_slider_target', '_promo_slider_url', '_promo_slider_disable_links', '_promo_slider_show_ad_code', '_promo_slider_ad_code');
		  // Use this filter to add postmeta to save for the default post type
		  $data = apply_filters('promoslider_add_meta_to_save', $data);
		  // Save meta data
		  foreach($data as $meta){
			  // Get current meta
			  $current = get_post_meta($post_id, $meta, TRUE);
			  // Get new meta
			  $new = $_POST[$meta];
			  // If the new meta is empty, delete the current meta
			  if( empty($new) ) delete_post_meta($post_id, $meta);
			  // Otherwise, update the current meta
			  else update_post_meta($post_id, $meta, $new);
		  }
	  }
	  
	  function show_slider( $atts ){
		// Extract shortcode attributes and set defaults
		extract( shortcode_atts(array('id' => NULL, 
									  'post_type' => $this->post_type, 
									  'category' => NULL, 
									  'width' => NULL, 
									  'height' => NULL, 
									  'time_delay' => NULL, 
									  'auto_advance' => NULL,
									  'numberposts' => -1,
									  'start_on' => NULL), $atts) );
		
		// Get plugin options
		$options = get_option('promotion_slider_options');
		
		// Make $post global and save current value so we can restore it later
		global $post; 
		$tmp_post = $post;
		
		// Create an array with default values that we can use to build our query
		$query = array('numberposts' => $numberposts, 'post_type' => $this->post_type);
		
		// Set the post type that we will be looking for.  If post type is invalid, use the default
		if( $post_type != $this->post_type && post_type_exists($post_type) ) $query['post_type'] = $post_type;
		
		// Check to see if the category is set and the term exists.  If it exists and the post type is post or the default, set the category based on taxonomy.
		if( !empty($category) && term_exists($category) ){
		  if( $query['post_type'] == $this->post_type ) $query[$this->taxonomy] = $category;
		  elseif( $query['post_type'] == 'post' ) $query['category_name'] = $category;
		}
		
		// Use the promoslider_query filter to customize the results returned.
		$query = apply_filters('promoslider_query', $query);
		
		// Use the promoslider_query_by_id filter to customize a query for a particular slider.
		$query_by_id = apply_filters('promoslider_query_by_id', array('query' => $query, 'id' => $id));
		$query = $query_by_id['query'];
		
		// Run query and get posts
		$promo_posts = get_posts( $query );
		
		// If there are results, build slider.  Otherwise, don't show anything.
		if($promo_posts): $i = 1; 
		
		  // Set width and height of slider if provided
		  $width = ($width) ? 'width:'.$width.';' : '';
		  $height = ($height) ? 'height:'.$height.';' : '';
		  $style = ($width || $height) ? ' style="'.$width.' '.$height.'"' : FALSE;
		  
		  // Validate time delay
		  $time_delay = (int) $time_delay;
		  if( !is_int($time_delay) || $time_delay < 3 || $time_delay > 15 ) $time_delay = NULL;
		  
		  // Validate auto advance
		  if($auto_advance !== NULL){
			  $auto_advance = (string) strtolower($auto_advance);
			  if($auto_advance == 'true') $auto_advance = ' auto_advance';
			  elseif($auto_advance == 'false') $auto_advance = ' no_auto_advance';
			  else $auto_advance = NULL;
		  }
		  
		  // Validate navigation option
		  $nav_option = $options['nav_option'];
		  $nav = ($nav_option == 'none') ? NULL : ' '.$nav_option.'_nav';
		  
		  // Valdate 'start on' option
		  if( $start_on ){
			  $start_on = ( $start_on == 'random' ) ? 'random' : 'first';
		  }
		  
		  // Setup thumbnail array
		  $thumb_collection = array();
		  
		  // Begin Output
		  ob_start(); ?>
  
			<div class="promo_slider_wrapper<?php if($nav) echo $nav; if($start_on) echo ' '.$start_on; ?>">
			  <?php do_action('before_promoslider', $id); ?>
              
              <div <?php if(isset($id)) echo 'id="'.$id.'" '; ?>class="promo_slider<?php echo $auto_advance; ?>"<?php if( $style ) echo $style; ?>>
              <?php if($time_delay) echo '<span class="promo_slider_time_delay" style="display:none;">'.$time_delay.'</span>'; ?>
                
                <?php foreach($promo_posts as $post): setup_postdata($post);
                  // Setup values to be passed to the promoslide_content action hook
                    // Get the title
                    $title = get_the_title(); 
                    // Get the excerpt
                    $excerpt = get_the_excerpt();
                    // Get the thumbnail, if any
                    if( function_exists('has_post_thumbnail') && has_post_thumbnail($post->ID) ){
					  $image = get_the_post_thumbnail($post->ID, 'full');
					  $thumb = get_the_post_thumbnail($post->ID, 'post-thumbnail');
					  $thumb = preg_replace('/width="[^"]*"/', 'width="50"', $thumb);
					  $thumb = preg_replace('/height="[^"]*"/', 'height="50"', $thumb);
					  $thumb = preg_replace('/title="[^"]*"/', 'title="'.$title.'"', $thumb);
					} else{
					  $image = NULL;
					  $thumb = NULL;
					}
					$thumb_collection[] = $thumb;
                    // If the destination url is set by the user, use that.  Otherwise, use the permalink
                    if( !$dest_url = get_post_meta($post->ID, '_promo_slider_url', TRUE) )$dest_url = get_permalink($post->ID);
                    // If the target attribute is set the user, use that.  Otherwise, set it to _self
                    if( !$target = get_post_meta($post->ID, '_promo_slider_target', TRUE) )$target = '_self';
                    // Setup the disable links variable
                    if( !$disable_links = get_post_meta($post->ID, '_promo_slider_disable_links', TRUE) ) $disable_links = FALSE;
                    // Store all the values in an array and pass it to the promoslider_content action
                    $values = array('title' => $title, 
                                    'excerpt' => $excerpt, 
                                    'thumb' => $image,
                                    'destination_url' => $dest_url, 
                                    'target' => $target, 
                                    'id' => $id, 
                                    'disable_links' => $disable_links); ?>
                              
                  <div class="panel panel-<?php echo $i; ?>">
                    <span class="panel-number" style="display:none;"><?php echo $i; ?></span>
                    <span class="panel-title" style="display:none;"><?php echo $title; ?></span>
                    <?php // Use the promoslider_content action to populate the contents for panel
                    do_action('promoslider_content', $values); ?>
                  </div>
                    
                  <?php $i++; ?>
                <?php endforeach; 
                
                // Restore original value of $post global variable for later use by WordPress
                $post = $tmp_post; ?>
                    
                <?php if($i > 2): ?>
                  <?php // Use promoslider_nav action to generate the nav options
                  do_action('promoslider_nav', $id); ?>
                <?php endif; ?>
                  
                <div class="clear"></div>
                  
              </div>
              <?php do_action('promoslider_thumbnail_nav', array('id' => $id, 'title' => $title, 'thumbs' => $thumb_collection, 'width' => $width) ); ?>
              <?php do_action('after_promoslider', $id);?>
            </div><?php
		  
		  // End Output
		  $slider = ob_get_contents();
		  ob_end_clean();
		  
		  return $slider;
		
		endif;
	  }

  }
  
}

global $promo_slider;
$promo_slider = new promo_slider();

?>