=== Plugin Name ===
Contributors: marquex
Donate link: http://marquex.posterous.com/pages/custom-sidebars
Tags: custom sidebars, widgets, sidebars, custom, sidebar, widget, personalize
Requires at least: 3.0
Tested up to: 3.01
Stable tag: trunk

Allows to create your own widgetized areas and custom sidebars, and select what sidebars to use for each post or page.

== Description ==

Sometimes it is necessary to show different elements on the sidebars for some posts or pages. The themes nowadays give you some areas to put the widgets, but those areas are common for all the posts that are using the same template. NOTE: **You need to use a theme that accepts widgets to make this plugin work** 

Custom Sidebars allows you to create all the widgetized areas you need, your own custom sidebars, configure them adding widgets, and replace the default ones on the posts or pages you want in just few clicks.

You can set also new default sidebars for all the posts of a custom content type easily, keeping the chance of change them individually.

This way your will boost the content manager facet of your Wordpress installation.  

Translations are welcome to the custom sidebars plugin! I will write your name down here if you donate your translation work. Thanks very much to:

*	marquex - English
*	marquex - Spanish

== Installation ==

There are two ways of installing the plugin:

**From the [WordPress plugins page](http://wordpress.org/extend/plugins/)**

1. Download the plugin
2. Upload the `custom-sidebars` folder to your `/wp-content/plugins/` directory.
3. Active the plugin in the plugin menu panel in your administration area.

**From inside your WordPress installation, in the plugin section.**

1. Search for custom sidebars plugin
2. Download it and then active it.

Once, you have the plugin activated, you will find a new option called 'Custom Sidebars' in your Appearance menu. There you will be able to create and manage your own sidebars.

You can find some simple tutorials on the [Custom sidebars plugin web page](http://marquex.posterous.com/pages/custom-sidebars)

== Frequently Asked Questions ==

= Why there are no asked questions in this section? =

Nobody has asked anyting yet. I will fill this section with real questions.

= Some howtos =

You can find some simple tutorials on the [Custom sidebars plugin web page](http://marquex.posterous.com/pages/custom-sidebars)


== Screenshots ==

1. screenshot-1.png The Custom Sidebars options page. Placed in the appearance menu, you can create or delete sidebars there, set the replaceable sidebars and assign custom sidebars for all the posts of a certain post-type. 
2. screenshot-2.png The new sidebars created by the plugin, can be customized in the Widgets menu.
3. screenshot-3.png A new box is added to the post and page edit forms, where you can set your custom sidebars up.

== Changelog ==

= 0.5 =

*	Fixed a bug that didn't allow to create new bars when every previous bars were deleted.
*	Fixed a bug introduced in v0.4 that did not allow to assign bars per post-types properly
*	Added an option to remove all the Custom Sidebars data from the database easily.

= 0.4 =

*	Empty sidebars will now be shown as empty, instead of displaying the theme's default sidebar.

= 0.3 =

*	PHP 4 Compatible (Thanks to Kay Larmer)
*	Fixed a bug introduced in v0.2 that did not allow to save the replaceable bars options

= 0.2 =

*	Improved security by adding wp_nonces to the forms.
*	Added the pt-widget post type to the ignored post types.
*	Improved i18n files.
*	Fixed screenshots for documentation.

= 0.1 =

*	Initial release


