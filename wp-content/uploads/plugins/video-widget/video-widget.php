<?php
/*
Plugin Name: Video Widget
Plugin URI: http://kcsnet.net
Description: A brief description of the Plugin.
Version: The Plugin's Version Number, e.g.: 1.0
Author: KCS
Author URI: http://URI_Of_The_Plugin_Author
*/

error_reporting(E_ALL);
add_action("widgets_init", array('video_widget', 'register'));
class video_widget {
  function control(){
    echo '';
  }
  function widget($args){
    global $post;
    if (get_post_meta($post->ID, 'video_link', true) !="") {
      echo "<a href='".get_post_meta($post->ID, 'video_link', true)."' title='Image Title' rel='shadowbox;player=swf;width=640;height=385;' alt='' >
        <div class='homevideo'>
                
                <p>".get_post_meta($post->ID, 'video_description', true)."</p>
                
        </div></a>";
    }
    //echo $args['before_widget'];
    //echo $args['before_title'] . 'Your widget title' . $args['after_title'];
    //echo 'I am your widget';
    //echo $args['after_widget'];
  }
  function register(){
    register_sidebar_widget('Video Widget', array('video_widget', 'widget'));
    register_widget_control('Video Widget', array('video_widget', 'control'));
  }
}



?>