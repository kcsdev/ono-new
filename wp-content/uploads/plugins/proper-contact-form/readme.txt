=== Proper Contact Form ===
Contributors: properwp
Donate link: 
Tags: contact, contact form
Requires at least: 3.0
Tested up to: 3.5
Stable tag: 0.9.3

Creates a flexible, secure contact form on your WP site

== Description ==

A well-coded, secure, and flexible WordPress plugin that makes creating contact (and other) forms very simple. This is meant to be a simple tool for both savvy WordPress users and seasoned WordPress developers. 

At the moment, this plugins creates a contact form with the shortcode `[proper_contact_form]` that works on any page. Users have the option to:

- Choose the fields that appear
- Create an auto-respond email
- Redirect contact submissions to a new page (helpful for goal tracking)
- Store contacts in the admin
- Over-ride label names

The current, basic functionality will not change but, in a future version, WP users will be able to add custom forms with validation quickly and easily. 

Get the absolute latest at the [Github repo](https://github.com/joshcanhelp/proper-contact-form).

== Screenshots ==

1. The contact form, with styles, on a page
2. First section of the settings page
3. Second section of the settings page
4: Fourth section of the settings page

== Installation ==

Activating the Proper Contact Form plugin is just like any other plugin. If you've uploaded the plugin package to your server already, skip to step 5 below:

1. In your WordPress admin, go to **Plugins > Add New**
2. In the Search field type "proper contact form"
3. Under "PROPER Contact Form," click the **Install Now** link
4. Once the process is complete, click the **Activate Plugin** link
5. Now, you're able to add contact forms but, first, we could configure a few settings. These can be found at **Settings > Proper Contact**
6. Make the changes desired, then click the **Save changes** button at the bottom
7. To add this form to any page or post, just copy/paste or type "[proper_contact_form]" into the page or post content and save. The form should appear on that page

== Changelog ==

= 0.9.3 =
* Fixed name requirement issue

= 0.9.2 =
* Fixed default handling
* Fixed custom label handling
* Changed email sent to admin slightly
* Added option for name

= 0.9.1 =
* Fixed wp_kses error
* Corrected outgoing email

= 0.9 =
* First release
e
