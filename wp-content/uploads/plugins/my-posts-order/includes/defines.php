<?php
  $path = WP_PLUGIN_URL . '/' . 'my-posts-order';
  define('PLUGIN_PATH', $path);
  define('STATIC_PATH', PLUGIN_PATH . '/static/');
  define('MPO_JS_PATH', STATIC_PATH . 'js/');
  define('MPO_CSS_PATH', STATIC_PATH . 'css/');
  define('MPO_IMAGES_PATH', STATIC_PATH . 'images/');
  define('AJAX_SEPARATOR', '~#$');

	global $selection_criteria, $global_section_array;
  $selection_criteria = array('specific_content' => 'Specific Posts', 'category_radio' => 'Top Category Posts', 'xml_feed' => 'XML/RSS Feed');

?>