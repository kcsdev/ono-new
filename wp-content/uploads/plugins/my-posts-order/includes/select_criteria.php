  <div id="selection_criteria" style="float:left;" >
    <div id="popupmain" style="float:left;">
      <p class="req_head"><?php echo 'Choose Your Criteria';?></p>
			<div class="formfield">
				<p class="row1">
					<label><?php echo 'Select option:'; ?></label>
					<em> <?php
						$main_criteria = array('add_section' => 'Add New Section', 'edit_section' => 'Edit / Delete Section');
						display_radio_buttons($main_criteria, 'main_criteria'); ?>
					</em>
				</p>
				<div id="add_edit_section_row"></div>
			</div>
    </div>
  </div>