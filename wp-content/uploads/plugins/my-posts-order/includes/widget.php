<?php
	add_action( 'widgets_init', 'my_posts_order_widget' );

  function my_posts_order_widget() {
    register_widget( 'My_Posts_Order' );
  }

  class My_Posts_Order extends WP_Widget {

    function My_Posts_Order() {
      $widget_ops = array('classname' => 'my_posts_order', 'description' => "It displays posts in any order." );

      $this->WP_Widget('featured-posts', 'My Posts Order', $widget_ops);
      $this->alt_option_name = 'my_posts_order';
    }

    function widget($args, $instance) {
      extract($args);
      $title = apply_filters('widget_title', empty($instance['title']) ? __('My Posts Order') : $instance['title'], $instance, $this->id_base);//print_r($instance);
      $section_identifier = $instance['section_name'];
      if (isset($section_identifier) ) {
				$sec_obj = new Section;
				$sec_obj->section_identifier = $section_identifier;
				$sec_obj->status = 1;
				$section_data = $sec_obj->get_section_data();

				$menu_options = isset($section_data->section_meta_key) ?  $section_data->section_meta_key : '';
				switch($menu_options) {
					case 'specific_content' :
						global $post_ids;
						$post_ids = $section_data->section_meta_value;
						$post_ids_array = explode(',', $section_data->section_meta_value);
					//	$post_ids_array = array_slice($post_ids_array, 0, 9);//loop must not be greater than 10
						$length = count($post_ids_array);
						if (is_array($post_ids_array)) {
							add_filter('posts_orderby', 'specific_content_orderby' );
							$args = array(
												'orderby' => 'none',
												'post__in'  => $post_ids_array,
												'posts_per_page' => $length,
												'ignore_sticky_posts' => 1
											);
							$name = $section_data->section_name;
							include('loop.php'); //We are not including it require_once because we need different output each time
							remove_filter('posts_orderby', 'specific_content_orderby' );
						}
					break;
					case 'category_radio' :
						if ($section_data->length > 10) {
							$length = 10;
						} else {
							$length = $section_data->length;
						}
						$args = array(
								'cat' => $section_data->section_meta_value,
								'orderby'=>'ID',
								'order' => 'DESC',
								'posts_per_page' => $length,
								'ignore_sticky_posts' => 1
						);
						$name = get_cat_name( $section_data->section_meta_value );
						$link = get_category_link( $section_data->section_meta_value );
						include ('loop.php');
					break;
					case 'xml_feed' :
						if ($section_data->length > 10) {
							$length = 10;
						} else {
							$length = $section_data->length;
						}

						include_once(ABSPATH . WPINC . '/feed.php');
						$res_feed_url = $section_data->section_meta_value;
						$rss = fetch_feed($res_feed_url);// Get a SimplePie feed object from the specified feed source.
						if (!is_wp_error( $rss ) ) { // Checks that the object is created correctly
							$maxitems = $rss->get_item_quantity($length);// Figure out how many total items there are, but limit it to 5.
							$rss_items = $rss->get_items(0, $maxitems);// Build an array of all the items, starting with element 0 (first element).
							include ('loop.php');
						} else {
							echo 'Could not get any feed for this url';
							return;
						}
					break;
					default:
						return '';
				}
      }
    }

    function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $instance['title'] = strip_tags($new_instance['title']);
      $instance['section_name'] = $new_instance['section_name'];
      return $instance;
    }

    function form( $instance ) {
      $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
      $section_name = isset($instance['section_name']) ? $instance['section_name'] : '';?>
      <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
      <p>
				<label for="<?php echo $this->get_field_id('section_name'); ?>"><?php echo 'Select Section Name'; ?></label><?php
				$sec_obj = new Section;
				$all_sections = $sec_obj->get_all_sections();
				if (count($all_sections) > 0 ) { ?>
					<select id="<?php echo $this->get_field_id('section_name'); ?>" name="<?php echo $this->get_field_name('section_name'); ?>">
						<option value="">Choose your option</option><?php
						foreach ($all_sections as $_all_section) { ?>
							<option value="<?php echo $_all_section->section_identifier; ?>" <?php if($_all_section->section_identifier == $section_name) { echo ' selected = "selected" ';} ?>><?php echo $_all_section->section_name; ?></option> <?php
						} ?>
					</select> <?php
				} else {
					echo 'No Section Found';
				} ?>
      </p> <?php
    }
  }