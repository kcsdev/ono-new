	jQuery("#add_section").click(function() {
		get_loading_image('add_edit_section_row');
		jQuery.ajax({
			type: "post",url:"admin-ajax.php", data: { action: 'add_edit_section' },
			success: function(response){
				jQuery('#add_edit_section_row').html(response);
				allEvents(true);//function to load drag-drop functionality after the ajax response
			}
		});
	});

	jQuery("#edit_section").click(function() {
		get_loading_image('add_edit_section_row');
		jQuery.ajax({
			type: "post",url:"admin-ajax.php", data: { action: 'edit_section' },
			success: function(response){
				jQuery('#add_edit_section_row').html(response);
			}
		});
	});

	jQuery(".content_type").live('click', function() {
		var sel_val = jQuery(this).val();
		var obj = { specific_content: "content_desc", category_radio: "category_desc", xml_feed: "xml_feed_desc"};
		jQuery.each(obj, function(key, value) {
			if (key == sel_val) {
				jQuery('#' + value).show();
			} else {
				jQuery('#' + value).hide();
			}
		});
	});



	function validate_form() {
		var main_criteria = jQuery("input[name=main_criteria]:checked").val();
		var section_identifier;
		if (typeof(main_criteria) == 'undefined') {
			alert ('Please select your main criteria');
			return false;
		} else if (main_criteria == 'edit_section') {
			var section_identifier = jQuery('#all_sections').val();
			if (section_identifier == '') {
				alert('Please select a section name');
				return false;
			}
		}
		if (jQuery('#specific_content').is(':checked') == true) {
			if (fetch_selected_ids()) {
				var post_ids = jQuery('#selected_entries').val();
				var section_name = jQuery('#section_name').val();
			} else {
				return false;
			}
		} else if (jQuery('#category_radio').is(':checked') == true) {
			if (check_textfield('categories_list', 'a category')) {
				var length = jQuery('#no_posts_category').val();
				var category_id = jQuery('#categories_list').val();
			} else {
				return false;
			}
		} else if (jQuery('#xml_feed').is(':checked') == true) {
			if (check_textfield('xml_feed_url', 'url of xml feed'))  {
				if (checkUrl('xml_feed_url') ) {
					var feed_url = jQuery('#xml_feed_url').val();
					var length = jQuery('#no_posts_xml_feed').val();
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			alert('Please select content type');
			return false;
		}
		if (check_textfield('section_name', 'Section name') ) {
			var section_name = jQuery('#section_name').val();
			var illegalChars =/^[A-Z0-9 _]*$/; // allow only letters and numbers
			if (illegalChars.test(section_name)) {
				alert('Only letters or numbers are allowed');
				return false;
			}
		} else {
			return false;
		}
		var radio_val = jQuery("input[name=content_type]:checked").val();
		var nonce_field = jQuery('#nonce_field').val();
		get_loading_image('add_edit_section_row');
		jQuery.ajax({
			type: "post",url:"admin-ajax.php", data: { action: 'save_section_data', content_type: radio_val,
				category_id: category_id, post_ids:post_ids, length:length, feed_url:feed_url,
				_ajax_nonce: nonce_field, section_name: section_name, section_identifier: section_identifier },
			success: function(html){ //so, if data is retrieved, store it in html
				var res = html.split('~#$');
				jQuery('#add_edit_section_row').hide();
				alert(res[0]);
				window.location.href = 'options-general.php?page=my-posts-order';
			}
		});
	}

	function confirm_delete() {
	  var confirm_box = confirm('Are you sure you want to Delete');
	  if (confirm_box) {
		 var section_identifier = jQuery('#all_sections').val();
		if (section_identifier == '' || typeof(section_identifier) == 'undefined') {
			alert('Please select a section name');
			return false;
		}
		 get_loading_image('add_edit_section_row');
		 jQuery.ajax({
			type: "post",url:"admin-ajax.php", data: { action: 'delete_section_data', section_identifier: section_identifier },
			success: function(res){ //so, if data is retrieved, store it in html
				if (res) {
					jQuery('#add_edit_section_row').hide();
					alert(res);
					window.location.href = 'options-general.php?page=my-posts-order';
				} else {
				  alert(res);
				}
			}
		});

	  }
	}

	function check_textfield (id, mess) {
		var text = jQuery.trim(jQuery('#' + id).val());
		if (text == '') {
			alert('Please enter ' + mess);
			return false;
		} else {
			return true;
		}
	}

	function edit_posts_section (val) {
		if (val == '')	{
			alert('Please select a valid criteria');
			return false;
		}
		get_loading_image('section_ajax_container');
		jQuery.ajax({
			type: "post",url: "admin-ajax.php",data: { action: 'add_edit_section', section_identifier: val },
			success: function(res){ //so, if data is retrieved, store it in html
				jQuery('#section_ajax_container').html(res);
				allEvents(true);//function to load drag-drop functionality after the ajax response
			}
			});
	}


	function fetch_selected_ids () {
			var entryIdString = '';
			jQuery('#tbody_selections > tr[id^="entry_"] > td[id^="action_entry_"]').each(
				function () {
					var entryId = this.id.split('_');
					entryIdString += entryId[(entryId.length - 1)] + ',';
				}
			);
			if (jQuery.trim(entryIdString) == '') {
				alert('Please make some selections.');
			return false;
		} else {
			jQuery('#selected_entries').val((entryIdString.substring(0, (entryIdString.length - 1))));
			return true;
		}
	}

	function getUrlVars() {
		var vars = [], hash;alert(window.location.href)
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}

	function multiple_sel_box_val() {
		var selected_val_array = [];
		jQuery('#categories_list :selected').each(function(i, selected) {
		selected_val_array[i] = jQuery(selected).val();
		});
	}

	function checkUrl(theUrl){ return true;
		var url = jQuery('#'+ theUrl).val();
		var pattern = '^((ht|f)tp(s?)\:\/\/|~/|/)?([\w]+:\w+@)?([a-zA-Z]{1}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?((/?\w+/)+|/?)(\w+\.[\w]{3,4})?((\?\w+=\w+)?(&\w+=\w+)*)?';
		if(url.match(pattern) ){
			return true;
		} else {
	     alert("Please enter a valid url.");
			return false;
		}
	}


	jQuery('#search_posts_text').live('focus', function() {
		if (jQuery(this).val() == 'Search Posts..' ) {
			jQuery(this).val('');
		}
	});

	//jQuery('#search_posts_text').blur(function() {
		//  jQuery(this).val('Search Posts..');
	//});

	jQuery('#search_posts_button').live('click', function() {
		var search_text = jQuery.trim(jQuery('#search_posts_text').val());
		if (search_text == '' || search_text == 'Search Posts..') {
			alert('Please enter search text');
			return false;
		} else {
			get_loading_image('specific_content_container');
			jQuery.ajax({
			type: "post",url: ajaxurl,data: { action: 'search_posts', search_str: search_text },
			success: function(res){ //so, if data is retrieved, store it in html
				jQuery('#specific_content_container').html(res);
				allEvents(true);//function to load drag-drop functionality after the ajax response
			}
			});
		}
	});


	function get_loading_image(id) {
    jQuery('#' + id).html('<img  style="padding-left:40px;" src="' + MPO_IMAGES_PATH + 'loadingAnimation.gif" />');
  }




jQuery(document).ready(
  function() {
    allEvents(false);
  }
);

function allEvents(unBind) {
  if (unBind) {
    jQuery('#tbody_entries > tr[id^="entry_"] > td[id^="action_entry_"]').unbind('click');
    jQuery('#tbody_selections > tr[id^="entry_"] > td[id^="action_entry_"]').unbind('click');
  }
  jQuery("#table_selections").tableDnD();

  try {
    jQuery('#tbody_entries > tr[id^="entry_"] > td[id^="action_entry_"]').click(
      function () {
        jQuery('#msg_selections').remove();
        jQuery('#no_content').remove();
        jQuery(this).html('Remove');
        jQuery(this).parent().clone().prependTo("#tbody_selections");
        jQuery(this).parent().remove();
        var entriesCount = 0;
        jQuery('#tbody_entries > tr[id^="entry_"]').each(
          function () {
            ++entriesCount
          }
        );
        if (!entriesCount) {
          jQuery('#tbody_entries').html('<tr id="msg_entries"><td>No post found.</td></tr>');
        } else {
          jQuery('#msg_entries').remove();
        }
        allEvents(true);
      }
    );

    jQuery('#tbody_selections > tr[id^="entry_"] > td[id^="action_entry_"]').click(
      function () {
        jQuery('#msg_entries').remove();
        jQuery(this).html('Add');
        jQuery(this).parent().clone().prependTo("#tbody_entries");
        jQuery(this).parent().remove();
        var entriesCount = 0;
        jQuery('#tbody_selections > tr[id^="entry_"]').each(
          function () {
            ++entriesCount
          }
        );
        if (!entriesCount) {
          jQuery('#tbody_selections').append('<tr id="msg_selections"><td>No selections made.</td></tr>');
        } else {
          jQuery('#msg_selections').remove();
        }
        allEvents(true);
      }
    );

    jQuery('td[id^="action_entry_"]').hover(
      function () {
        jQuery(this).css({cursor:"pointer"});
      },
      function () {
        jQuery(this).css({cursor:"auto"});
      }
    );


    if (jQuery.trim(jQuery('#tbody_entries').html()) == '') {
      jQuery('#tbody_entries').html('<tr id="msg_entries"><td>No post found.</td></tr>');
    }

    if (jQuery.trim(jQuery('#tbody_selections').html()) == '') {
      jQuery('#tbody_selections').html('<tr id="msg_selections"><td>No selections made.</td></tr>');
    }


    jQuery('span[id^="code_link_"]').hover(
      function () {
        jQuery(this).css({cursor:"pointer"});
      },
      function () {
        jQuery(this).css({cursor:"auto"});
      }
    );

  } catch (e) {
    alert(e);
  }
}