=== My Posts Order ===
Contributors: kapilchugh
Tags: custom posts order, arrange post order, drag and drop, posts, order, posts order, post, post order, ajax, category posts, top category posts, category post, xml feed, rss, widget
Requires at least: 3.0
Tested up to: 3.3
Stable tag: 1.0.2

A plugin which allows you to sort posts in ANY order.

== Description ==

While WordPress allows you to make your posts sticky, or even sort them in ascending or descending order, sometimes this is just not enough. What if you want to display the posts in ANY order you need? Unfortunately there is no such functionality in WordPress, which is where this plugin saves the day.

This plugin works on sections of posts, which you can define based on the following three criteria:

1. Specific Posts: You can use the drag-and-drop feature to rearrange posts in any order.
2. Top Category Posts : You can select top posts from any category.
3. XML/RSS Feed : Here you just need to give the URL of XML Feed and it will fetch latest posts.

**Once the sections are created, you can display these posts using widgets.**

Now wasn't that easy!

== Installation ==

1. Upload the `my-posts-order` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Go to the Settngs -> My Posts Order Options.
4. Create new Sections.
5. Go to the Appearance -> Widgets.
6. Drag and Dop 'My Posts Order' Widget in the Widget Area.
7. Select the section name.
8. In the same way you can add multiple widgets.



== Frequently Asked Questions ==
Please send us your questions to kapil.chugh@hotmail.com.

== Screenshots ==
1. Edit Section based on Specific Posts criteria.
2. Edit Section based on Top Category Posts criteria.
3. Edit Section based on XML/RSS feed criteria.
4. Select Widget
5. Widget Display.

== Changelog ==

= 1.0.1 =
* Removed Notices and Warnings.

= 1.0 =
* Initial release

== Upgrade Notice ==

= 1.0.1 =
This version fixes some bugs . So Upgrade immediately.
