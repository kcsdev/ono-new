<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */

$isiPod = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPod');
$isiPhone = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPhone');
$isAndroid= (bool) strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'android');

$is_mobile=$iPod || $isiPhone || $isAndroid;
if ($is_mobile) {
    header("Location: http://mobile.ono.ac.il");
}


define('WP_USE_THEMES', true);

/** Loads the WordPress Environment and Template */
require('./wp-blog-header.php');
?>