<?php

// Load Plugin Options
$options = get_option('promotion_slider_options');

// Add actions
add_action('promoslider_content', 'promoslider_display_image');
// Add options actions
if( $options['show_title'] ) 				add_action('promoslider_content', 'promoslider_display_title');
if( $options['show_excerpt'] ) 				add_action('promoslider_content', 'promoslider_display_excerpt');
if( $options['nav_option'] == 'default' )	add_action('promoslider_nav', 'promoslider_display_nav');
if( $options['nav_option'] == 'links' ) 	add_action('promoslider_nav', 'promoslider_display_links');
if( $options['nav_option'] == 'thumb' ) 	add_action('promoslider_thumbnail_nav', 'promoslider_thumb_nav');

function promoslider_display_image($values){
  global $post; extract($values);
  // Check to see if ad code should be displayed
  if( get_post_meta($post->ID, '_promo_slider_show_ad_code', TRUE) ): 
    echo get_post_meta($post->ID, '_promo_slider_ad_code', TRUE);
  // Otherwise, show featured image
  elseif( $thumb ): ?>
    <div class="promo_slider_background_image"><?php
      if( !$disable_links ) echo '<a href="'.$destination_url.'" target="'.$target.'">';
	  echo $thumb;
	  if( !$disable_links ) echo '</a>'; ?>
    </div><?php
  endif;
}

function promoslider_display_title($values){
  global $post; extract($values);
  $options = get_option('promotion_slider_options');
  // If ad code is being displayed, don't show the title
  if( get_post_meta($post->ID, '_promo_slider_show_ad_code', TRUE) ) return;
  // Otherwise, if there is a title, display it
  if( $title ): ?>
    <div class="promo_slider_title<?php if( !$options['disable_fancy_title'] ) echo ' fancy_ps_title'; ?>"><?php
	  if( !$disable_links ) echo '<a href="'.$destination_url.'" target="'.$target.'">';
	  echo $title;
	  if( !$disable_links ) echo '</a>'; ?>
    </div><?php
  endif;
}

function promoslider_display_excerpt($values){
  global $post; extract($values);
  // If add code is being displayed, don't show the excerpt
  if( get_post_meta($post->ID, '_promo_slider_show_ad_code', TRUE) ) return;
  // Otherwise, if there is an excerpt, display it
  if($excerpt): ?>
    <div class="promo_slider_excerpt"><?php echo $excerpt; ?></div><?php
  endif;
}

function promoslider_display_nav(){ // Display the default navigation
  $options = get_option('promotion_slider_options'); ?>
  <div class="promo_slider_nav<?php if( !$options['disable_fancy_nav'] ) echo ' fancy_ps_nav'; ?>">
	<span class="move_backward pointer" title="Move Backward">&lt;&lt;</span>
	<span class="slider_selections pointer"></span>
	<span class="move_forward pointer" title="Move Forward">&gt;&gt;</span>
  </div><?php
}

function promoslider_display_links(){ // Display the alternate navigation ?> 
  <div class="right_arrow move_forward pointer ps_hover" title="Move Forward"></div>
  <div class="left_arrow move_backward pointer ps_hover" title="Move Backward"></div><?php 
}

function promoslider_thumb_nav($values){ // Display thumbnail navigation 
  extract($values); ?>
  <div class="promo_slider_thumb_nav"<?php if($width) echo ' style="'.$width.'"'; ?>>
    <div class="right_arrow move_forward pointer" title="Move Forward"></div>
    <div class="thumb_nav slider_selections pointer">
      <?php foreach($thumbs as $key => $thumb): ?>
        <span class="<?php echo $key + 1; ?>">
          <?php echo $thumb; ?>
        </span>
      <?php endforeach; ?>
    </div>
    <div class="clear"></div>
    <div class="left_arrow move_backward pointer" title="Move Backward"></div>
  </div><?php
}

?>