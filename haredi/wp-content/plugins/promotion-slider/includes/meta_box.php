<?php 
global $post;
wp_nonce_field( 'ps_update_promotion', 'promo_slider_noncename' ); 

// Setup form data
$target = get_post_meta($post->ID, '_promo_slider_target', TRUE);
$url = get_post_meta($post->ID, '_promo_slider_url', TRUE);
$disable_links = get_post_meta($post->ID, '_promo_slider_disable_links', TRUE);
$show_ad_code = get_post_meta($post->ID, '_promo_slider_show_ad_code', TRUE);
$ad_code = get_post_meta($post->ID, '_promo_slider_ad_code', TRUE);

// Shortcut variables
$selected = ' selected="selected"';
$checked = ' checked="checked"';
?>

<br /><h3><?php _e('Change Linking Behaviour'); ?></h3>

<!-- Link Target Attribute -->
  <p>
    <label for="_promo_slider_target"><?php _e('Select the behaviour for this link:');?> </label>
    <select name="_promo_slider_target">
      <option value=""<?php if( empty($target) ) echo $selected; ?>><?php _e('-- Choose One --'); ?></option>
      <option value="_self"<?php if( $target == '_self' ) echo $selected; ?>><?php _e('Open link in same page'); ?></option>
      <option value="_blank"<?php if( $target == '_blank' ) echo $selected; ?>><?php _e('Open link in a new page'); ?></option>
    </select>
  </p>

<!-- Destination URL --><br />
  <p><?php _e('By default, all links will point to this promotion page.  If you want to have your links point elsewhere, set the destination URL below.'); ?></p>
  <p>
    <label for="_promo_slider_url"><?php _e('Destination URL: ') ?></label> 
    <input type="text" id= "_promo_slider_url" name="_promo_slider_url" value="<?php if(!empty($url)) echo $url; ?>" size="75" />
  </p>

<!-- Disable Promotion Page --><br />
  <p><?php _e("If you just want to display the featured image and don't want to use this promotion page, you can disable all links here."); ?></p>
  <p>
    <input type="checkbox" id= "_promo_slider_disable_links" name="_promo_slider_disable_links" value="true"<?php if($disable_links) echo $checked; ?> size="75" />
    <label for="_promo_slider_disable_links"><?php _e('Disable all links for this promotion') ?></label>
  </p>

<br /><h3 class="hndle"><?php _e('Insert Ad Code'); ?></h3>

<!-- Show Ad Code -->
  <p><?php _e('Use the options here to display third party ads, such as Google AdSense ads, on your slider.'); ?></p>
  <p>
    <input type="checkbox" id= "_promo_slider_show_ad_code" name="_promo_slider_show_ad_code" value="true"<?php if($show_ad_code) echo $checked; ?> size="75" />
    <label for="_promo_slider_show_ad_code"><?php _e('Display the ad code below rather than the featured image') ?></label>
  </p>

<!-- Ad Code --><br />
  <p><?php _e('Insert ad code here:'); ?></p>
  <p><textarea id= "_promo_slider_ad_code" name="_promo_slider_ad_code" rows="10" style="width:100%;"><?php echo $ad_code; ?></textarea></p>