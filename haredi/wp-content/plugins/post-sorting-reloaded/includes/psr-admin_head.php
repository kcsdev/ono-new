<?php

add_action('admin_head-settings_page_psr', 'psr_add_help');

function psr_add_help(){
	$help_content = '
		<h3><strong>Condition notes:</strong></h3>
		<ul>
			<li>Condition 1 has <strong>precedence</strong> over condition 2, and so on.</li>
			<li>You may also use <strong>logical operators</strong> in the condition box: <code>is_home() || is_tag(\'hat\')</code> or <code>is_tag() && !is_admin()</code></li>
			<li>If you want <strong>sitewide sorting</strong>, just put the number 1 in the condition box.</li>
			<li><strong>Exotic sample</strong> -- selecting all ancestors of certain category: <code>cat_is_ancestor_of(3, get_query_var(\'cat\'))</code></li>
		</ul>

		<h3><strong>Sort type notes:</strong></h3>
		<ul>
			<li><strong>ASC</strong> stands for ascending, <strong>DESC</strong> stand for descending. If you leave these out, the default is ascending.</li>
			<li>You can use <strong>any field</strong> from the <a href="http://codex.wordpress.org/Database_Description#Table:_wp_posts">wp_posts table</a>.</li>
			<li>The <strong>defalut</strong> WordPress value is <code>post_date DESC</code></li>
			<li><strong>Exotic sample</strong> -- ignoring certain words when sorting: replace <code>post_title</code> with <code>REPLACE(post_title, \'The \', \'\')</code></li>
		</ul>
	';
	add_contextual_help('settings_page_psr', $help_content);
}
