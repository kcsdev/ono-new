	<p class="row1">
		<label><?php echo 'Select type:'; ?></label>
		<em> <?php
			$display_block = '';
			$section_identifier = isset($_POST['section_identifier']) ? $_POST['section_identifier'] : '';
			$content_type = '';
			$section_name = '';
			$post_ids = '';
			$category_id = '';
			$num_posts = '';
			$feed_url = '';

			$size = '';
			$num_posts_xml_feed = '';

			$disabled_button_array = array();
			$disabled_button = get_section_info($section_identifier, 'disabled_radio');
			if (is_array($disabled_button)) {
				$disabled_button_array = $disabled_button;
			}

			$sec_obj = new Section;
			$sec_obj->section_identifier = $section_identifier;
			$section_object = $sec_obj->section_exists();
			if (is_object($section_object)) {
				$content_type = $section_object->section_meta_key;
				$section_name = $section_object->section_name;
				switch ($content_type) {
					case 'specific_content':
						$display_specific_content_block = 1;
						$post_ids = $section_object->section_meta_value;
					break;
					case 'category_radio':
						$display_category_radio_block = 1;
						$category_id = $section_object->section_meta_value;
						$num_posts = $section_object->length;
					break;
					case 'xml_feed':
						$display_xml_feed_block = 1;
						$feed_url = $section_object->section_meta_value;
						$num_posts_xml_feed = $section_object->length;
					break;
					default:
						$display_block = 0;
				}
			} else {
				$disabled_button_array[] = 'reset';
			}
			global $selection_criteria;
			display_radio_buttons($selection_criteria, 'content_type', $content_type, $disabled_button_array ); ?>
		</em>
	</p>
	<p class="row1">
		<label><?php echo 'Enter Section Name:';?></label>
		<em><input type="text" name="section_name" value="<?php echo $section_name; ?>" id="section_name" /></em>
	</p>
	<div id="content_desc" <?php if (empty($display_specific_content_block)) { ?> style="display:none;" <?php } ?> class="row1">
		<div>
			<label><?php echo 'Select Posts:';?></label>
			<div id="content1" class="section_content_selection_parent" style="width:640px;">
				<strong>
					<dfn><?php echo 'Click on Add button to select from latest 150 posts';?></dfn>
					<del>
						<input type="text" name="search_posts_text" value="Search Posts.." id="search_posts_text" size="20" />
						<input type="button" id="search_posts_button" value="search" />
					</del>
				</strong>
				<div id="specific_content_container" class="section_content_child_overflow" style="border:1px solid #ccc;"> <?php
					$section_ids_array = array();
					$args = array( 'posts_per_page' => 150, 'post_status' => 'publish'); //Give -1 if you want to show all posts but it requires high memory utilization
					if ($post_ids)  {
						$section_ids_array = explode(',', $post_ids);
						$args['post__not_in'] = $section_ids_array;
					}
					require_once ('show_posts.php');//We are using same UI in search ?>
				</div>
				<div class="section_content_selection_parent" id="selections" style="padding-top:10px; float:left; width:632px;">
					<strong>
						<dfn id="labelSelections"><?php echo 'Drag and Drop Posts to rearrange in any order';?></dfn>
					</strong>
					<div id="labelSelections_" class="tableDemo section_content_child" style="padding:4px;">
						<div class="section_content_child_overflow">
							<table cellspacing="0" cellpadding="2" width="99%" id="table_selections">
								<tbody id="tbody_selections"> <?php
									if ($post_ids)  {
										$section_ids_array = explode(',', $post_ids);
										for($i=0; $i < count($section_ids_array); $i++)  {
											$post_info = get_post($section_ids_array[$i]); ?>
											<tr id="entry_<?php echo $section_ids_array[$i];?>" style="cursor: move;">
												<td style="width:75%"><?php echo $post_info->post_title; ?></td>
												<td><?php echo get_the_time('j M, Y', $post_info->ID); ?></td>
												<td id="action_entry_<?php echo $section_ids_array[$i];?>" style="cursor: auto;">Remove</td>
											</tr> <?php
										}
									} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p id="category_desc" <?php if (empty($display_category_radio_block)) { ?> style="display:none;" <?php } ?> class="row1">
		<label>Choose Category:</label>
		<em> <?php
			wp_dropdown_categories( array( 'name' => 'categories_list', 'selected' => $category_id, 'orderby' => 'name', 'hierarchical' => 1 ) ); ?>
		</em>
		<span>
			<label>Number Of Posts:</label>
			<em><?php display_selection_box('no_posts_category', 10, $num_posts); ?></em>
		</span>
	</p>
	<p id="xml_feed_desc" <?php if (empty($display_xml_feed_block)) { ?> style="display:none;" <?php } ?> class="row1">
		<label>Enter Xml Feed Url:</label>
		<em><input type="text" name="xml_feed_url" value="<?php echo $feed_url; ?>" id="xml_feed_url" /> </em>
		<span>
			<label>Number Of Posts:</label>
			<em><?php display_selection_box('no_posts_xml_feed', 10, $num_posts_xml_feed); ?></em>
		</span>
	</p> <?php
	if (empty($section_identifier) ) { ?>
		<p class="row1">
			<label>&nbsp;</label>
			<em>
				<input onclick="validate_form()" type="submit" class="button-primary" value="Submit" />
			</em>
		</p> <?php
	} ?>
	<input type="hidden" name="nonce_field" id="nonce_field" value="<?php echo wp_create_nonce( 'select_criteria' ); ?>"/>
	<input type="hidden" name="selected_entries" id="selected_entries" />