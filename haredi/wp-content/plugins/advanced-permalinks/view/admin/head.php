<?php if (!defined ('ABSPATH')) die (); ?><link rel="stylesheet" href="<?php echo $this->url () ?>/admin.css" type="text/css" media="all" title="no title" charset="utf-8"/>
<script type="text/javascript" charset="utf-8">
	var wp_apl_base = '<?php echo $this->url () ?>/ajax.php';
	var wp_apl_delete = '<?php _e ('Are you sure you want to delete this permalink?', 'advanced-permalinks'); ?>';
</script>
<script type="text/javascript" charset="utf-8" src="<?php echo $this->url () ?>/js/jquery.pack.js?version=<?php echo $this->version () ?>"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo $this->url () ?>/js/admin.js?version=<?php echo $this->version () ?>"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo $this->url () ?>/js/jform.js?version=<?php echo $this->version () ?>"></script>
