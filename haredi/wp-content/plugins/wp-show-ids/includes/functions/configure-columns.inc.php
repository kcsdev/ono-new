<?php
/*
Copyright: © 2009 WebSharks, Inc. ( coded in the USA )
<mailto:support@websharks-inc.com> <http://www.websharks-inc.com/>

Released under the terms of the GNU General Public License.
You should have received a copy of the GNU General Public License,
along with this software. In the main directory, see: /licensing/
If not, see: <http://www.gnu.org/licenses/>.
*/
/*
Direct access denial.
*/
if (realpath (__FILE__) === realpath ($_SERVER["SCRIPT_FILENAME"]))
	exit ("Do not access this file directly.");
/*
Configure all of the required Hooks/Filters for columns & css.
Attach to: add_action("admin_init");
*/
if (!function_exists ("ws_plugin__wp_show_ids_configure"))
	{
		function ws_plugin__wp_show_ids_configure () /* The magic happens here. */
			{
				global $wp_post_types, $wp_taxonomies; /* Grab these global references. */
				/**/
				do_action ("ws_plugin__wp_show_ids_before_configure", get_defined_vars ());
				/**/
				add_action ("admin_head", "_ws_plugin__wp_show_ids_echo_css"); /* Add CSS. */
				/**/
				add_filter ("manage_edit-post_columns", "_ws_plugin__wp_show_ids_return_column");
				add_action ("manage_posts_custom_column", "_ws_plugin__wp_show_ids_echo_value", 10, 2);
				/**/
				add_filter ("manage_edit-comments_columns", "_ws_plugin__wp_show_ids_return_column");
				add_action ("manage_comments_custom_column", "_ws_plugin__wp_show_ids_echo_value", 10, 2);
				/**/
				add_filter ("manage_edit-page_columns", "_ws_plugin__wp_show_ids_return_column");
				add_action ("manage_pages_custom_column", "_ws_plugin__wp_show_ids_echo_value", 10, 2);
				/**/
				add_filter ("manage_link-manager_columns", "_ws_plugin__wp_show_ids_return_column");
				add_action ("manage_link_custom_column", "_ws_plugin__wp_show_ids_echo_value", 10, 2);
				/**/
				add_filter ("manage_edit-link-categories_columns", "_ws_plugin__wp_show_ids_return_column");
				add_filter ("manage_link_categories_custom_column", "_ws_plugin__wp_show_ids_return_value", 10, 3);
				/**/
				add_filter ("manage_upload_columns", "_ws_plugin__wp_show_ids_return_column");
				add_action ("manage_media_custom_column", "_ws_plugin__wp_show_ids_echo_value", 10, 2);
				/**/
				add_filter ("manage_users_columns", "_ws_plugin__wp_show_ids_return_column");
				add_filter ("manage_users_custom_column", "_ws_plugin__wp_show_ids_return_value", 10, 3);
				/**/
				if (is_array ($wp_post_types))
					foreach ($wp_post_types as $type => $post_type_object) /* Handle WP 3.0+ Post Types. */
						{
							add_action ("manage_edit-${type}_columns", "_ws_plugin__wp_show_ids_return_column");
							add_filter ("manage_${type}_custom_column", "_ws_plugin__wp_show_ids_return_value", 10, 3);
						}
				/**/
				if (is_array ($wp_taxonomies))
					foreach ($wp_taxonomies as $taxonomy => $taxonomy_o) /* Handle WP 3.0+ Taxonomies. */
						{
							add_action ("manage_edit-${taxonomy}_columns", "_ws_plugin__wp_show_ids_return_column");
							add_filter ("manage_${taxonomy}_custom_column", "_ws_plugin__wp_show_ids_return_value", 10, 3);
						}
				/**/
				do_action ("ws_plugin__wp_show_ids_after_configure", get_defined_vars ());
			}
	}
?>