<?php
/**
 * Template Name: תבנית פורום בוגרים
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<style>
	#main{padding: 20px 0 0 0;}
	#container {
		float: right;
		margin: 0 0 0 -350px;
	}
	#content {
		margin: 0 20px 36px 50x;
	}
	.widget-area{margin-top:0px;}
	.side_menu{
		width:326px;
	}
	/*.chat {
		left:702px !important;
	}
	#lang_sel_footer  {
		right:527px;
	}*/
</style>
<div class="upper_banner">

			<?php 

          $pageparentarray =  get_page( $page_id,ARRAY_A ); 
	  $pageparent = $pageparentarray[post_parent];
	  $parent_title = get_the_title($post->post_parent);
	  $page_ancestors = get_post_ancestors( $post );
	  //print_r($page_ancestors);

	  $parent = wp_list_pages('title_li=&child_of='.$pageparent.'&echo=0&depth=1&exclude=11846'); //?????????????
	  $parent=str_replace("page_item page-item-1785","page_item page-item-1785 current_page_item",$parent);
	  $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=1&exclude=11846');
	  $parent_url = get_permalink( $pageparent );
	  //echo $parent;
	  $split_parent = split('</li>',$parent);
	//print_r($split_parent);
	//meta('upperpic');
	$meta_values = get_post_meta($post->ID, 'upperpic', true);
	
//ob_start();
//    	meta('upperpic');
//    	$content = ob_get_contents();
//	ob_end_clean();
	
if($meta_values){echo $meta_values;}
else{

		foreach($page_ancestors as $post_id){
			
				if (get_post_meta($post_id, 'upperpic',true)){
					echo( get_post_meta($post_id, 'upperpic',true));
					break;
				}
		}	
	}
 ?> 
		</div>

		
		<div id="container">
			<div class="stories">
			  <div class="stories_header">סיפורי בוגרים</div>
				<div id = 'bogrim_slider' class="stories_text">					
					<?php $temp_query = $wp_query; $nump=1;?>
					<?php     query_posts('category_name=alumni-stories'); ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php $do_not_duplicate[$post->ID] = $post->ID; ?>
					<?php // the_post_thumbnail('thumbnail', 'class=alignleft'); ?>
					<div <?php echo "class=\"slider_content\""; echo "id=\"";echo $nump; echo "\" "; if($nump==1){echo "style=\"display:block;\"";}$nump+=1;?> >
					<strong class="news_title"><a href="<?php the_permalink();?>" ><?php the_title();?></a></strong>
						<?php the_glide_limit2(2000); ?>
					</div>
					<?php endwhile; else: ?>
        				<?php endif; ?>
					</div>
			</div>							
		</div><!-- #container -->

<div id="primary" class="widget-area" role="complementary">
	<?php //start menu

//if ($children){
	
	

	echo '<div class = "nav_menu">';


		 //begin to echo menu
	 
		
	
	
	//if (($content2)||($children)) {echo '<div class="side_menu_top">&nbsp;</div>';}
		
		//print_r($split_parent);
		echo '<ul class = "menulevel1">';
		if (count($page_ancestors) > 2 ){echo "<li><a href ='$parent_url'>$parent_title</a><li>";}
		if (count($page_ancestors) > 1 ){

			foreach($split_parent as $uncle){
				
				    if (strrpos($uncle,'current_page_item')){
					    echo $uncle;
						if ($children){ //to make sure no empty ul is created
							echo '<ul class = "menulevel2">' . $children . "</ul>";
						}
			   }
			}
		}//end counting ancestors
		
	//	if (($content2)||($children)) {echo '<div class="side_menu_bot"></div>';}
	echo '</div> ';
//}

  //menu end here?>
	
</div>
								
	<div class="forum_news">
				<div class="contenthome" >
				<img alt="" src="images/forum_news.jpg">
				<a class="more" href="/haredi/index.php?cat=13">עוד</a>
				 <div id="bogrim_news_slider" class="news_slider">
					<?php $temp_query = $wp_query; $nump=1; ?>
					<?php     query_posts('category_name=bogrim_news&showposts=3'); ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php $do_not_duplicate[$post->ID] = $post->ID; ?>
					<div <?php echo "class=\"slider_content\""; echo "id=\"";echo $nump; echo "\" "; if($nump==1){echo "style=\"display:block;\"";}$nump+=1;?> >
					<div class="glidetext"#>
					<strong class="news_title"><a href="<?php the_permalink();?>" ><?php the_title();?></a></strong>
						<?php the_glide_limit2(200); ?>
					</div>
					</div>
					<?php endwhile; else: ?>
        				<?php endif; ?>
					</div>
			</div>
			
			<div class="contenthome">
				<img alt="" src="images/forum_events.jpg">
				<a class="more" href="/haredi/index.php?cat=12">עוד</a>
					<div id="bogrim_events_slider" class="events_slider">
					<?php $temp_query = $wp_query; $nump=1; ?>
					<?php     query_posts('category_name=bogrim_events&showposts=3'); ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php $do_not_duplicate[$post->ID] = $post->ID; ?>
					<div <?php echo "class=\"slider_content\""; echo "id=\"";echo $nump; echo "\" "; if($nump==1){echo "style=\"display:block;\"";}$nump+=1;?> >
					<div class="glidetext">
					<strong class="news_title"><a href="<?php the_permalink();?>" ><?php the_title();?></a></strong>
						<?php $chars = get_option('glideshow-text-length');?>
						<div class="time">
							<?php $post_Data =  get_post_custom_values("event_date", $post->ID);
							      $post_Time =  get_post_custom_values("event_time", $post->ID);
							list ($post_year,$post_month,$post_day) = split ('[/.-]', $post_Data[0]); ?>
  							<span class="year">
								<?php echo $post_year; ?>
							</span>
							<span class="date">
								<?php echo $post_day.'.'.$post_month; ?>
							</span>
							<span class="hour">
								<?php echo $post_Time[0] ?>
							</span>
						</div>
						<?php the_glide_limit2(200); ?>
					</div>
						
					</div>
					<?php endwhile; else: ?>
        				<?php endif; ?>
					</div>

			</div>
                       
			
			<div class="contenthome " style="margin:0px;">
				<img alt="" src="images/forum_atavot.jpg">
				<a class="more" href="/haredi/index.php?cat=11">עוד</a>
				<div id="bogrim_atavot_slider" class="news_slider">
					<?php $temp_query = $wp_query; $nump=1; ?>
					<?php     query_posts('category_name=bogrim_atavot&showposts=3'); ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php $do_not_duplicate[$post->ID] = $post->ID; ?>
					<div <?php echo "class=\"slider_content\""; echo "id=\"";echo $nump; echo "\" "; if($nump==1){echo "style=\"display:block;\"";}$nump+=1;?> >
					<div class="glidetext">
					<strong class="news_title"><a href="<?php the_permalink();?>" ><?php the_title();?></a></strong>
						<?php the_glide_limit2(200);?>
					</div>
					</div>
					<?php endwhile; else: ?>
        				<?php endif; ?>
					</div>
			</div>
	</div>


<?php get_footer(); ?>
</div>