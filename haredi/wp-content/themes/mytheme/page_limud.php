<?php
/**
 * Template Name: תבנית תחומי לימוד
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<style>
	.entry-content {
width:500px;
}

	#primary {	
		margin-top:40px;
	}
#container {
		
		margin: 0 0 0 -350px;
	}
	#content {
		float: right;
	
		
		margin: 0 20px 36px 0px;
	}
	.widget-area{margin-top:17px;}
	.side_menu{
		width:326px;
	}
	
	#content .entry-title{font-size:27px;top:255px; right:20px}
	
</style>
		<div class="upper_banner">

			<?php 

$pageparentarray =  get_page( $page_id,ARRAY_A ); 
	  $pageparent = $pageparentarray[post_parent];
	  $parent_title = get_the_title($post->post_parent);
	  $page_ancestors = get_post_ancestors( $post );	

	  $parent = wp_list_pages('title_li=&child_of='.$pageparent.'&echo=0&depth=1');
	  $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=1');
	  $parent_url = get_permalink( $pageparent );
	  //echo $parent;
	$split_parent = split('</li>',$parent);
	//print_r($split_parent);
	meta('upperpic');
	$meta_values = get_post_meta($post->ID, 'upperpic', true);
	
ob_start();
    	meta('upperpic');
    	$content = ob_get_contents();
	ob_end_clean();
	
if($meta_values){echo $meta_values;}
else{

		foreach($page_ancestors as $post_id){
			
				if (get_post_meta($post_id, 'upperpic',true)){
					echo( get_post_meta($post_id, 'upperpic',true));
					break;
				}
		}	
	}
 ?> 
		</div>
		<div id = 'bread' style ="padding-top:3px;padding-bottom:5px;"> 
			<?php if (class_exists('breadcrumb_navigation_xt')) {
	
	
			echo 'הינך כאן : ';
				// New breadcrumb object
				$mybreadcrumb = new breadcrumb_navigation_xt;
				// Options for breadcrumb_navigation_xt
				$mybreadcrumb->opt['title_blog'] = '';
				$mybreadcrumb->opt['separator'] = ' &raquo; ';
				$mybreadcrumb->opt['singleblogpost_category_display'] = true;
				// Display the breadcrumb
				$mybreadcrumb->display();
			} ?>
		</div>
		<div id="container">
			<div id="content" role="main">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

			
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
						<?php the_content(); ?>
	
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
						
															<div class="special">
			
    <h2>תוכניות מיוחדות</h2>
    <a href="">lתוכנית הציונות הדתית</a><br/><p>תכנית הלימודים הינה מסגרת אינטגרטיבית וכוללנית המקיפה את תחומי השיווק, הפירסום והתקשורת השיווקית ידע אקדמי מעמיק, תוכנית הלימודים על היבטיהם השונים והיא בנויה ומתעדכנת על העת בהתאם למגמות החדשות. נא לשלוח טקסט  כאן</p> 
    <h2>תוכניות העשרה ותרומה לקהילה</h2>

    <div class="special_r">
        <img src="">
        <a>תכנית מצפן</a>
        <p>מטרת התוכנית למצוינות ופרקטיקה ניהולית הינה הידוק קשרי הגומלין בין האקדמייה לתעשייה</p>
    </div>
    
    <div class="special_r">
        <img src="">
        <a>העשרה</a>
        <p>מטרת התוכנית למצוינות ופרקטיקה ניהולית הינה הידוק קשרי הגומלין בין האקדמייה לתעשייה</p>
    </div>
    
</div>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
		


					</div><!-- .entry-content -->
				</div><!-- #post-## -->

				<?php comments_template( '', true ); ?>

<?php endwhile; ?>


			</div><!-- #content -->
<div style="float:left;width:324px;">
<?php //start menu

//if ($children){
	
	

	echo '<div class = "nav_menu">';


		 //begin to echo menu
	 
		
	//if($children){}
	
	ob_start();
	
	if (count($page_ancestors) > 1 ){

			foreach($split_parent as $uncle){
				
				    if (strrpos($uncle,'current_page_item')){
						if ($children){ //to make sure no empty ul is created
							echo $children ;
						}
			   }
			}
		      }//end counting ancestors
		else{
			//echo '<ul class = "menulevel1">' .$children . "</ul>";
		}
    	
    	$content2 = ob_get_contents();
	ob_end_clean();
	
	if (($content2)||($children)) {echo '<div class="side_menu_top">&nbsp;</div>';}
		echo '<ul class = "menulevel1">';
		if (count($page_ancestors) > 1 ){

			foreach($split_parent as $uncle){
				echo $uncle;
				    if (strrpos($uncle,'current_page_item')){
						if ($children){ //to make sure no empty ul is created
							//echo '<ul class = "menulevel2">' . $children . "</ul>";
						}
			   }
			}
		      }//end counting ancestors
		else{
			echo '<ul class = "menulevel1">' .$children . "</ul>";
		}
		if (($content2)||($children)) {echo '<div class="side_menu_bot"></div>';}
	echo '</div> ';
//}

  //menu end here?>
	<div class="news_slider">
					<?php $temp_query = $wp_query; $nump=1; ?>
					<?php     query_posts('category_name=news&showposts=3'); ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php $do_not_duplicate[$post->ID] = $post->ID; ?>
					<div <?php echo "class=\"odot_news_content\""; echo "id=\"";echo $nump; echo "\" "; if($nump==1){echo "style=\"display:block;\"";}$nump+=1;?> >
					<div class="glidetext">
					<strong class="news_title"><a href="<?php the_permalink();?>" ><?php the_title();?></a></strong>
						<?php the_glide_limit2(500); ?>
					</div>
					</div>
					<?php endwhile; else: ?>
        				<?php endif; ?>
	</div>
</div>

		</div><!-- #container -->



	

</div>
<?php get_footer(); ?>
</div>
