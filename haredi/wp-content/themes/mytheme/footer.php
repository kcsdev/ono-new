<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	</div><!-- #main -->
<?php	$url_end = $_GET['lang'];
$lang = 'he';

$page_ancestors = get_post_ancestors( $post );	

switch($url_end){
	case 'en':
	$lang = 'en';
	break;
	case 'ar':
	$lang = 'ar';
	break;
	case 'ru':
	$lang = 'ru';
	break;
	default:
	$lang = 'he';
	break;
}


$utm_source=isset($_COOKIE['utm_source']) ? $_COOKIE['utm_source'] : @$_REQUEST['utm_source'];
$utm_medium=isset($_COOKIE['utm_medium']) ? $_COOKIE['utm_medium'] : @$_REQUEST['utm_medium'];
$utm_campaign=isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : @$_REQUEST['utm_campaign'];	

?>
	<div class="footer">
			<div class="contact">

			
			
			<?php
			
			if (substr_count(urldecode(get_permalink()),"ייעוץ-ורישום-תכנית-הציונות-הדתית")>0 || substr_count(urldecode(get_permalink()),"היחידה-ללימודי-חוץ")>0) {
			
			} else {
				?>
				<img src="<?php          if ($lang=='he'){echo 'images/contact_title.png';}
						 if ($lang=='en'){echo 'images/english/contact_title.png';}
						?>" alt="">
				
				<?

				if ($lang=='en') {echo do_shortcode('[contact-form 18 "FotterContactForm"]');}
				//if ($lang=='he') {echo do_shortcode('[contact-form 5 "צור קשר פוטר"]');}
				if ($lang=='he') {
					
					//echo do_shortcode('[contact-form 5 "צור קשר פוטר"]');
					?>
					<form action="https://web2.ono.ac.il/leeds/SqlCheckIncludeLeeds.asp" id="leeds" method="post" name="leeds" style="" target="_top">
	<table border="0" cellpadding="0" cellspacing="0" id="table1" style='max-width:1055px !important;width:975px !important;'>
		<tr>
			<td style="width: 62px;" valign="bottom" class="style1">
			<strong style="">שם פרטי:</strong></td>
			<td style="width: 60px" valign="bottom" class="style1">
			<input name="firstName" type="text" size="8" /></td>
			<td style="width: 5px;" valign="bottom" class="style1"> </td>
			<td style="width: 60px;" valign="bottom" class="style1">
			<strong style="">משפחה:</strong></td>
			<td style="width: 90px" valign="bottom" class="style1">
			<input name="lastName" type="text" size="10" /></td>
			<td style="width: 5px;" valign="bottom" class="style1"> </td>
			<td style="width: 25px;" valign="bottom" class="style1">
			<strong style="">טל':</strong></td>
			<td style="width: 80px" valign="bottom" class="style1">
			<input name="mobile" type="text" size="11" /></td>
			<td style="width: 5px;" valign="bottom" class="style1"> </td>
			<td style="width: 55px;" valign="bottom" class="style1">
			<span lang="he"><strong>דוא"ל</strong></span><span style=""><strong>:</strong></span></td>
			<td style="width: 70px" valign="bottom" class="style1">
			<input name="eMail" type="text" size="10" /></td>
			<td style="width: 5px;" valign="bottom" class="style1"> </td>
			<td style="width: 75px;" valign="bottom" class="style1">
			<span lang="he"><strong>תחום עניין:</strong></span></td>
			<td style="width: 70px" valign="bottom" class="style1">

			<select name="interest">
			<option value="כללי">כללי</option>
			<option value="BA מנהל עסקים">B.A. במנהל עסקים</option>
			<option value="פרסום ותקשורת">B.A. בפרסום ותקשורת</option>
			<option value="ציונות דתית">ציונות דתית</option>
			<option value="MBA מנהל עסקים">M.B.A. במנהל עסקים</option>
			<option value="LLB משפטים">L.L.B. במשפטים</option>
			<option value="LLM משפטים">L.L.M. במשפטים</option>
			<option value="מוסיקה">תואר ראשון במוסיקה</option>
			<option value="מקצועות הבריאות">מקצועות הבריאות</option>
			<option value="הפרעות בתקשורת">B.A. בהפרעות בתקשורת</option>
			<option value="פיזיותרפיה">B.P.T. בפיזיותרפיה</option>
			<option value="ריפוי בעיסוק">B.O.T. בריפוי בעיסוק</option>
			</select> </td>
			<td style="width: 5px;" valign="bottom" class="style1"> </td>
			
			<td style="width: 40px" style="" valign="bottom" class="style1">
			<input dir="ltr" name="send" style="font-family: Arial;" type="submit" value="שליחה" /></td>
		</tr>
	</table>
	<input id="sourceSite" name="sourceSite" type="hidden" value="ono" />
	<input id="sourceID" name="sourceID" type="hidden" value="6" />
	<input id="sourceType" name="sourceType" type="hidden" value="1" />
</form>
					<?php
					
				}
			}
			
			
			?>

			</div>
			<div class="ft_links" style="height:200">
				<div class="link_up" style="height:400">
<ul>
	<li>
	<p class="MsoNormal" dir="RTL">
	<span lang="HE" style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<strong>משפטים</strong></span>
	<br />
	
	<a href="http://www.ono.ac.il/haredi/תחומי-לימוד/תואר-ראשון-במשפטים/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר ראשון במשפטים</span></a><br>
	<a href="http://www.ono.ac.il/haredi/schools2/llb/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר שני במשפטים</span></a><br>
	</p>
	</li>
	<li>
	<p class="MsoNormal" dir="RTL">
	<span lang="HE" style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<strong>מנהל 
	עסקים</strong></span><br>
	<a href="http://www.ono.ac.il/haredi/schools2/business-administration/ba/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	תואר ראשון במנהל עסקים</span></a><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://ono.ac.il/haredi/%d7%aa%d7%97%d7%95%d7%9e%d7%99-%d7%9c%d7%99%d7%9e%d7%95%d7%93/%d7%94%d7%a4%d7%a7%d7%95%d7%9c%d7%98%d7%94-%d7%9c%d7%9e%d7%a0%d7%94%d7%9c-%d7%a2%d7%a1%d7%a7%d7%99%d7%9d/b-a-%d7%91%d7%9e%d7%a0%d7%94%d7%9c-%d7%a2%d7%a1%d7%a7%d7%99%d7%9d/%d7%94%d7%aa%d7%9e%d7%97%d7%95%d7%99%d7%95%d7%aa-b-a/b-a-%d7%94%d7%aa%d7%9e%d7%97%d7%95%d7%aa-%d7%97%d7%a9%d7%91%d7%95%d7%a0%d7%90%d7%95%d7%aa/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	חשבונאות</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://ono.ac.il/haredi/%d7%aa%d7%97%d7%95%d7%9e%d7%99-%d7%9c%d7%99%d7%9e%d7%95%d7%93/%d7%94%d7%a4%d7%a7%d7%95%d7%9c%d7%98%d7%94-%d7%9c%d7%9e%d7%a0%d7%94%d7%9c-%d7%a2%d7%a1%d7%a7%d7%99%d7%9d/b-a-%d7%91%d7%9e%d7%a0%d7%94%d7%9c-%d7%a2%d7%a1%d7%a7%d7%99%d7%9d/%d7%94%d7%aa%d7%9e%d7%97%d7%95%d7%99%d7%95%d7%aa-b-a/b-a-%d7%94%d7%aa%d7%9e%d7%97%d7%95%d7%aa-%d7%91%d7%9e%d7%99%d7%9e%d7%95%d7%9f-%d7%95%d7%a9%d7%95%d7%a7-%d7%94%d7%94%d7%95%d7%9f/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	מימון ושוק ההון</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/haredi/schools2/business-administration/ba/%D7%94%D7%AA%D7%9E%D7%97%D7%95%D7%99%D7%95%D7%AA-b-a/%D7%94%D7%AA%D7%9E%D7%97%D7%95%D7%AA-%D7%91%D7%A9%D7%99%D7%95%D7%95%D7%A7-%D7%95%D7%A4%D7%A8%D7%A1%D7%95%D7%9D/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	שיווק ופרסום</span></a></span><br>
	<span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/haredi/schools2/business-administration/ba/%D7%94%D7%AA%D7%9E%D7%97%D7%95%D7%99%D7%95%D7%AA-b-a/%D7%94%D7%AA%D7%9E%D7%97%D7%95%D7%AA-%D7%91%D7%A0%D7%99%D7%AA%D7%95%D7%97-%D7%9E%D7%A2%D7%A8%D7%9B%D7%95%D7%AA-%D7%9E%D7%99%D7%93%D7%A2/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	ניתוח מערכות מידע</span></a></span><br>
        <span style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<a href="http://www.ono.ac.il/haredi/schools2/business-administration/ba/%D7%94%D7%AA%D7%9E%D7%97%D7%95%D7%99%D7%95%D7%AA-b-a/%D7%A0%D7%99%D7%94%D7%95%D7%9C-%D7%9E%D7%A9%D7%90%D7%91%D7%99-%D7%90%D7%A0%D7%95%D7%A9/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	ניהול משאבי אנוש</span></a></span><br>
	</p>
	</li>
	<li>
	<p class="MsoNormal" dir="RTL">
	<span lang="HE" style="font-size: 9.0pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; color: #1F2022">
	<strong>מקצועות הבריאות</strong></span>
	<br />
	<a href="http://www.ono.ac.il/haredi/schools2/health/%d7%a7%d7%9c%d7%99%d7%a0%d7%90%d7%95%d7%aa-%d7%aa%d7%a7%d7%a9%d7%95%d7%a8%d7%aa/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	B.A. בהפרעות בתקשורת</span></a><br>
	<a href="http://www.ono.ac.il/haredi/schools2/health/%d7%a8%d7%99%d7%a4%d7%95%d7%99-%d7%91%d7%a2%d7%99%d7%a1%d7%95%d7%a7/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	B.O.T. בריפוי בעיסוק</span></a><br>
	<a href="http://www.ono.ac.il/haredi/schools2/health/b-p-t-%d7%91%d7%a4%d7%99%d7%96%d7%99%d7%95%d7%aa%d7%a8%d7%a4%d7%99%d7%94/">
	<span lang="HE" style="color: #868F95; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm">
	B.T.P.  בפיזיוטרפיה</span></a><br>
	</p>
	</li>
	
</ul>
					
					<div class="lin">
					<a href="/feed/"><img src="images/c_rss.png"></a>
					<a  id = 'use_terms' href="http://www.ono.ac.il/haredi/10923/">תנאי שימוש</a>
					<!--<a target="_blank" href="http://twitter.com/onoun
"><img src="images/c_twit.png"></a>
					<a target="_blank" href="http://www.youtube.com/user/OnoAcademicCollege"><img src="images/c_ytube.png"></a>
					<a target="_blank" href="http://www.facebook.com/OnoCollege
"><img src="images/c_face.png"></a>		-->
 <a id = 'kcslink' href='http://www.kcsnet.net/'>בניית אתרים</a>
 <img id="kidumitimg" src="/wp-content/themes/mytheme/images/kidumit.png">
<a id='kidumitlink' href="http://www.kidumit.org/">שיווק באינטרנט</a>

					</div>
				</div>
				<div class="link_down">
		
					<li><a href ='http://www.kcsnet.net/' > &nbsp;</a></li>
				</div>
			</div>
		</div><!-- #footer -->

</div><!-- #wrapper -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<? if ($post->post_parent !='1683' && !in_array('1683',$page_ancestors) && $post->ID !='1683' ): ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1081317-1']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();



</script>
<?php else:?>
<style>.custom_sidebar .wpcf7-response-output {
    margin-right: 160px;
    margin-top: -27px;}</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7023584-1']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_trackPageLoadTime']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   // ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- BEGIN WebLeads Chat Code -->
<script>
var prom = (("https:" == document.location.protocol) ? "https://" : "http://");
document.write ("<script type='text/javascript' src='" + prom + "wl.wizsupport.com/collect/webleadscoil/chatfloater.asp?lang=he&profile=232&url=" + escape(window.location) + "'></script"); 
document.write(">");
</script> 
<script> var prom = (("https:" == document.location.protocol) ? "https://" : "http://"); document.write("<script type='text/javascript' src='" + prom + "wl.wizsupport.com/collect/webleadscoil/collect.asp?url=" + escape(window.location) + "&refer=" + escape(document.referrer) + "&title=" + escape(document.title) + "&profile=232'></script"); 
document.write(">");
</script> 
<!-- END WebLeads Monitor. -->

<?php endif;?>

<?/*	if ($post->post_parent =='242' || in_array('242',$page_ancestors)  || $post->ID =='242' ): ?>	
<!-- BEGIN LivePerson Button Code -->
<div id="lpButDivID-1308486170595" class="chat" style="left:352px;"></div>
<script type="text/javascript" charset="UTF-8" src="https://server.iad.liveperson.net/hc/48236541/?cmd=mTagRepstate&site=48236541&buttonID=12&divID=lpButDivID-1308486170595&bt=1&c=1"></script>
<!-- END LivePerson Button code -->	
	<?php elseif ($post->post_parent !='1683' && !in_array('1683',$page_ancestors)  && $post->ID !='1683' ): ?>
	<?php if (substr_count(urldecode(get_permalink()),"ייעוץ-ורישום-תכנית-הציונות-הדתית")>0): ?>
		<!-- BEGIN LivePerson Button Code -->
		<div id="lpButDivID-1296375712440" class='chat' <?php if (substr_count($_SERVER[REQUEST_URI],"s=")>0) echo 'style="left:570px;"'; ?>></div>
		<script type="text/javascript" charset="UTF-8" src="https://server.iad.liveperson.net/hc/13943730/?cmd=mTagRepstate&site=13943730&buttonID=13&divID=lpButDivID-1296375712440&bt=1&c=1"></script>
		<!-- END LivePerson Button code -->
	<?php else: ?>
		<!-- BEGIN LivePerson Button Code -->
		<?php print_r ($_SERVER[REQUSET_URI]);?>
		<div id="lpButDivID-1296374881567" class='chat' <?php if (substr_count($_SERVER[REQUEST_URI],"s=")>0) echo 'style="left:570px;"'; ?> ></div>
		<script type="text/javascript" charset="UTF-8" src="http://server.iad.liveperson.net/hc/83183091/?cmd=mTagRepstate&site=83183091&buttonID=12&divID=lpButDivID-1296374881567&bt=1&c=1"></script>
		<!-- END LivePerson Button code -->
	<?php endif; ?>
	
	
	
	<?php if (substr_count(urldecode(get_permalink()),"ייעוץ-ורישום-תכנית-הציונות-הדתית")>0): ?>
	<!-- BEGIN LivePerson Monitor. --><script language='javascript'> var lpMTagConfig = {'lpServer' : "server.iad.liveperson.net",'lpNumber' : "13943730",'lpProtocol' : (document.location.toString().indexOf('https:')==0) ? 'https' : 'http'}; function lpAddMonitorTag(src){if(typeof(src)=='undefined'||typeof(src)=='object'){src=lpMTagConfig.lpMTagSrc?lpMTagConfig.lpMTagSrc:'/hcp/html/mTag.js';}if(src.indexOf('http')!=0){src=lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpServer+src+'?site='+lpMTagConfig.lpNumber;}else{if(src.indexOf('site=')<0){if(src.indexOf('?')<0)src=src+'?';else src=src+'&';src=src+'site='+lpMTagConfig.lpNumber;}};var s=document.createElement('script');s.setAttribute('type','text/javascript');s.setAttribute('charset','iso-8859-1');s.setAttribute('src',src);document.getElementsByTagName('head').item(0).appendChild(s);} if (window.attachEvent) window.attachEvent('onload',lpAddMonitorTag); else window.addEventListener("load",lpAddMonitorTag,false);</script><!-- END LivePerson Monitor. -->
	<?php else: ?>
	<!-- BEGIN LivePerson Monitor. --><script language='javascript'> var lpMTagConfig = {'lpServer' : "server.iad.liveperson.net",'lpNumber' : "83183091",'lpProtocol' : (document.location.toString().indexOf('https:')==0) ? 'https' : 'http'}; function lpAddMonitorTag(src){if(typeof(src)=='undefined'||typeof(src)=='object'){src=lpMTagConfig.lpMTagSrc?lpMTagConfig.lpMTagSrc:'/hcp/html/mTag.js';}if(src.indexOf('http')!=0){src=lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpServer+src+'?site='+lpMTagConfig.lpNumber;}else{if(src.indexOf('site=')<0){if(src.indexOf('?')<0)src=src+'?';else src=src+'&';src=src+'site='+lpMTagConfig.lpNumber;}};var s=document.createElement('script');s.setAttribute('type','text/javascript');s.setAttribute('charset','iso-8859-1');s.setAttribute('src',src);document.getElementsByTagName('head').item(0).appendChild(s);} if (window.attachEvent) window.attachEvent('onload',lpAddMonitorTag); else window.addEventListener("load",lpAddMonitorTag,false);</script><!-- END LivePerson Monitor. -->
	<?php endif; ?>
<?php endif;*/ ?>



<?php if( $post->ID !="16353" && $post->ID !="16984" && $post->ID !='11430'):?>
<!-- Google Code for Ono Site General Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "JSA4CL_xvAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=JSA4CL_xvAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif;?>



<?php if ($post->post_parent =='1172' || in_array('1172',$page_ancestors)  || $post->ID =='1172' ): ?>	
<!-- Google Code for BA Minhal Asakim Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "oKhkCLfyvAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=oKhkCLfyvAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif;?>

<?php if ($post->post_parent =='1563' || in_array('1563',$page_ancestors)  || $post->ID =='1563' ): ?>
<!-- Google Code for Heasbonut Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "47-LCKf0vAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=47-LCKf0vAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif;?>


<?php if ($post->post_parent =='13862' || in_array('13862',$page_ancestors)  || $post->ID =='13862' ): ?>
<!-- Google Code for Pirsum Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "7EtrCI_3vAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=7EtrCI_3vAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif;?>


<?php if ($post->post_parent =='1187' || in_array('1187',$page_ancestors)  || $post->ID =='1187' ): ?>

<!-- Google Code for LLB Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "8owICJ_1vAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=8owICJ_1vAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif; ?>

<?php if ($post->post_parent =='1208' || in_array('1208',$page_ancestors)  || $post->ID =='1208' ): ?>
<!-- Google Code for Briut Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "5odxCJf2vAIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=5odxCJf2vAIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif; ?>

<?php if ($post->post_parent =='1184' || in_array('1184',$page_ancestors)  || $post->ID =='1184' ): ?>
<!-- Google Code for MBA New Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038494939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "mdv0CJfQ6gIQ29mY7wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1038494939/?label=mdv0CJfQ6gIQ29mY7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php endif; ?>

<?php if ($post->ID=='11430'):?>

<!-- Google Code for צור קשר אתר - 2012 Conversion Page --> <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1050446152;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "JrcqCLjykAMQyJLy9AM"; var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript"
src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="http://www.googleadservices.com/pagead/conversion/1050446152/?value=0&amp;label=JrcqCLjykAMQyJLy9AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif; ?>


<?php if ($post->ID=='22054'): ?>
	<!-- Google Code for MBA Open Day Blat Aug 12 -->
	<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1038494939;
	var google_conversion_label = "AYVmCP_5zxQQ29mY7wM";
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1038494939/?value=0&amp;label=AYVmCP_5zxQQ29mY7wM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
<?php endif;?>



</body>
</html>