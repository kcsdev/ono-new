<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); 


?>

<style>
	#main{padding: 40px 0 0 0;}
	#container {
		float: right;
		margin: 0 0 0 -350px;
	}
	#content {
		float: right;
		margin: 0 20px 0px 0px;
		width:550px;
	}
	.widget-area{margin-top:0px;}
	.side_menu{
		width:326px;
	}	
</style>
<div class="upper_banner">
<?php


/*function get_taxo_number($id) {
    global $wpdb;
    $myrows = $wpdb->get_results("SELECT wp_term_relationships.object_id as id FROM wp_term_relationships LEFT JOIN wp_posts on (wp_term_relationships.object_id = wp_posts.ID)
                    LEFT JOIN wp_term_taxonomy on (wp_term_taxonomy.term_taxonomy_id = wp_term_relationships.term_taxonomy_id)
                    where wp_term_relationships.term_taxonomy_id = $id and wp_posts.post_status = 'publish' and wp_posts.post_type = 'post'");
    return $myrows;
}
print_r(get_taxo_number(1));*/
?>
<img src = 'images/Blank2.jpg'></img>
		</div>

		<div id="container">
			<div id="content" role="main">

				<h1 class="page-title"><?php
					printf( __( 'Category Archives: %s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '<div class="archive-meta">' . $category_description . '</div>';

				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				 $url_end = substr($_SERVER['REQUEST_URI'],-2);
				 $cat_num = $_GET['cat'];
				 
				 $category_name=str_replace("haredi/cat","",$_SERVER['REQUEST_URI']);
				 $category_name=str_replace("/","",$category_name);
				 $category_name=str_replace('?lang=en','',$category_name);
				$myarr =  get_posts("category_name=$category_name&numberposts=-1&orderby=post_date&order=DESC");
				if ($category_name=='bogrim_events') {
					$querystr = "SELECT * FROM $wpdb->posts
					LEFT JOIN $wpdb->postmeta ON($wpdb->posts.ID = $wpdb->postmeta.post_id)
					LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
					LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
					LEFT JOIN $wpdb->terms ON ($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)
					WHERE  $wpdb->terms.slug = '$category_name'
					AND $wpdb->posts.post_status = 'publish'
					AND $wpdb->postmeta.meta_key = 'event_date'
					ORDER BY $wpdb->postmeta.meta_value DESC";
					$myarr= $wpdb->get_results($querystr, OBJECT);
				}
				
				
				
				
				
				//print_r($myarr[0]);
				foreach($myarr as $my_post){
//					print_r($my_post);
					echo "<h3><a href = '$my_post->guid'>$my_post->post_title</a></h3>";
		
					echo  first_words(strip_tags (  $my_post->post_content,'<p><a>' ),40,"<a href ='$my_post->guid' >  להמשך-></a>") ;
					echo "<br/>";
					echo "<br/>";
					
				}	
//				get_template_part( 'loop', 'category' );
				?>

			</div><!-- #content -->
			
		<div class = 'allnav' style="float:left;width:324px;">
			<div class = "nav_menu" style='padding-top:50px;'>
			
				<ul class = "menulevel1">

		<?php
		echo "<ul class = 'pagenav'>";
		wp_list_pages( 'depth=2&title_li=&exclude=11846' );
		
		 ?>
		 		</ul>
			</ul>
 	</div>
 </div>
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); 
function first_words($string, $num, $tail=' ...')
{
       /** words into an array **/
       //$words = str_word_count_utf8($string);
               
               $words = explode  ( ' '  , $string , $num + 1 );
               if (count($words) <= $num) return $string;
       /*** get the first $num words ***/
       $firstwords = array_slice( $words, 0, $num);
       /** return words in a string **/
       return implode(' ', $firstwords).$tail;
}


?>
