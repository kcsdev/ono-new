<?php
/**
 * Template Name: קורות חיים
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */




get_header();

?>
	
<style>
.entry-content {
width:500px;
}

	#primary {	
		margin-top:40px;
	}
#container {
		
		margin: 0 0 0 -350px;
	}
	
	<?php if ($lang!='he'):?>
		.resume_div #content {
			padding: 0 0 0 0 !important;
		}
	<?php endif;?>    
	
	#content {
	
		width:550px;
		float:right;
		margin: 0 20px 36px 0px;
	}
	.widget-area{margin-top:17px;}
	.side_menu{
		width:326px;
	}
	#content .entry-title{font-size:27px;position:absolute;top:255px; right:20px}
	
	
</style>
		<div class="upper_banner">

			<?php 
$pageparentarray =  get_page( $page_id,ARRAY_A ); 
	  $pageparent = $pageparentarray[post_parent];
	  $parent_title = get_the_title($post->post_parent);
	  $page_ancestors = get_post_ancestors( $post );	

	  $parent = wp_list_pages('title_li=&child_of='.$pageparent.'&echo=0&depth=1&exclude=11846');
	  $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=1&exclude=11846');
	  //echo $parent;
	$split_parent = split('</li>',$parent);
	//print_r($split_parent);
ob_start();
    	meta('upperpic');
    	$content = ob_get_contents();
	ob_end_clean();
	
if($content){meta('upperpic');}
else{

		foreach($page_ancestors as $post_id){
			
				if (get_post_meta($post_id, 'upperpic',true)){
					echo( get_post_meta($post_id, 'upperpic',true));
					break;
				}
		}	
	}
 ?> 
		</div>
		<div id = 'bread' style ="padding-top:3px;padding-bottom:5px;"> 
<?php if (class_exists('breadcrumb_navigation_xt')) {
	
	
echo 'הינך כאן : ';
	// New breadcrumb object
	$mybreadcrumb = new breadcrumb_navigation_xt;
	// Options for breadcrumb_navigation_xt
	$mybreadcrumb->opt['title_blog'] = '';
	$mybreadcrumb->opt['separator'] = ' &raquo; ';
	$mybreadcrumb->opt['singleblogpost_category_display'] = true;
	// Display the breadcrumb
	$mybreadcrumb->display();
} ?>
		</div>
		
		<div id="container">
			<div class = 'resume_div'>
				<div id="content" role="main">
			
				<div id = "resume_head" style = 'padding-top:10px;'>
				<?php 
				ob_start();
			
				the_permalink();
				$permalink = ob_get_contents();
			ob_end_clean();
			
				//echo $permalink;
				//echo $myurl;
				$page = get_page( $page_id ); 
				$title = $page->ID;
			//echo "SELECT * FROM wp_staff_directory WHERE post_link = '". $title."'";
					$all_rows = $wpdb->get_results( "SELECT * FROM wp_staff_directory WHERE post_link = '". $title."'");
					
					
					
					
					
						foreach($all_rows as $row){
					
						if ($row->photo !="") echo "";
						echo "<h1 class = 'prof_name'>$row->name </h1>";
					
					
						$email = $row->email;
					
						echo "<div class='resume_position'>".$row ->position."</div>";
					if ($lang=='he' || $lang=="") {
						echo '<h4> דרכי התקשרות </h4>';
						//echo $row->bio;
						echo  '<p><strong>דוא"ל:  </strong>';
					
						echo "<a href = '$email' > $row->email </a></p>";

						echo  '<p><strong>מספר טלפון: </strong>';
					
						echo "$row->phone </p>";
					
						echo  '<p><strong>מספר פקס: </strong>';
					
						echo "$row->fax </p>";
					} else {
						echo '<h4>Ways to contact</h4>';
						//echo $row->bio;
						echo  '<p><strong>E-mail:  </strong>';
					
						echo "<a href = '$email' > $row->email </a></p>";

						echo  '<p><strong>Phone: </strong>';
					
						echo "$row->phone </p>";
					
						echo  '<p><strong>Fax: </strong>';
					
						echo "$row->fax </p>";
					}
						//echo  '<p><strong>כיתות לימוד: </strong>';
					
						//echo "$row->phone </p>";
					
						break;
					
						}
					
					
				?>
				</div>			
	
		
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php if ( is_front_page() ) { ?>
							<h2 class="entry-title"><?php //the_title(); ?></h2>
						<?php } else { ?>
							<h1 class="entry-title"><?php //the_title(); ?></h1>
						<?php } ?>

						<div class="entry-content">
							<?php the_content(); ?>

							<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
							<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
						</div><!-- .entry-content -->
					</div><!-- #post-## -->

					<?php comments_template( '', true ); ?>

	<?php endwhile; ?>


			</div><!-- #content -->
		</div><!-- #resume_div -->	
	<div style="float:left;width:324px;">

<?php //start menu

//if ($children){
	
	

	echo '<div class = "nav_menu">';


		 //begin to echo menu
	 //get_custom_sidebar();

	
		echo '<ul class = "menulevel1">';
		if (count($page_ancestors) > 1 ){

			foreach($split_parent as $uncle){
				echo $uncle;
				if (strrpos($uncle,'current_page_item')){
				    	if($children){
				    	
			      			echo '<ul class = "menulevel2">' . $children . "</ul>";
			      		}
			   	}
			}
		      }//end counting ancestors
		else{
			echo '<ul class = "menulevel1">' .$children . "</ul>";
		}
		if ($children){
		echo '<div class="side_menu_bottom"></div>';
	}
	echo '</div> ';
//}

  //menu end here?>
 
		

 
	</div>

</div><!-- #container -->
<?php get_footer(); ?>
</div>

