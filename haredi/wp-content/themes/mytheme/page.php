<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */




get_header();

?>
	
<style>
.entry-content {
width:500px;
}

	#primary {	
		margin-top:40px;
	}
#container {
		
		margin: 0 0 0 -350px;
	}
	#content {
		float: right;
		
		
		margin: 0 20px 36px 0px;
	}
	.widget-area{margin-top:17px;}
	.side_menu{
		width:326px;
	}
	#content .entry-title{font-size:27px;top:255px; right:20px}
	
	
</style>
		<div class="upper_banner">

			<?php 

$pageparentarray =  get_page( $page_id,ARRAY_A ); 
	  $pageparent = $pageparentarray[post_parent];
	  $parent_title = get_the_title($post->post_parent);
	  $page_ancestors = get_post_ancestors( $post );	

	  $parent = wp_list_pages('title_li=&child_of='.$pageparent.'&echo=0&depth=1&exclude=11846');
	  $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=1&exclude=11846');
	  $parent_url = get_permalink( $pageparent );
	  //echo $parent;
	
	//print_r($split_parent);
	//meta('upperpic');
	$meta_values = get_post_meta($post->ID, 'upperpic', true);
	
//ob_start();
//    	//meta('upperpic');
//    	$content = ob_get_contents();
//	ob_end_clean();
	
if($meta_values){echo $meta_values;}
else{

		foreach($page_ancestors as $post_id){
			
				if (get_post_meta($post_id, 'upperpic',true)){
					echo( get_post_meta($post_id, 'upperpic',true));
					break;
				}
		}	
	}
 ?> 
		</div>
		<div id = 'bread' style ="padding-top:3px;padding-bottom:5px;"> 
<?php if (class_exists('breadcrumb_navigation_xt')) {
	
	
if ($lang == 'he'){ echo 'הינך כאן : ';}
if ($lang == 'en'){echo 'You are here :'; }
	// New breadcrumb object
	$mybreadcrumb = new breadcrumb_navigation_xt;
	// Options for breadcrumb_navigation_xt
	$mybreadcrumb->opt['title_blog'] = '';
	$mybreadcrumb->opt['separator'] = ' &raquo; ';
	$mybreadcrumb->opt['singleblogpost_category_display'] = true;
	// Display the breadcrumb
	$mybreadcrumb->display();
} 


?>
<?php if ($lang != 'en' && ($post->post_parent !='1683' && !in_array('1683',$page_ancestors) && $post->ID !='1683') ): ?>
<a href="https://yedion.ono.ac.il/Yedion/fireflyweb.aspx?appname=BSHITA&prgname=Enter_1" style="font-size:14px;float:left;font-weight:bold;">להרשמה אונליין – לחצו כאן</a>
<?php endif;?>

		</div>
		<div id="container">
			<div id="content" role="main">
		
			
	
		
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>

					<div class="entry-content">
						<?php the_content(); ?>

						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

				<?php comments_template( '', true ); ?>

<?php endwhile; ?>

			
			</div><!-- #content -->

<?php
if (($pageparent=='812' &&  $post->ID !='594' && $post->ID !='598') || ($pageparent=='9453' && $post->ID !='9449')) {
		?>
		<div class = 'allnav' style="float:left;width:324px;">
			
			<div class = "nav_menu" style='padding-top:50px; position:relative;'>

				<div class = "menulevel1">

					<?php
					echo "<ul class = 'pagenav'>";
					wp_list_pages( 'depth=2&title_li=&exclude=11846' );
					 ?>
		 			
					</ul>
			</div>
 		</div>
		<?php
	} else {
	?>
<?php
if($post->post_parent) {
			$parent1 = get_post($post->post_parent);
		}
if ($post->post_parent =='1683' || $post->ID =='1683' || in_array('1683',$page_ancestors)): ?>		
		<div class = 'custom_sidebar' style="float:left;">
			<?php 
			
			
			ob_start();
    	get_sidebar();
    	$content = ob_get_contents();
	ob_end_clean();
	
if($content){
echo "<div class = 'news_slider'>";

echo $content;

echo "</div>";
 }

			 
			?>
 
	</div>
<?php endif;?>
		<div class = 'allnav' style="float:left;width:324px;">
			

<?php //start menu

//if ($children){
	
	
	
	echo '<div class = "nav_menu">';
	
	
	
	
	
	$split_parent = split('</li>',$parent);
		
		 //begin to echo menu
	 
		
	if ((count($page_ancestors) > 1 ) || ($children)){	
	//echo '<div class="side_menu_top">&nbsp;</div>';
	
	}
	
	
	//if (($content2)||($children)) {echo '<div class="side_menu_top">&nbsp;</div>';}
		
		echo '<ul class = "menulevel1">';
		
		
			//new menu
			
		if ((count($page_ancestors) > 2 ) && (!$children)){	
			
			// dont_show_menu will stop any other echoing of the menu
			$dont_show_menu = true;
		
		        $grand_parent_children = wp_list_pages('title_li=&child_of='.$pageparent.'&echo=0&depth=1&exclude=11846');	
			$parent_info = get_page($pageparent, ARRAY_A);
			$grand_parent = $parent_info[post_parent];
			//			print_r ($parent_info);
			
			$grand_nav = wp_list_pages('title_li=&child_of='.$grand_parent.'&echo=0&depth=1&exclude=11846');
			$split_grand_parent = explode('</li>',$grand_nav);
			
			//echo "<ul class = menulevel1>" .$grand_nav. "</ul>";
			//echo "<!--1111111111"; echo $grand_parent_children;echo "-->";
			//foreach($split_grand_parent as &$v){
			//	
			//	if (substr_count($grand_parent_children,trim($v))>0) {
			//		unset($v);
			//	}
			//}
			
			foreach($split_grand_parent as $uncle){
				    echo $uncle;

				    if (strrpos($uncle,'current_page_parent')){
				    	//echo ';;;;;;;;;;;;;;;;;;<br/>';
						
							echo '<ul class = "new_menulevel2">' . $grand_parent_children . "</ul>";
						
			   	    }
				    
		 	}
	
			
		}


			//echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!';
			
			
						
		
			
			//old regular menu - if level 2 show children or brothers
		if ((count($page_ancestors) > 1 )&&(!$dont_show_menu)){ 
		//show page parent from third level down
		if (count($page_ancestors) > 2 ){echo "<li class = 'papa_page'><a href ='$parent_url'>$parent_title</a></li>";} //this will need to be edited
		
			foreach($split_parent as $uncle){
				echo $uncle;
				    if (strrpos($uncle,'current_page_item')){
						if ($children){ //to make sure no empty ul is created
							echo '<ul class = "menulevel2">' . $children . "</ul>";
				}
			   }
			}
		      }//end counting ancestors
		else{
			//if (count($page_ancestors) > 3 ){}
			if (!$children) { 
				//Commented Out 02.08.2012 - KCS
				//foreach($split_parent as $uncle){
				//	echo $uncle;
				//	    if (strrpos($uncle,'current_page_item')){
				//			if ($children){ //to make sure no empty ul is created
				//				echo '<ul class = "menulevel2">' . $children . "</ul>";
				//	}
				//   }
				//}
			} else {
			
				echo '<ul class = "menulevel1">' .$children . "</ul>";
			}
		}
		//if (($content2)||($children)) {//echo '<div class="side_menu_bot"></div>';}
	echo '</div> ';
}

  //menu end here?>
 
<?php if ($post->post_parent !='1683' && $post->ID !='1683' && !in_array('1683',$page_ancestors) ): ?>		
		<div class = 'custom_sidebar'>
			<?php 
			
			
			ob_start();
    	get_sidebar();
    	$content = ob_get_contents();
	ob_end_clean();
	
if($content){
echo "<div class = 'news_slider'>";

echo $content;

echo "</div>";
 }

			 
			?>
 
	</div>
<?php endif;?>
<?php/* if ($post->post_parent !='1683' && !in_array('1683',$page_ancestors)  && $post->ID !='1683' ): ?>
<div class="fb-like-box" data-href="https://www.facebook.com/OnoCollege" data-width="325" data-show-faces="true" data-border-color="#C3D81B" data-stream="false" data-header="false"></div>
<?php endif;*/?>
<?php get_footer(); ?>
</div><!-- #container -->

</div>
<?php
function first_words($string, $num, $tail=' ...')
{
       /** words into an array **/
       //$words = str_word_count_utf8($string);
               
               $words = explode  ( ' '  , $string , $num + 1 );
               if (count($words) <= $num) return $string;
       /*** get the first $num words ***/
       $firstwords = array_slice( $words, 0, $num);
       /** return words in a string **/
       return implode(' ', $firstwords).$tail;
}

?>
