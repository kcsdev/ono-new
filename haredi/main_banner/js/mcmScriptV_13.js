$(document).ready(function(){
  
  //Menu Over//
  $(".Button").mouseover(function(){
    $(this).css('background-image', 'url(images/1_BTNOver.png)');
  });
  
  $(".Button").mouseout(function(){
    $(this).css('background-image', 'url(images/1_BTN.png)');
  });
  
  
  //Pages//
  $(".CloseBTN").click(function(){
    $(".Pages").hide();
    $("#Main").show();
  });
  
  $(".Page1BTN").click(function(){
    $(".Pages").hide();
    $("#Page1").show();
  });
  
  $(".Page2BTN").click(function(){
    $(".Pages").hide();
    $("#Page2").show();
  });
  
  $(".Page3BTN").click(function(){
    $(".Pages").hide();
    $("#Page3").show();
  });
  
  $(".Page4BTN").click(function(){
    $(".Pages").hide();
    $("#Page4").show();
  });
  
  $(".Page5BTN").click(function(){
    $(".Pages").hide();
    $("#Page5").show();
  });
  
});